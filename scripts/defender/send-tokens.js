/*
    Send tokens to recipient.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { ethers } = require("ethers");
const { DefenderRelaySigner, DefenderRelayProvider } = require('defender-relay-client/lib/ethers');

// Inline ABI of the contract we'll be interacting with (check out the rollup example for a better way to handle this!)
const ERC20_ABI = [{anonymous:false,inputs:[{indexed:true,internalType:"address",name:"owner",type:"address"},{indexed:true,internalType:"address",name:"spender",type:"address"},{indexed:false,internalType:"uint256",name:"value",type:"uint256"}],name:"Approval",type:"event"},{anonymous:false,inputs:[{indexed:true,internalType:"address",name:"from",type:"address"},{indexed:true,internalType:"address",name:"to",type:"address"},{indexed:false,internalType:"uint256",name:"value",type:"uint256"}],name:"Transfer",type:"event"},{inputs:[{internalType:"address",name:"owner",type:"address"},{internalType:"address",name:"spender",type:"address"}],name:"allowance",outputs:[{internalType:"uint256",name:"",type:"uint256"}],stateMutability:"view",type:"function"},{inputs:[{internalType:"address",name:"spender",type:"address"},{internalType:"uint256",name:"amount",type:"uint256"}],name:"approve",outputs:[{internalType:"bool",name:"",type:"bool"}],stateMutability:"nonpayable",type:"function"},{inputs:[{internalType:"address",name:"account",type:"address"}],name:"balanceOf",outputs:[{internalType:"uint256",name:"",type:"uint256"}],stateMutability:"view",type:"function"},{inputs:[],name:"totalSupply",outputs:[{internalType:"uint256",name:"",type:"uint256"}],stateMutability:"view",type:"function"},{inputs:[{internalType:"address",name:"recipient",type:"address"},{internalType:"uint256",name:"amount",type:"uint256"}],name:"transfer",outputs:[{internalType:"bool",name:"",type:"bool"}],stateMutability:"nonpayable",type:"function"},{inputs:[{internalType:"address",name:"sender",type:"address"},{internalType:"address",name:"recipient",type:"address"},{internalType:"uint256",name:"amount",type:"uint256"}],name:"transferFrom",outputs:[{internalType:"bool",name:"",type:"bool"}],stateMutability:"nonpayable",type:"function"}];

// Main function, exported separately for testing
exports.main = async function(signer, recipient, amount, contractAddress) {
  // Create contract instance from the relayer signer
  const token = new ethers.Contract(contractAddress, ERC20_ABI, signer);

  // Check relayer balance via the Defender network provider
  const relayer = await signer.getAddress();
  const balance = await token.balanceOf(relayer);
  const weis = ethers.utils.parseEther(amount.toString());

  // Send funds to recipient is not more than relayer balance
  if (balance.gte(weis)) {
    const tx = await token.transfer(recipient, weis);
    console.log(`Transferred ${amount} to ${recipient} with tx hash = ${tx.hash}`);
    return tx;
  } else {
    console.log(`No balance to transfer`);
  }
}

// Entrypoint for the Autotask
exports.handler = async function(credentials, recipient, amount, contractAddress) {
  // Initialize defender relayer provider and signer
  const provider = new DefenderRelayProvider(credentials);
  const signer = new DefenderRelaySigner(credentials, provider, { speed: 'average' });
  return exports.main(signer, recipient, amount, contractAddress); // Send funds to self
}

const yargs = require('yargs');

const argv = yargs
    .command('', 'Send token to recipient', {})
    .option(
        'token_address', {
            alias: 't',
            description: 'Token contract address',
            type: 'string',
            required: true
        })
    .option(
        'recipient', {
            alias: 'r',
            description: 'Recipient address to transfer the tokens',
            type: 'string',
            required: true    
        })
    .option(
        'amount', {
            alias: 'a',
            description: 'Number of token to transfer',
            type: 'number',
            required: true
        })
    .help()
    .alias('help', 'h')
    .argv;

// To run locally (this code will not be executed in Autotasks)
if (require.main === module) {
  require('dotenv').config();
  const { DEFENDER_API_KEY: apiKey, DEFENDER_API_SECRET: apiSecret } = process.env;
  exports.handler({ apiKey, apiSecret }, argv.recipient, argv.amount, argv.token_address)
    .then(() => process.exit(0))
    .catch(error => { console.error(error); process.exit(1); });
}