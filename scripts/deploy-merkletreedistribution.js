const hre = require('hardhat')

const DeployResultWriter = require('../utils/deployResultWriter')
const realTree = require('../scripts/presale-bounties.json')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("MerkleTreeDistribution doesn't deploy to xDai or Chiado")
    return
  }
  const writer = new DeployResultWriter(hre.network.name)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const startTime = 1608674400 // 22 -12 - 2020 -- 22:00
  const endTime = 1616450400
  const editAddressUntil = 1616450400


  const MerkleTreeVestingReserve = await ethers.getContractFactory('MerkleTreeVestingReserve')

  const reserve = await MerkleTreeVestingReserve.deploy(ethixAddress, startTime, endTime, editAddressUntil, realTree.merkleRoot)

  console.log(`Presale-Bounties deployed to: ${reserve.address}`)
  writer.addContract('PresaleBountiesVestingReserve', reserve.address)

  const EthixToken = await ethers.getContractFactory('EthixToken')
  console.log(ethixAddress)
  const ethix = await EthixToken.attach(ethixAddress)
  console.log(realTree.total_tokens)
  await ethix.transfer(reserve.address, realTree.total_tokens)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
