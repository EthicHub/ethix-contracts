const hre = require('hardhat')

const DeployResultWriter = require('../utils/deployResultWriter')
const realTree = require('./presale-bounties.json')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("MerkleTreeDistribution doesn't deploy to xDai or Chiado")
    return
  }
  const userAddress = '0x09231d46553A5DFaBCD8e611231574EA0d2e4b24'
  const newAddress = '0xdFe435e074E9830036aeA0eA5C4fC506b7a11D5f'
  console.log(`Changing ${userAddress} for ${newAddress}`)
  const leaf = realTree.claims[userAddress]
  if (!leaf) {
    console.log('No leaf for address: '+leaf)
    return
  }
  console.log(leaf)

  const writer = new DeployResultWriter(hre.network.name)

  const presaleAddress = writer.currentDeployment.PresaleBountiesVestingReserve.address
  console.log('reserve:', presaleAddress)
  const MerkleTreeVestingReserve = await ethers.getContractFactory('MerkleTreeVestingReserve')

  const reserve = await MerkleTreeVestingReserve.attach(presaleAddress)
  const isAdded = await reserve.isVerified(leaf.index)
  if (isAdded) {
    console.log('Address already verified')
  } else {
    console.log('Not verified')
    const veryTx = await reserve.verifyAndAddEntry(leaf.index, userAddress, leaf.amount, leaf.proof)
    console.log(veryTx)
    veryTx.wait()
  }
  const changeTx = await reserve.changeTokenOwnership(userAddress, newAddress)
  console.log('Changing...')
  console.log(changeTx)

  
  

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
