const DeployResultWriter = require('../utils/deployResultWriter')
const ownableABI = require('../artifacts/@openzeppelin/contracts/access/Ownable.sol/Ownable.json').abi

async function main() {
  let newOwner
  switch (hre.network.name) {
    case 'rinkeby':
      newOwner = '0x8459eC490195FBf3DD4256a91Cc8999058E1fE6c'
      break
    case 'mainnet':
      newOwner = '0xA8f0A7dC8695c2233D847c7E34719830cdFaa4E1'
      break
    case 'kovan':
      newOwner = '0x26e630b8C0638BC0421Bf268F751f4030e942E43' // regular address not multisig
      break
    default:
      throw Error('undefined network')
  }
  const writer = new DeployResultWriter(hre.network.name)

  let contract = writer.currentDeployment.IncentiveVestingReserve3
  console.log('Transferring ownership of '+ contract.address+ ' to '+ newOwner + "...")
  
  const [signer] = await ethers.getSigners()
  const ownable = new ethers.Contract(contract.address, ownableABI, signer);
  const tx = await ownable.transferOwnership(newOwner)
  console.log(tx)
  console.log('Transferred ownership of '+ contract.address+ ' to '+ newOwner)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
