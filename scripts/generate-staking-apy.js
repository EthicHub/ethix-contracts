const hre = require('hardhat')

const DeployResultWriter = require('../utils/deployResultWriter')
const calculateTokensPerSecond = require('../utils/rewardCalculator')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("StkETHIX doesn't deploy to xDai or Chiado")
    return
  }
  
  const writer = new DeployResultWriter(hre.network.name)
  const stkEthixAddress = writer.currentDeployment.StakedETHIX.address
  console.log('Connecting to contracts...')
  console.log(writer.currentDeployment.StakedETHIX.address)
  const StkETHIX = await ethers.getContractFactory('StakedETHIX')
  const stkEthix = await StkETHIX.attach(stkEthixAddress)

  
  let stakeEventFilter = stkEthix.filters.Staked()
  let stakeEvents = await stkEthix.queryFilter(stakeEventFilter)
  console.log(stakeEvents.length)

  let redeemEventFilter = stkEthix.filters.Redeem()
  let redeemEvents = await stkEthix.queryFilter(redeemEventFilter)
  console.log(redeemEvents.length)
  
  let assetsConfigEventFilter = stkEthix.filters.AssetConfigUpdated()
  let assetsConfigEvents = await stkEthix.queryFilter(assetsConfigEventFilter)
  console.log(assetsConfigEvents.length)

  

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
