const hre = require('hardhat')

const DeployResultWriter = require('../utils/deployResultWriter')
const realTree = require('./presale-bounties.json')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("MerkleTreeDistribution doesn't deploy to xDai or Chiado")
    return
  }
  const userAddress = '0x89a672AC3686035b644564974A798e70B60A2A3a'
  console.log(`FirstClaim ${userAddress}`)
  const leaf = realTree.claims[userAddress]
  if (!leaf) {
    console.log('No leaf for address: '+leaf)
    return
  }
  console.log(leaf)

  const writer = new DeployResultWriter(hre.network.name)

  const presaleAddress = writer.currentDeployment.PresaleBountiesVestingReserve.address
  console.log('reserve:', presaleAddress)
  const MerkleTreeVestingReserve = await ethers.getContractFactory('MerkleTreeVestingReserve')

  const reserve = await MerkleTreeVestingReserve.attach(presaleAddress)
  console.log('First claiming...')
  const tx = await reserve.firstClaim(leaf.index, userAddress, leaf.amount, leaf.proof)
  console.log(tx.hash)
  tx.wait()
  

  
  

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
