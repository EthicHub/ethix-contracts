/*
    Deploy Originator Staking With LP Factory.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("OriginatorStakingWithLPFactory doesn't deploy to xDai or Chiado")
    return
  }
  const [owner] = await ethers.getSigners()
  const writer = new DeployResultWriter(hre.network.name)
  const OriginatorStakingWithLPFactory = await ethers.getContractFactory('OriginatorStakingWithLPFactory')

  const implementation = writer.currentDeployment.OriginatorStakingWithLPFactory.implementation
  console.log('Deploying originatorStkWithLPFactory...')
  const originatorStkWithLPFactory = await upgrades.deployProxy(
    OriginatorStakingWithLPFactory,
    [
      implementation
    ]
  )
  await originatorStkWithLPFactory.deployed()

  console.log(`Originator Staking Factory deployed to: ${originatorStkWithLPFactory.address}`)

  writer.addContract('OriginatorStakingWithLPFactory', originatorStkWithLPFactory.address, true)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
