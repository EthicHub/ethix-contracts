const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("InvestorsVestingReserve doesn't deploy to xDai or Chiado")
    return
  }
  const writer = new DeployResultWriter(hre.network.name)

  const InvestorsVestingReserve = await ethers.getContractFactory('InvestorsVestingReserve')
  const investorsVestingReserve = await InvestorsVestingReserve.attach(writer.currentDeployment.InvestorsVestingReserve2.address)
  const tx = await investorsVestingReserve.initialize()
  console.log(`Initialized InvestorsVestingReserve2 with this tx (${tx.hash})`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
