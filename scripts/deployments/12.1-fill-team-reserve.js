const { parseEther, formatEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("TeamVestingReserve doesn't deploy to xDai or Chiado")
    return
  }
  const vestedForTeam = parseEther('8870000')
  const writer = new DeployResultWriter(hre.network.name)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  console.log('Attaching to ETHIX')
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Attached to ETHIX')
  /*console.log('Approving ETHIX to compensation Reserve at '+ writer.currentDeployment.TeamVestingReserve.address)
  const teamVestingReserveAddress = writer.currentDeployment.TeamVestingReserve.address
  console.log(teamVestingReserveAddress)
  let tx = await ethix.approve(writer.currentDeployment.TeamVestingReserve.address, vestedForTeam)
  console.log(tx)
  tx.wait()*/
  const teamVestingReserveAddress = writer.currentDeployment.TeamVestingReserve.address
  console.log('Attaching to TeamVestingReserve')
  const TeamVestingReserve = await ethers.getContractFactory('TeamVestingReserve')
  const teamVestingReserve = await TeamVestingReserve.attach(teamVestingReserveAddress)
  console.log('Attached to TeamVestingReserve')
  let initializeTx = await teamVestingReserve.initialize()
  console.log(initializeTx)
  initializeTx.wait()


}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
