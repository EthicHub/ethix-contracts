const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("stkBEthixDai doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')
  const uEthixEthPool = writer.currentDeployment.UniswapEthixEthPool.address
  const rewardsVaultAddress = writer.currentDeployment.RewardsReserve.address
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const rewardsVault = await ERC20Reserve.attach(rewardsVaultAddress)

  const StakedUETHIXETH = await ethers.getContractFactory('StakedUETHIX')

  const ethixGovernanceAddress = writer.currentDeployment.StakedUETHIXETH.ethixGovernanceAddress
  const coolDownSeconds = writer.currentDeployment.StakedUETHIXETH.coolDownSeconds
  const unstakeWindow = writer.currentDeployment.StakedUETHIXETH.unstakeWindow
  const emissionManagerAddress = writer.currentDeployment.StakedUETHIXETH.emissionManagerAddress
  const distributionDuration = writer.currentDeployment.StakedUETHIXETH.distributionDuration
  console.log('Deploying StakedUETHIXETH...')

  const stkUEthixEth = await upgrades.deployProxy(
    StakedUETHIXETH,
    [
      uEthixEthPool,
      ethixGovernanceAddress,
      coolDownSeconds,
      unstakeWindow,
      rewardsVaultAddress,
      emissionManagerAddress,
      distributionDuration,
    ],
    { unsafeAllowCustomTypes: true }
  )
  await stkUEthixEth.deployed()

  console.log(`StakedUETHIXETH deployed to: ${stkUEthixEth.address}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'Staked U-ETHIX/ETH',
      address: stkUEthixEth.address,
    })

    await hre.tenderly.push({
      name: 'Staked U-ETHIX/ETH',
      address: stkUEthixEth.address,
    })
  }
  writer.addContract('StakedUETHIXETH', StakedUETHIXETH.address, true)

  await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), stkUEthixEth.address)
  console.log(`Staked U-ETHIX/DAI is TRANSFER_ROLE for ${rewardsVault.address}`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
