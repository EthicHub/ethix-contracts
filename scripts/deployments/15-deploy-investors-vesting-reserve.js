const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("VestingReserve doesn't deploy to xDai or Chiado")
    return
  }
  const startTime = 1648591200 // 30 marzo 2022 (1648591200)
  const endTime = 1743289200 // 30 marzo 2025 (1743289200)
  const editAddressUntil = 1648591200 // 30 marzo 2022 (1648591200)
  const writer = new DeployResultWriter(hre.network.name)
  const ethixAddress = writer.currentDeployment.EthixToken.address

  const InvestorsVestingReserve = await ethers.getContractFactory('InvestorsVestingReserve')

  const investorsVestingReserve = await InvestorsVestingReserve.deploy(ethixAddress, startTime, endTime, editAddressUntil)
  await investorsVestingReserve.deployed()

  console.log(`InvestorsVestingReserve deployed to: ${investorsVestingReserve.address}`)
  writer.addContract('InvestorsVestingReserve', investorsVestingReserve.address)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
