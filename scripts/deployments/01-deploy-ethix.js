const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("ETHIX doesn't deploy to xDai or Chiado")
    return
  }

  const [owner] = await ethers.getSigners()

  const EthixToken = await ethers.getContractFactory('EthixToken')

  const ethix = await upgrades.deployProxy(EthixToken)
  await ethix.deployed()

  console.log(`EthixToken deployed to: ${ethix.address}`)
  console.log(`Checking balance...`)
  console.log(`Owner address ${owner.address}`)

  const balance = await ethix.balanceOf(owner.address)
  console.log(`Balance of ${owner.address} is ${ethers.utils.formatEther(balance.toString())}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'Ethix',
      address: ethix.address,
    })

    await hre.tenderly.push({
      name: 'Ethix',
      address: ethix.address,
    })
  }

  const writer = new DeployResultWriter(hre.network.name)
  writer.addContract('EthixToken', ethix.address, true)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
