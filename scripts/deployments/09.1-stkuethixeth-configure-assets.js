const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')
const calculateTokensPerSecond = require('../../utils/rewardCalculator')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("stkUEthixEth doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')

  const stkUEthixEthAddress = writer.currentDeployment.StakedUETHIXETH.address

  const StakedEthixEth = await ethers.getContractFactory('StakedUETHIX')
  const stkUEthixEth = await StakedEthixEth.attach(stkUEthixEthAddress)

  const emissionsPerSecond = calculateTokensPerSecond('1000')
  console.log('emissionsPerSecond', emissionsPerSecond)
  const totalStaked = await stkUEthixEth.totalSupply()
  console.log('Total Staked', totalStaked.toString())
  console.log('Connected')

  console.log('Configuring assets stkUEthixEth...')
  const arguments = [
    {
      emissionPerSecond: emissionsPerSecond,
      totalStaked: totalStaked,
      underlyingAsset: stkUEthixEthAddress,
    }
  ]

  console.log(arguments)
  const tx = await stkUEthixEth.configureAssets(arguments)
  console.log(tx)
  console.log('Configured')
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
