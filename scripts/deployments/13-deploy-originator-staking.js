const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("OriginatorStaking doesn't deploy to xDai or Chiado")
    return
  }
  const [owner] = await ethers.getSigners()
  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const rewardsVaultAddress = writer.currentDeployment.RewardsReserve.address
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const rewardsVault = await ERC20Reserve.attach(rewardsVaultAddress)
  console.log('connected')
  const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')

  const emissionManagerAddress = writer.currentDeployment.OriginatorStaking.emissionManagerAddress
  const distributionDuration = writer.currentDeployment.OriginatorStaking.distributionDuration
  console.log('Deploying originatorStk...')
  const originatorStk = await upgrades.deployProxy(
    OriginatorStaking,
    [
      'Staked Um Coffee',
      'stkUmCoffee',
      18,
      ethixAddress,
      rewardsVaultAddress,
      emissionManagerAddress,
      distributionDuration,
    ],
    { unsafeAllowCustomTypes: true }
  )
  await originatorStk.deployed()

  console.log(`Originator Staking deployed to: ${originatorStk.address}`)

  writer.addContract('OriginatorStaking', originatorStk.address, true)

  await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStk.address)
  console.log(`orginatorStaking is TRANSFER_ROLE for ${rewardsVault.address}`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
