const { parseEther, formatEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("FirstOriginatorReserve doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  console.log('Attaching to ETHIX')
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Attached to ETHIX')
  console.log('Transferring ETHIX to compensation Reserve at ' + writer.currentDeployment.FirstOriginatorReserve.address)

  let tx = await ethix.transfer(writer.currentDeployment.FirstOriginatorReserve.address, parseEther('10000000'))
  tx.wait()
  let balance = await ethix.balanceOf(writer.currentDeployment.FirstOriginatorReserve.address)
  console.log('CompensationReserveBalance: ' + formatEther(balance))



}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
