const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("TeamVestingReserve doesn't deploy to xDai or Chiado")
    return
  }
  const startTime = 1616450400 // 22/03/2021 10:00 pm
  const endTime = 1742598000 // 21/03/2025 11:00 pm
  const editAddressUntil = 1647986400 // 22/03/2022 @ 10:00pm (UTC)
  const writer = new DeployResultWriter(hre.network.name)
  const ethixAddress = writer.currentDeployment.EthixToken.address

  const TeamVestingReserve = await ethers.getContractFactory('TeamVestingReserve')

  const teamVestingReserve = await TeamVestingReserve.deploy(ethixAddress, startTime, endTime, editAddressUntil)
  await teamVestingReserve.deployed()

  console.log(`TeamVestingReserve deployed to: ${teamVestingReserve.address}`)
  writer.addContract('TeamVestingReserve', teamVestingReserve.address)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
