const { parseEther, formatEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("InvestorsVestingReserve doesn't deploy to xDai or Chiado")
    return
  }
  const writer = new DeployResultWriter(hre.network.name)

  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(writer.currentDeployment.EthixToken.address)
  console.log(`Approving transfer ETHIX to InvestorsVestingReserve (${writer.currentDeployment.InvestorsVestingReserve.address})`)
  const vestedForInvestors = parseEther('277776')
  const tx = await ethix.approve(writer.currentDeployment.InvestorsVestingReserve.address, vestedForInvestors)
  console.log(`Approved transfer ETHIX to InvestorsVestingReserve (${writer.currentDeployment.InvestorsVestingReserve.address}) with tx (${tx.hash})`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
