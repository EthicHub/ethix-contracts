const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("PoolManager doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const daiAddress = writer.currentDeployment.StableCoin.address

  const poolAddress = writer.currentDeployment.BalancerCompensationPool.address
  if (!poolAddress) {
    throw new Error('No pool address. Did you deploy it previously?')
  }
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const compensationReserve = await ERC20Reserve.deploy(ethixAddress)
  await compensationReserve.deployed()

  console.log(`CompensationReserve deployed to: ${compensationReserve.address}`)

  const CompensationSystemManager = await ethers.getContractFactory('CompensationSystemManager')
  const compensationSystemManager = await upgrades.deployProxy(CompensationSystemManager, [
    ethixAddress,
    daiAddress,
    compensationReserve.address,
    poolAddress,
  ])
  await compensationSystemManager.deployed()
  console.log(`CompensationSystemManager deployed to: ${compensationSystemManager.address}`)

  await compensationReserve.grantRole(
    ethers.utils.id('TRANSFER_ROLE'),
    compensationSystemManager.address
  )
  console.log('Set TRANSFER_ROLE to CompensationSystemManager')

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'CompensationReserve',
      address: compensationReserve.address,
    })

    await hre.tenderly.push({
      name: 'CompensationReserve',
      address: compensationReserve.address,
    })

    await hre.tenderly.persistArtifacts({
      name: 'CompensationSystemManager',
      address: compensationSystemManager.address,
    })

    await hre.tenderly.push({
      name: 'CompensationSystemManager',
      address: compensationSystemManager.address,
    })
  }

  writer.addContract('CompensationReserve', compensationReserve.address)

  writer.addContract('CompensationSystemManager', compensationSystemManager.address, true)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
