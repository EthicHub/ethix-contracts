const { parseEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  const writer = new DeployResultWriter(hre.network.name)
  
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  const accountIncentive = writer.currentDeployment.RewardsReserve.address

  
  const amountIncentive = parseEther('10000000') //10M

  const IncentiveVestingReserve3 = await ethers.getContractFactory('IncentiveVestingReserve')

  const incentiveReserve3 = await IncentiveVestingReserve3.attach(writer.currentDeployment.IncentiveVestingReserve3.address)

  const txapp = await ethix.approve(incentiveReserve3.address, amountIncentive)
  console.log(`Approved...`)
  await txapp.wait();

  const [owner] = await ethers.getSigners()
  const approval = await ethix.allowance(owner.address, incentiveReserve3.address)
  console.log(approval.toString())
  console.log('Initializing...')
  let tx = await incentiveReserve3.initializeFor(accountIncentive, amountIncentive)
  tx.wait()
  let balance = await ethix.balanceOf(incentiveReserve3.address)
  console.log(`IncentiveVestingReserve3 balance : ${balance.toString()}`)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
