const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (!['local', 'xdai', 'chiado'].includes(hre.network.name)) {
    console.log('FeesTreasury goes in local, xDAI or Chiado')
    return
  }

  const writer = new DeployResultWriter(hre.network.name)

  const Treasury = await ethers.getContractFactory('NativeFeesTreasury')

  const minPeriodDistribution = writer.currentDeployment.NativeFeesTreasury.minPeriodDistribution
  const recipient = writer.currentDeployment.NativeFeesTreasury.recipient

  const treasury = await upgrades.deployProxy(Treasury, [minPeriodDistribution, recipient])
  await treasury.deployed()

  console.log(`NativeFeesTreasury deployed to: ${treasury.address}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'NativeFeesTreasury',
      address: treasury.address,
    })

    await hre.tenderly.push({
      name: 'NativeFeesTreasury',
      address: treasury.address,
    })
  }

  writer.addContract('NativeFeesTreasury', treasury.address)
  const reserveAddress = await treasury.reserve()
  writer.addContract('NativeFeesTreasuryReserve', reserveAddress)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
