const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers
console.log(ethers)

async function main() {
  if (!['kovan', 'goerli', 'local', 'rinkeby'].includes(hre.network.name)) {
    console.log('Only deploy testnet toolings to testnets')
    return
  }
  const writer = new DeployResultWriter(hre.network.name)
  const [owner] = await ethers.getSigners()

  console.log(`Deploying TestToken...`)

  const TestToken = await ethers.getContractFactory('TestToken')

  const token = await TestToken.deploy()

  console.log(`TestToken deployed to: ${token.address}`)
  console.log(`Owner address ${owner.address}`)

  console.log(`Checking balance...`)
  const balance = await token.balanceOf(owner.address)
  console.log(`Balance of ${owner.address} is ${ethers.utils.formatEther(balance.toString())}`)

  writer.addContract('DAI', token.address, true)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
