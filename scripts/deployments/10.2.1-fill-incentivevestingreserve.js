const { parseEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  const writer = new DeployResultWriter(hre.network.name)
  
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  const accountIncentive = writer.currentDeployment.RewardsReserve.address
  console.log(accountIncentive)
  const amountIncentive = parseEther('10000000') //10M

  const IncentiveVestingReserve2 = await ethers.getContractFactory('IncentiveVestingReserve')

  const incentiveReserve2 = await IncentiveVestingReserve2.attach(writer.currentDeployment.IncentiveVestingReserve2.address)
  console.log(incentiveReserve2.address)
  
  const txapp = await ethix.approve(incentiveReserve2.address, amountIncentive)
  console.log(`Approved...`)
  await txapp.wait();

  const [owner] = await ethers.getSigners()
  const approval = await ethix.allowance(owner.address, incentiveReserve2.address)
  console.log(approval.toString())
  console.log('Initializing...')
  let tx = await incentiveReserve2.initializeFor(accountIncentive, amountIncentive)
  tx.wait()
  let balance = await ethix.balanceOf(incentiveReserve2.address)
  console.log(`IncentiveVestingReserve2 balance : ${balance.toString()}`)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
