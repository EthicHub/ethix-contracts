const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')
const configurableRightsPoolABI = require('../../utils/balancerABI/configurableRightsPool.json')

const crpFactoryABI = require('../../utils/balancerABI/crpFactory.json')

const ethers = hre.ethers
const parseEther = ethers.utils.parseEther

/**
 * Run this only in a network where balancer is deployed
 */
async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("Pool doesn't deploy to xDai or Chiado")
    return
  }
  const writer = new DeployResultWriter(hre.network.name)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  console.log('Attaching to Ethix...')

  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Attached.')

  const daiAddress = writer.currentDeployment.StableCoin.address
  const DaiToken = await ethers.getContractFactory('TestToken')
  console.log('Attaching to DAI...')

  const dai = await DaiToken.attach(daiAddress)
  console.log('Attached.')

  const swapFee = parseEther('0.01')

  const SYMBOL = 'B-ETHIX/DAI'
  const NAME = 'EthicHub Compensation Pool'

  const permissions = {
    canPauseSwapping: true,
    canChangeSwapFee: true,
    canChangeWeights: true,
    canAddRemoveTokens: true,
    canWhitelistLPs: false,
    canChangeCap: false,
  }

  const startWeights = [parseEther('36'), parseEther('4')]
  const startBalances = [parseEther('281256'), parseEther('20000')]

  const poolParams = {
    poolTokenSymbol: SYMBOL,
    poolTokenName: NAME,
    constituentTokens: [ethixAddress, daiAddress],
    tokenBalances: startBalances,
    tokenWeights: startWeights,
    swapFee: swapFee,
  }
  const crpFactoryAddress = writer.currentDeployment.balancer.smartPoolFactory.address
  const signers = await ethers.getSigners()

  console.log('Attacking to crpFactory at ' + crpFactoryAddress + ' ...')
  const factory = new ethers.Contract(crpFactoryAddress, crpFactoryABI, signers[0])
  console.log('Attached.')
  console.log('Creating CRP...')
  const controllerCRPAddress = await factory.callStatic.newCrp(
    writer.currentDeployment.balancer.corePoolFactory.address,
    poolParams,
    permissions
  )

  await factory.newCrp(
    writer.currentDeployment.balancer.corePoolFactory.address,
    poolParams,
    permissions
  )
  console.log(' CRP: ' + controllerCRPAddress)
  writer.addContract('ConfigurableRightsPool', controllerCRPAddress)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
