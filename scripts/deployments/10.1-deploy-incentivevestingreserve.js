const { parseEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

const startTime = 1608674400 // 2020/12/22 22:00 UTC
const endTime = 1640210400 // 2021/12/22 22:00 UTC
const editAddressUntil = 1614031200 // 2021/02/22 22:00 UTC

async function main() {
  const writer = new DeployResultWriter(hre.network.name)
  
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)

  const IncentiveVestingReserve = await ethers.getContractFactory('IncentiveVestingReserve')

  const incentiveReserve = await IncentiveVestingReserve.deploy(ethix.address, startTime, endTime, editAddressUntil)

  console.log(`IncentiveVestingReserve deployed to: ${incentiveReserve.address}`)

  console.log(`npx hardhat --network mainnet verify ${incentiveReserve.address} ${ethixAddress} ${startTime} ${endTime} ${editAddressUntil}`)


  writer.addContract('IncentiveVestingReserve1', incentiveReserve.address)



}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
