/*
    Configure assets in Originator Staking.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { Wallet } = require('ethers')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("OriginatorStaking doesn't deploy to xDai or Chiado")
    return
  }
  const provider = ethers.getDefaultProvider('http://localhost:8545')
  const writer = new DeployResultWriter(hre.network.name)
  var [owner] = await ethers.getSigners()
  const originatorStakingAddress = writer.currentDeployment.OriginatorStaking.address
  console.log('Connecting to contracts...')
  console.log(writer.currentDeployment.OriginatorStaking.address)
  const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
  const originatorStaking = await OriginatorStaking.attach(originatorStakingAddress)
  console.log('Connected')

  const auditorPercentage = writer.currentDeployment.OriginatorStaking.auditorPercentage
  const originatorPercentage = writer.currentDeployment.OriginatorStaking.originatorPercentage
  const stakingGoal = writer.currentDeployment.OriginatorStaking.stakingGoal
  const defaultDelay = writer.currentDeployment.OriginatorStaking.defaultDelay

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  var auditorAddress = writer.currentDeployment.OriginatorStaking.auditorAddress
  var originatorAddress = writer.currentDeployment.OriginatorStaking.originatorAddress
  const governanceAddress = writer.currentDeployment.OriginatorStaking.governanceAddress

  if (hre.network.name != 'mainnet') {
    // Send ETHs to auditor and originator
    var [owner, originator, auditor] = await ethers.getSigners()
    const auditorBalance = await auditor.getBalance()
    const originatorBalance = await originator.getBalance()
    if (auditorBalance.isZero()) await owner.sendTransaction({ to: auditor.address, value: ethers.utils.parseEther('0.001') })
    if (originatorBalance.isZero()) await owner.sendTransaction({ to: originator.address, value: ethers.utils.parseEther('0.001') })

    console.log('Configuring auditor approve: ' + auditor.address)
    console.log('Configuring originator approve: ' + originator.address)
    await ethix.connect(originator).approve(originatorStaking.address, ethers.constants.MaxUint256)
    await ethix.connect(auditor).approve(originatorStaking.address, ethers.constants.MaxUint256)
    console.log('Configured approves.')

    await ethix.transfer(auditor.address, ethers.utils.parseEther('100'))
    await ethix.transfer(originator.address, ethers.utils.parseEther('100'))
    console.log('Transfered tokens to auditor and originator.')
    originatorAddress = originator.address
    auditorAddress = auditor.address
  }

  console.log('Configuring setupTerms originatorStaking...')

  const tx = await originatorStaking.setUpTerms(
    auditorAddress,
    originatorAddress,
    governanceAddress,
    auditorPercentage,
    originatorPercentage,
    stakingGoal,
    defaultDelay
  )
  console.log(tx)
  console.log('Configured')

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
