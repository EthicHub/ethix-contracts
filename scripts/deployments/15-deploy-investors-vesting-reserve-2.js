const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("VestingReserve doesn't deploy to xDai or Chiado")
    return
  }
  const startTime = 1672441200 // 2022-12-31 (1672441200)
  const endTime = 1767135600 // 2025-12-31 (1767135600)
  const editAddressUntil = 1767049200 // 2025-12-30 (1767049200)
  const writer = new DeployResultWriter(hre.network.name)
  const ethixAddress = writer.currentDeployment.EthixToken.address

  const InvestorsVestingReserve = await ethers.getContractFactory('InvestorsVestingReserve')

  const investorsVestingReserve = await InvestorsVestingReserve.deploy(ethixAddress, startTime, endTime, editAddressUntil)
  await investorsVestingReserve.deployed()

  console.log(`InvestorsVestingReserve deployed to: ${investorsVestingReserve.address}`)
  writer.addContract('InvestorsVestingReserve2', investorsVestingReserve.address)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
