const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

const startTime = 1797976800 // 2026/12/22 22:00 UTC
const endTime = 2113596000 // 2036/12/22 22:00 UTC
const editAddressUntil = 1800655200 // 2027/01/22 22:00 UTC

async function main() {
  const writer = new DeployResultWriter(hre.network.name)
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  const IncentiveVestingReserve3 = await ethers.getContractFactory('IncentiveVestingReserve')
  const incentiveReserve = await IncentiveVestingReserve3.deploy(ethix.address, startTime, endTime, editAddressUntil)

  console.log(`IncentiveVestingReserve3 deployed to: ${incentiveReserve.address}`)
  writer.addContract('IncentiveVestingReserve3', incentiveReserve.address)
  
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
