const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("StkETHIX doesn't deploy to xDai or Chiado")
    return
  }
  const [owner] = await ethers.getSigners()
  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const rewardsVaultAddress = writer.currentDeployment.RewardsReserve.address
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const rewardsVault = await ERC20Reserve.attach(rewardsVaultAddress)
  console.log('connected')
  const StakedETHIX = await ethers.getContractFactory('StakedETHIX')

  const ethixGovernanceAddress = writer.currentDeployment.StakedETHIX.ethixGovernanceAddress
  const coolDownSeconds = writer.currentDeployment.StakedETHIX.coolDownSeconds
  const unstakeWindow = writer.currentDeployment.StakedETHIX.unstakeWindow
  const emissionManagerAddress = owner.address 
  const distributionDuration = writer.currentDeployment.StakedETHIX.distributionDuration
  console.log('Deploying stkEthix...')
  const stkEthix = await upgrades.deployProxy(
    StakedETHIX,
    [
      ethixAddress,
      ethixGovernanceAddress,
      coolDownSeconds,
      unstakeWindow,
      rewardsVaultAddress,
      emissionManagerAddress,
      distributionDuration,
    ],
    { unsafeAllowCustomTypes: true }
  )
  await stkEthix.deployed()

  console.log(`StakedETHIX deployed to: ${stkEthix.address}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'StakedETHIX',
      address: stkEthix.address,
    })

    await hre.tenderly.push({
      name: 'StakedETHIX',
      address: stkEthix.address,
    })
  }
  writer.addContract('StakedETHIX', stkEthix.address, true)

  await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), stkEthix.address)
  console.log(`stkEthix is TRANSFER_ROLE for ${rewardsVault.address}`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
