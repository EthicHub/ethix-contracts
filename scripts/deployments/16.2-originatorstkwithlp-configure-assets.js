/*
    Configure assets in Originator Staking With LP.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')
const calculateTokensPerSecond = require('../../utils/rewardCalculator')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("OriginatorStakingWithLP doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)
  const originatorStakingWithLPAddress = writer.currentDeployment.OriginatorStakingWithLP.address
  console.log('Connecting to contracts...')
  console.log(writer.currentDeployment.OriginatorStakingWithLP.address)
  const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
  const originatorStakingWithLP = await OriginatorStakingWithLP.attach(originatorStakingWithLPAddress)

  const emissionsPerDay = writer.currentDeployment.OriginatorStakingWithLP.emissionsPerDay
  const emissionPerSecond = calculateTokensPerSecond(emissionsPerDay)
  console.log('tokens per day: ' + emissionsPerDay)
  const totalStaked = await originatorStakingWithLP.totalSupply.call()
  console.log('Connected')

  console.log('Configuring assets originatorStakingWithLP...')
  const config = {
    emissionPerSecond: emissionPerSecond,
    totalStaked: totalStaked.toString(),
    underlyingAsset: originatorStakingWithLPAddress,
  }
  console.log(config)
  const tx = await originatorStakingWithLP.configureAssets([config])
  console.log(tx)
  console.log('Configured')

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
