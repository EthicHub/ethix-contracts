const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("stkBEthixDai doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')
  const bEthixDaiAddress = writer.currentDeployment.BalancerCompensationPool.address
  const rewardsVaultAddress = writer.currentDeployment.RewardsReserve.address
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const rewardsVault = await ERC20Reserve.attach(rewardsVaultAddress)

  const StakedBETHIXDAI = await ethers.getContractFactory('StakedBETHIX')

  const ethixGovernanceAddress = writer.currentDeployment.StakedBETHIXDAI.ethixGovernanceAddress
  const coolDownSeconds = writer.currentDeployment.StakedBETHIXDAI.coolDownSeconds
  const unstakeWindow = writer.currentDeployment.StakedBETHIXDAI.unstakeWindow
  const emissionManagerAddress = writer.currentDeployment.StakedBETHIXDAI.emissionManagerAddress
  const distributionDuration = writer.currentDeployment.StakedBETHIXDAI.distributionDuration
  console.log('Deployng stkBEthixDAi...')

  const stkBEthixDai = await upgrades.deployProxy(
    StakedBETHIXDAI,
    [
      bEthixDaiAddress,
      ethixGovernanceAddress,
      coolDownSeconds,
      unstakeWindow,
      rewardsVaultAddress,
      emissionManagerAddress,
      distributionDuration,
    ],
    { unsafeAllowCustomTypes: true }
  )
  await stkBEthixDai.deployed()

  console.log(`StakedBETHIXDAI deployed to: ${stkBEthixDai.address}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'Staked B-ETHIX/DAI',
      address: stkBEthixDai.address,
    })

    await hre.tenderly.push({
      name: 'Staked B-ETHIX/DAI',
      address: stkBEthixDai.address,
    })
  }
  writer.addContract('StakedBETHIXDAI', stkBEthixDai.address, true)

  await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), stkBEthixDai.address)
  console.log(`Staked B-ETHIX/DAI is TRANSFER_ROLE for ${rewardsVault.address}`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
