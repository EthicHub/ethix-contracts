const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')
const configurableRightsPoolABI = require('../../utils/balancerABI/configurableRightsPool.json')

const ethers = hre.ethers
const parseEther = ethers.utils.parseEther

/**
 * Run this only in a network where balancer is deployed
 */
async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("Pool doesn't deploy to xDai or Chiado")
    return
  }
  const writer = new DeployResultWriter(hre.network.name)
  const signers = await ethers.getSigners()

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  console.log('Attaching to Ethix...')

  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Attached.')

  const daiAddress = writer.currentDeployment.StableCoin.address
  const DaiToken = await ethers.getContractFactory('TestToken')
  console.log('Attaching to DAI...')

  const dai = await DaiToken.attach(daiAddress)
  console.log('Attached.')

  const startBalances = [parseEther('471000'), parseEther('33492.94')]
  const controllerCRPAddress = writer.currentDeployment.ConfigurableRightsPool.address

  console.log(' Approving DAI...')
  await dai.approve(controllerCRPAddress, startBalances[0])
  console.log(' Approving ETHIX...')
  await ethix.approve(controllerCRPAddress, startBalances[1])
  console.log(' Approved')
  console.log('Attacking to Pool Controller (CRP) at ' + controllerCRPAddress + ' ...')

  const controller = new ethers.Contract(
    controllerCRPAddress,
    configurableRightsPoolABI,
    signers[0]
  )
  console.log('Attached')

  // Period is in blocks; 500 blocks ~ 2 hours; 90,000 blocks ~ 2 weeks
  const DEFAULT_MIN_WEIGHT_CHANGE_BLOCK_PERIOD = '10'
  const DEFAULT_ADD_TOKEN_TIME_LOCK_IN_BLOCKS = '10'
  console.log('Creating Pool...')
  console.log(controller)
  await controller.createPool(
    parseEther('100'),
    DEFAULT_MIN_WEIGHT_CHANGE_BLOCK_PERIOD,
    DEFAULT_ADD_TOKEN_TIME_LOCK_IN_BLOCKS
  )

  const bPoolAddr = await controller.bPool()
  console.log('Pool created at: ' + bPoolAddr)

  writer.addContract('BalancerCompensationPool', bPoolAddr, false)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
