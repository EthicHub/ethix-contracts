const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("RewardsReserve doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)

  const [owner] = await ethers.getSigners()
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const ethixAddress = writer.currentDeployment.EthixToken.address

  const rewardsVault = await ERC20Reserve.deploy(ethixAddress)
  await rewardsVault.deployed()
  const rewardsVaultAddress = rewardsVault.address

  console.log(`Rewards Reserve deployed to: ${rewardsVaultAddress}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'RewardsReserve',
      address: rewardsVaultAddress,
    })

    await hre.tenderly.push({
      name: 'RewardsReserve',
      address: rewardsVaultAddress,
    })
  }
  writer.addContract('RewardsReserve', rewardsVaultAddress)

  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  await ethix.transfer(rewardsVaultAddress, ethers.utils.parseEther('700000'))
  const bal = await ethix.balanceOf(rewardsVaultAddress)
  console.log('Transferred ' + bal.toString() + ' to Rewards Vault')
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
