/*
    Deploy CrowdEquityVestingReserve.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (!['celo', 'alfajores', 'hardhat'].includes(hre.network.name)) {
    console.log("CrowdEquityVestingReserve doesn't deploy to another network Celo or Alfajores")
    return
  }
  const startTime = 1692057600 // 2023-08-15(1692057600)
  const endTime = 1755216000 // 2025-08-15 (1755216000)
  const editAddressUntil = 1755215999 // 2025-08-15 (1755215999)
  const writer = new DeployResultWriter(hre.network.name)
  const ethixAddress = writer.currentDeployment.EthixToken.address

  const CrowdEquityVestingReserve = await ethers.getContractFactory('CrowdEquityVestingReserve')

  const crowdEquityVestingReserve = await CrowdEquityVestingReserve.deploy(ethixAddress, startTime, endTime, editAddressUntil)
  await crowdEquityVestingReserve.deployed()

  console.log(`CrowdEquityVestingReserve deployed to: ${crowdEquityVestingReserve.address}`)
  writer.addContract('CrowdEquityVestingReserve', crowdEquityVestingReserve.address)
  const tx = await crowdEquityVestingReserve.initialize()
  console.log(`Initialized CrowdEquityVestingReserve with this tx (${tx.hash})`)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
