const { parseEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

const startTime = 1640210400 // 2021/12/22 22:00 UTC
const endTime = 1797976800 // 2026/12/22 22:00 UTC
const editAddressUntil = 1642888800 // 2022/01/22 22:00 UTC

async function main() {
  const writer = new DeployResultWriter(hre.network.name)
  
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)

  const IncentiveVestingReserve2 = await ethers.getContractFactory('IncentiveVestingReserve')

  const incentiveReserve = await IncentiveVestingReserve2.deploy(ethix.address, startTime, endTime, editAddressUntil)

  console.log(`IncentiveVestingReserve2 deployed to: ${incentiveReserve.address}`)
  console.log(`npx hardhat --network mainnet verify ${incentiveReserve.address} ${ethixAddress} ${startTime} ${endTime} ${editAddressUntil}`)

  writer.addContract('IncentiveVestingReserve2', incentiveReserve.address)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
