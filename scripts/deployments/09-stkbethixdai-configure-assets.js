const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("stkBEthixDai doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')

  const stkBEthixDaiAddress = writer.currentDeployment.StakedBETHIXDAI.address

  const StakedBETHIXDAI = await ethers.getContractFactory('StakedBETHIX')
  const stkBEthixDai = await StakedBETHIXDAI.attach(stkBEthixDaiAddress)

  const emissionsPerSecond = writer.currentDeployment.StakedBETHIXDAI.emissionsPerSecond
  const totalStaked = await stkBEthixDai.totalSupply()
  console.log('Connected')

  console.log('Configuring assets stkBEthixDai...')
  await stkBEthixDai.configureAssets([
    {
      emissionPerSecond: emissionsPerSecond,
      totalStaked: totalStaked,
      underlyingAsset: stkBEthixDaiAddress,
    },
  ])
  console.log('Configured')
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
