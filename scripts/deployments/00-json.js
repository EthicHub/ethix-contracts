const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')


async function main() {

  const writer = new DeployResultWriter(hre.network.name)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
