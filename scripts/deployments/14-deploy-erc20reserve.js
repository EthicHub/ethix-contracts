const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['mainnet', 'kovan', 'rinkeby'].includes(hre.network.name)) {
    console.log("ERC20Reserve doesn't deploy to mainnet, Rinkeby or Kovan")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)

  const [owner] = await ethers.getSigners()
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const ethixAddress = writer.currentDeployment.EthixToken.address

  const erc20reserve = await ERC20Reserve.deploy(ethixAddress)
  await erc20reserve.deployed()
  const erc20ReserveAddress = erc20reserve.address

  console.log(`ERC20 Reserve deployed to: ${erc20ReserveAddress}`)

  // Tenderly pushing
  if (process.env.TENDERLY_ENABLED) {
    await hre.tenderly.persistArtifacts({
      name: 'ERC20Reserve',
      address: erc20ReserveAddress,
    })

    await hre.tenderly.push({
      name: 'ERC20Reserve',
      address: erc20ReserveAddress,
    })
  }
  writer.addContract('ERC20Reserve', erc20ReserveAddress)

  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  await ethix.transfer(erc20ReserveAddress, ethers.utils.parseEther('10000000'))
  const bal = await ethix.balanceOf(erc20ReserveAddress)
  console.log('Transferred ' + bal.toString() + ' to ERC20Reserve')
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
