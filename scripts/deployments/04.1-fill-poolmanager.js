const { parseEther, formatEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("PoolManager doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  console.log('Attaching to ETHIX')
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Attached to ETHIX')
  console.log('Transferring ETHIX to compensation Reserve at ' + writer.currentDeployment.CompensationReserve.address)

  await ethix.transfer(writer.currentDeployment.CompensationReserve.address, parseEther('50000000'))
  let balance = await ethix.balanceOf(writer.currentDeployment.CompensationReserve.address)
  console.log('CompensationReserveBalance: ' + formatEther(balance))



}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
