const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("PoolManager doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)

  const ethixAddress = writer.currentDeployment.EthixToken.address

  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const compensationReserve = await ERC20Reserve.deploy(ethixAddress)
  await compensationReserve.deployed()

  console.log(`FirstOriginatorReserve deployed to: ${compensationReserve.address}`)
  writer.addContract('FirstOriginatorReserve', compensationReserve.address)
  const compensationSystemManagerAddress = writer.currentDeployment.CompensationSystemManager.address 

  await compensationReserve.grantRole(
    ethers.utils.id('TRANSFER_ROLE'),
    compensationSystemManagerAddress
  )
  console.log('Set TRANSFER_ROLE to CompensationSystemManager')



}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
