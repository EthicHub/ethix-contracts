/*
    Deploy Originator Staking With LP.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("OriginatorStakingWithLP doesn't deploy to xDai or Chiado")
    return
  }
  const [owner] = await ethers.getSigners()
  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const uniV3PoolAddress = writer.currentDeployment.UniswapV3Pool.address
  const nftPositionManagerAddress = writer.currentDeployment.NonfungiblePositionManager.address
  const rewardsVaultAddress = writer.currentDeployment.RewardsReserve.address
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const rewardsVault = await ERC20Reserve.attach(rewardsVaultAddress)
  console.log('connected')
  const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')

  const emissionManagerAddress = writer.currentDeployment.OriginatorStakingWithLP.emissionManagerAddress
  const distributionDuration = writer.currentDeployment.OriginatorStakingWithLP.distributionDuration
  console.log('Deploying originatorStkWithLP...')
  const originatorStkWithLP = await upgrades.deployProxy(
    OriginatorStakingWithLP,
    [
      'Staked Test',
      'stkTEST',
      ethixAddress,
      rewardsVaultAddress,
      emissionManagerAddress,
      distributionDuration,
      uniV3PoolAddress,
      nftPositionManagerAddress,
    ],
    { unsafeAllowCustomTypes: true }
  )
  await originatorStkWithLP.deployed()

  console.log(`Originator Staking deployed to: ${originatorStkWithLP.address}`)

  writer.addContract('OriginatorStakingWithLP', originatorStkWithLP.address, true)

  await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStkWithLP.address)
  console.log(`orginatorStaking is TRANSFER_ROLE for ${rewardsVault.address}`)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
