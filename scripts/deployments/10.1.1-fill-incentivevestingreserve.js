const { parseEther } = require('ethers/lib/utils')
const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')

const ethers = hre.ethers

async function main() {
  const writer = new DeployResultWriter(hre.network.name)
  
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  const accountIncentive = writer.currentDeployment.RewardsReserve.address
  const amountIncentive = parseEther('5000000') //5M
  console.log('accountIncentive', accountIncentive)
  const IncentiveVestingReserve = await ethers.getContractFactory('IncentiveVestingReserve')

  const incentiveReserve = await IncentiveVestingReserve.attach(writer.currentDeployment.IncentiveVestingReserve1.address)
  console.log(incentiveReserve.address)
  
  const txapp = await ethix.approve(incentiveReserve.address, amountIncentive)
  console.log(`Approved...`)
  await txapp.wait();

  const [owner] = await ethers.getSigners()
  const approval = await ethix.allowance(owner.address, incentiveReserve.address)
  console.log(approval.toString())
  console.log('Initializing...')
  let tx = await incentiveReserve.initializeFor(accountIncentive, amountIncentive)
  tx.wait()
  let balance = await ethix.balanceOf(incentiveReserve.address)
  console.log(`IncentiveVestingReserve balance : ${balance.toString()}`)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
