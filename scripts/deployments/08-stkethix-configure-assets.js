const hre = require('hardhat')

const DeployResultWriter = require('../../utils/deployResultWriter')
const calculateTokensPerSecond = require('../../utils/rewardCalculator')
const ethers = hre.ethers

async function main() {
  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("StkETHIX doesn't deploy to xDai or Chiado")
    return
  }

  const writer = new DeployResultWriter(hre.network.name)
  const stkEthixAddress = writer.currentDeployment.StakedETHIX.address
  console.log('Connecting to contracts...')
  console.log(writer.currentDeployment.StakedETHIX.address)
  const StkETHIX = await ethers.getContractFactory('StakedETHIX')
  const stkEthix = await StkETHIX.attach(stkEthixAddress)

  // const emissionsPerSecond = writer.currentDeployment.StakedETHIX.emissionsPerSecond
  // const emissionsPerSecond = 11574074074074074
  const emissionsPerSecond = calculateTokensPerSecond('1000')
  console.log('tokens per second')
  const totalStaked = await stkEthix.totalSupply()
  console.log('Connected')

  console.log('Configuring assets stkEthix...')
  const config = {
    emissionPerSecond: emissionsPerSecond,
    totalStaked: totalStaked.toString(),
    underlyingAsset: stkEthixAddress,
  }
  console.log(config)
  const tx = await stkEthix.configureAssets([config])
  console.log(tx)
  console.log('Configured')

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
