#!/usr/bin/env python
#   Send Ethix not reclaimed with webhook on defender.

#   Copyright (C) 2021 EthicHub

#   This file is part of EthicHub ethix-contracts.
#
#   This is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import csv
from time import sleep
import json
import os
import requests
import argparse
from web3 import Web3


RED_COLOR = "\033[0;31m"
GREEN_COLOR = "\033[0;32m"
YELLOW_COLOR = "\033[0;33m"
NO_COLOR = "\033[0m"


def get_secrets(config_file_name: str):
    module_dir = os.path.dirname(__file__)
    file_path = os.path.join(module_dir, config_file_name)

    with open(file_path, "r") as jsonfile:
        return json.load(jsonfile)


def get_recipients(csv_file_name: str):
    module_dir = os.path.dirname(__file__)
    file_path = os.path.join(module_dir, csv_file_name)
    recipients = {}

    with open(file_path, 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')

        for row in reader:
            recipients[row['wallet'].lower()] = int(row['rewards'])
    
    return recipients


def execute_webhook(amount: str, recipient: str, webhook_url: str, token_address: str) -> str:
    url = webhook_url

    payload = {
        'amount': amount,
        'recipient': recipient,
        'token_address': token_address
    }
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    try:
        response = requests.post(url=url, headers=headers, data=json.dumps(payload))
        if response.status_code != 200 or not 'result' in response.json():
            raise Exception(f'Code status: {response.status_code}: Message: {response.json()["message"]}')
        return response.json()['result']
    except Exception as excp:
        raise Exception(f'{RED_COLOR}{excp}{NO_COLOR}')


if __name__ == '__main__':
    print(f'{GREEN_COLOR}************ START ************{NO_COLOR}')
    all_args = argparse.ArgumentParser()

    all_args.add_argument("-i", "--csv_file_users", required=True, help="CSV with all users not reclaimed on the presale")
    all_args.add_argument("-n", "--network", required=True, help="Blockchain network (mainnet, kovan, rinkeby, ...)")
    all_args.add_argument("-t", "--token_address", required=True, help="Token to send address")

    args = vars(all_args.parse_args())

    config = get_secrets('../.secrets.json')
    users = get_recipients(args['csv_file_users'])
    print('Total users not reclaimed ethix:', len(users))
    for wallet, weis in users.items():
        tokens = Web3.fromWei(weis, 'ether')
        print(f'Sending {tokens} to {wallet} ...')
        response = execute_webhook(str(tokens), wallet, config[args['network']]['defender_webhook_url'], args['token_address'])
        print(f'{GREEN_COLOR} {response}{NO_COLOR}')
        sleep(15)
    print(f'{GREEN_COLOR}************ DONE ************{NO_COLOR}')
