const fs = require("fs");
const csvParse = require('papaparse');
const beautify = require('json-beautify');

const { ethers } = require('hardhat');
const BigNumber = ethers.BigNumber;
const BalanceTree = require('../utils/merkle/balance-tree')

async function main() {
  const file = ''; // Ruta donde estan los csv
  dict = {}
  // When the file is a local file when need to convert to a file Obj.
  //  This step may not be necessary when uploading via UI
  var content = fs.readFileSync(file, "utf8");

  var parsedData = csvParse.parse(content, {header: true, delimiter: ","});
  dict['total_tokens'] = "" // A mano
  dict['total_addresses'] = "" // A mano
  leafs = []
  claims = {}
  for (var item of parsedData.data) {
    if (item['wallet'] != "") {
      account = item['wallet']
      amount = item['ethix']
      leafs.push({account: account, amount: BigNumber.from(amount)})
    }
  }
  tree = new BalanceTree(leafs)
  for (let i = 0; i < leafs.length; i++) {
      let leaf = leafs[i]
      let proof = tree.getProof(i, leaf.account, leaf.amount)
      claims[leaf.account.toString()] = {"index":i, "amount":leaf.amount.toString(), "proof": proof}
  }
  dict['merkleRoot'] = tree.getHexRoot()
  dict['claims'] = claims

    fs.writeFileSync('merkle-tree.json', beautify(dict, null, 2, 80))
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })