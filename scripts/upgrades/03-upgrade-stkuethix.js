/*
    Upgrade StakedUEthix contract script.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { ethers, upgrades } = require('hardhat')
const DeployResultWriter = require('../../utils/deployResultWriter')

async function main() {
  const writer = new DeployResultWriter(hre.network.name)

  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("stkETHIX or stkUETHIX doesn't deploy to xDai or Chiado")
    return
  }
  else if (['kovan', 'rinkeby', 'goerli', 'alfajores'].includes(hre.network.name)) {
    const StakedUETHIX = await ethers.getContractFactory('StakedUETHIX')
    const stakedUETHIX = await upgrades.upgradeProxy(
      writer.currentDeployment.StakedUETHIX.address,
      StakedUETHIX,
      { unsafeAllow: ['struct-definition'] }
    )
    console.log(`StakedUETHIX upgraded`)
  }
  else if (['mainnet'].includes(hre.network.name)) {
    const StakedUETHIX = await ethers.getContractFactory('StakedUETHIX')
    const stakedUETHIXAddress = await upgrades.prepareUpgrade(
      writer.currentDeployment.StakedUETHIX.address,
      StakedUETHIX
    );
    console.log(`StakedUETHIX prepare upgrade: ${stakedUETHIXAddress}`)
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
