const { ethers, upgrades } = require('hardhat')
const DeployResultWriter = require('../../utils/deployResultWriter')

async function main() {
  const writer = new DeployResultWriter(hre.network.name)

  const CompensationSystemManager = await ethers.getContractFactory('CompensationSystemManager')
  const compensationSystemManager = await upgrades.upgradeProxy(
    writer.currentDeployment.CompensationSystemManager.address,
    CompensationSystemManager
  )
  console.log('CompensationSystemManager upgraded')
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
