/*
    Upgrade OriginatorStaking contract script.

    Copyright (C) 2022 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const hre = require('hardhat')
const { ethers, upgrades } = require('hardhat')
const DeployResultWriter = require('../../utils/deployResultWriter')
const { getImplementationAddress } = require('@openzeppelin/upgrades-core')

async function main() {
  const writer = new DeployResultWriter(hre.network.name)

  if (['xdai', 'chiado'].includes(hre.network.name)) {
    console.log("originatorStaking doesn't deploy to xDai or Chiado")
    return
  }
  else if (['sepolia', 'goerli', 'local', 'celo', 'mainnet', 'alfajores'].includes(hre.network.name)) {
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking');
    console.log('Upgrading stkUmCoffee...', writer.currentDeployment.stkUmCoffee.address);
    const originatorStaking = await upgrades.upgradeProxy(
      writer.currentDeployment.stkUmCoffee.address,
      OriginatorStaking,
      {
        pollingInterval: 10000000
      }
    );
    const currentImplAddress = await getImplementationAddress(originatorStaking.provider, originatorStaking.address)
    console.log(`stkUmCoffee upgraded: ${originatorStaking.address}, implementation: ${currentImplAddress}`)
    writer.addContract('OriginatorStaking', originatorStaking.address, true)
  }
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })