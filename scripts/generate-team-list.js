const csvFilePath='./scripts/wallets-vesting-team.csv'
const csv=require('csvtojson')
// Async / await usage
const hre = require('hardhat')

const ethers = hre.ethers
const parseEther = ethers.utils.parseEther
const formatEther = ethers.utils.formatEther
const formatUnits = ethers.utils.formatUnits

async function main() {
  const jsonArray = await csv().fromFile(csvFilePath)
  console.log(jsonArray)
  const teamAmounts = jsonArray.filter(row => {
    console.log(row)
    return row.address && row.address !== ''
  }).map(x => {
    console.log(x)
    return {
      address: x.address,
      amount: parseEther(x.amount)
    }
  })
  const total = teamAmounts.map( x => x.amount).reduce((prev, next) => {
    return prev.add(next)
  })
  console.log('totalEther', formatEther(total))
  console.log('totalWei', formatUnits(total, 'wei').replace('.0',''))
  console.log('totalWei', `${formatEther(total).replace('.0','')}*10**18`)

  const result = teamAmounts.map( x => {
    // return `locked[${x.address}] = ${formatUnits(x.amount, 'wei').replace('.0','')};`
    return `locked[${x.address}] = ${formatEther(x.amount).replace('.0','')}*10**18;`
  }).reduce((prev, next) => `${prev}\n${next}`)
  console.log(result)
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
