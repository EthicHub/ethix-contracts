#!/usr/bin/env python
#   Calculate liquidity token script
#   download from https://uniswapv3book.com/docs/milestone_1/calculating-liquidity/.

#   Copyright (C) Uniswap V3 Book.
#
#   This is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import math

q96 = 2**96
def price_to_sqrtp(p):
    return int(math.sqrt(p) * q96)

sqrtp_low = price_to_sqrtp(0)
sqrtp_cur = price_to_sqrtp(9934)
sqrtp_upp = price_to_sqrtp(887220)

def liquidity0(amount, pa, pb):
    if pa > pb:
        pa, pb = pb, pa
    return (amount * (pa * pb) / q96) / (pb - pa)

def liquidity1(amount, pa, pb):
    if pa > pb:
        pa, pb = pb, pa
    return amount * q96 / (pb - pa)

eth = 10**18
amount_ethix = 99.33 * eth
amount_cusd = 0.009999 * eth

liq0 = liquidity0(amount_ethix, sqrtp_cur, sqrtp_upp)
liq1 = liquidity1(amount_cusd, sqrtp_cur, sqrtp_low)
liq = int(min(liq0, liq1))

print('Liquidity0: ', liq0)
print('Liquidity1: ', liq1)
print('Liquidity: ', liq)
