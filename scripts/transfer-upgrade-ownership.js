async function main() {
  let gnosisSafe
  switch (hre.network.name) {
    case 'rinkeby':
      gnosisSafe = '0x8459eC490195FBf3DD4256a91Cc8999058E1fE6c'
      break
    case 'mainnet':
      gnosisSafe = '0xA8f0A7dC8695c2233D847c7E34719830cdFaa4E1'
      break
    case 'kovan':
      gnosisSafe = '0x26e630b8C0638BC0421Bf268F751f4030e942E43' // regular address not multisig
      break
    default:
      throw Error('undefined network')
  }

  console.log('Transferring ownership of ProxyAdmin to '+ gnosisSafe + "...")
  // The owner of the ProxyAdmin can upgrade our contracts
  const tx = await upgrades.admin.transferProxyAdminOwnership(gnosisSafe)
  console.log('Transferred ownership of ProxyAdmin to:', gnosisSafe)

}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
