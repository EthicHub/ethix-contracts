// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import './IReserve.sol';

import '@openzeppelin/contracts/access/AccessControl.sol';
import '@openzeppelin/contracts/token/ERC20/IERC20.sol';
import '@openzeppelin/contracts/token/ERC20/SafeERC20.sol';
import '@openzeppelin/contracts/utils/ReentrancyGuard.sol';

/**
 * @title NativeReserve
 * @dev A simple holder of ether
 * @author Ethichub
 **/
contract NativeReserve is IReserve, AccessControl, ReentrancyGuard {
    using SafeERC20 for IERC20;
    bytes32 public constant TRANSFER_ROLE = keccak256('TRANSFER_ROLE');
    bytes32 public constant RESCUE_ROLE = keccak256('RESCUE_ROLE');

    constructor() {
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(TRANSFER_ROLE, msg.sender);
        _setupRole(RESCUE_ROLE, msg.sender);
    }

    receive() external payable {}

    function balance() external view override returns (uint256) {
        return address(this).balance;
    }

    function transfer(address payable _to, uint256 _value)
        external
        override
        nonReentrant
        returns (bool)
    {
        require(hasRole(TRANSFER_ROLE, msg.sender), 'NativeReserve: Caller is not a transferrer');

        (bool success, ) = _to.call{value: _value}('');
        require(success, 'NativeReserve: Transfer failed');

        emit Transfer(_to, _value);

        return true;
    }

    /**
    @dev WARNING: Thoroughly research the token to be rescued, it could be malicious code.
     */
    function rescueFunds(
        address _tokenToRescue,
        address _to,
        uint256 _amount
    ) external override {
        require(hasRole(RESCUE_ROLE, msg.sender), 'NativeReserve: Caller is not a rescuer');

        IERC20(_tokenToRescue).safeTransfer(_to, _amount);

        emit RescueFunds(_tokenToRescue, _to, _amount);

    }
}
