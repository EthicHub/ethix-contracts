// SPDX-License-Identifier: agpl-3.0

pragma solidity 0.7.5;
pragma experimental ABIEncoderV2;

import './bases/staking/StakingRewards.sol';
import './bases/BaseTokenUpgradeable.sol';
import './bases/staking/interfaces/IOriginatorStakingWithLP.sol';
import '../reserve/IReserve.sol';
import '../utils/SafeMathUint128.sol';

import '@uniswap/v3-core/contracts/libraries/TickMath.sol';
import '@uniswap/v3-periphery/contracts/libraries/LiquidityAmounts.sol';
import '@uniswap/v3-core/contracts/interfaces/IUniswapV3Pool.sol';
import '@uniswap/v3-periphery/contracts/interfaces/INonfungiblePositionManager.sol';

import '@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol';
import '@openzeppelin/contracts-upgradeable/token/ERC20/SafeERC20Upgradeable.sol';
import '@openzeppelin/contracts-upgradeable/token/ERC721/IERC721ReceiverUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/math/SafeMathUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/proxy/Initializable.sol';

/**
 * @title  OriginatorStaking
 * @notice Contract to stake Originator Hub tokens, tokenize the position and get rewards, inheriting from a distribution manager contract
 * @author Aave / Ethichub
 **/
contract OriginatorStakingWithLP is Initializable, StakingRewards, BaseTokenUpgradeable, IStaking, IProjectFundedRewards, IOriginatorManager, IERC721ReceiverUpgradeable {
    using SafeERC20Upgradeable for IERC20Upgradeable;
    using SafeMathUpgradeable for uint256;
    using SafeMathUint128 for uint128;

    enum OriginatorStakingState {
        UNINITIALIZED,
        STAKING,
        STAKING_END,
        DEFAULT
    }

    OriginatorStakingState public state;

    IERC20Upgradeable public TOKEN_TO_STAKE;
    IUniswapV3Pool public POOL;
    INonfungiblePositionManager public LP_MANAGER;
    /// @notice IReserve to pull from the rewards, needs to have this contract as WITHDRAW role
    IReserve public REWARDS_VAULT;

    uint256 public stakingGoal;
    uint256 public defaultedAmount;
    uint256 public depositAmount;
    uint256 public defaultDate;
    address public liquidityPositionOwner;

    mapping(address => uint256) public stakerRewardsToClaim;

    bytes32 public constant GOVERNANCE_ROLE = keccak256('GOVERNANCE_ROLE');
    bytes32 public constant ORIGINATOR_ROLE = keccak256('ORIGINATOR_ROLE');
    bytes32 public constant AUDITOR_ROLE = keccak256('AUDITOR_ROLE');

    int24 private constant MIN_TICK = -887220;
    int24 private constant MAX_TICK = -MIN_TICK;
    uint8 private constant ERROR_MARGIN = 10;

    event StateChange(uint256 state);

    event Staked(address indexed from, address indexed onBehalfOf, uint256 amount);
    event Redeem(address indexed from, address indexed to, uint256 amount);

    event Withdraw(address indexed liquidityPositionOwner, uint256 positionId);
    event LiquidateStake(address indexed liquidityPositionOwner, uint256 positionId);

    event RewardsAccrued(address user, uint256 amount);
    event RewardsClaimed(address indexed from, address indexed to, uint256 amount);

    event StartRewardsProjectFunded(uint128 previousEmissionPerSecond, uint128 extraEmissionsPerSecond, address lendingContractAddress);
    event EndRewardsProjectFunded(uint128 restoredEmissionsPerSecond, uint128 extraEmissionsPerSecond, address lendingContractAddress);

    modifier onlyGovernance() {
        require(hasRole(GOVERNANCE_ROLE, msg.sender), 'ONLY_GOVERNANCE');
        _;
    }

    modifier onlyEmissionManager() {
        require(hasRole(EMISSION_MANAGER_ROLE, msg.sender), 'ONLY_EMISSION_MANAGER');
        _;
    }

    modifier onlyOnStakingState() {
        require(state == OriginatorStakingState.STAKING, 'ONLY_ON_STAKING_STATE');
        _;
    }

    modifier notZeroAmount(uint256 _amount) {
        require(_amount > 0, 'INVALID_ZERO_AMOUNT');
        _;
    }

    function initialize(
        string memory _name,
        string memory _symbol,
        IERC20Upgradeable _lockedToken,
        IReserve _rewardsVault,
        address _emissionManager,
        uint128 _distributionDuration,
        IUniswapV3Pool _pool,
        INonfungiblePositionManager _nonFungiblePositionManager
    ) public initializer {
        __BaseTokenUpgradeable_init(
            msg.sender,
            0,
            _name,
            _symbol,
            _name
        );
        __StakingRewards_init(_emissionManager, _distributionDuration);
        TOKEN_TO_STAKE = _lockedToken;
        REWARDS_VAULT = _rewardsVault;
        POOL = _pool;
        LP_MANAGER = _nonFungiblePositionManager;
        _changeState(OriginatorStakingState.UNINITIALIZED);
    }

    /**
     * @notice Function to set up proposers (originator and auditor)
     * in proposal period.
     * @param _auditor address
     * @param _originator address
     * @param _auditorPercentage uint256 (value * 100 e.g. 20% == 2000)
     * @param _originatorPercentage uint256 (value * 100 e.g. 20% == 2000)
     * @param _stakingGoal uint256 wei amount in Ethix. IMPORTANT!! Only stakers amount without position amount
     * @param _defaultDelay uint256 seconds
     */
    function setUpTerms(
        address _auditor,
        address _originator,
        address _governance,
        uint256 _auditorPercentage,
        uint256 _originatorPercentage,
        uint256 _stakingGoal,
        uint256 _defaultDelay,
        uint256 _positionId
    ) external override notZeroAmount(_stakingGoal) onlyEmissionManager {
        require(_auditor != _originator, 'PROPOSERS_CANNOT_BE_THE_SAME');
        require(_auditorPercentage != 0 && _originatorPercentage != 0, 'INVALID_PERCENTAGE_ZERO');
        require(state == OriginatorStakingState.UNINITIALIZED, 'ONLY_ON_UNINITILIZED_STATE');

        _setupRole(AUDITOR_ROLE, _auditor);
        _setupRole(ORIGINATOR_ROLE, _originator);
        _setupRole(GOVERNANCE_ROLE, _governance);
        liquidityPositionOwner = INonfungiblePositionManager(LP_MANAGER).ownerOf(_positionId);

        _depositProposers(_auditorPercentage.add(_originatorPercentage), _stakingGoal, _positionId);
        stakingGoal = _stakingGoal;
        defaultDate = _defaultDelay.add(DISTRIBUTION_END);
        _changeState(OriginatorStakingState.STAKING);
    }

    /**
     * @notice Function to renew terms in STAKING_END or DEFAULT period.
     * @param _newAuditorPercentage uint256 (value * 100 e.g. 20% == 2000)
     * @param _newOriginatorPercentage uint256 (value * 100 e.g. 20% == 2000)
     * @param _newStakingGoal uint256 wei amount in Ethix. IMPORTANT!! Only stakers amount without position amoun
     * @param _newDistributionDuration uint128 seconds (e.g. 365 days == 31536000)
     * @param _newDefaultDelay uint256 seconds (e.g 90 days == 7776000)
     */
    function renewTerms(
        uint256 _newAuditorPercentage,
        uint256 _newOriginatorPercentage,
        uint256 _newStakingGoal,
        uint128 _newDistributionDuration,
        uint256 _newDefaultDelay,
        uint256 _positionId
    ) external override notZeroAmount(_newStakingGoal) onlyGovernance {
        require(state == OriginatorStakingState.STAKING_END || state == OriginatorStakingState.DEFAULT, 'INVALID_STATE');
        require(INonfungiblePositionManager(LP_MANAGER).ownerOf(_positionId) == address(this), 'INVALID_LP_TO_RENEW');
        DISTRIBUTION_END = block.timestamp.add(_newDistributionDuration);
        _depositProposers(_newAuditorPercentage.add(_newOriginatorPercentage), _newStakingGoal, _positionId);
        stakingGoal = _newStakingGoal;
        defaultDate = _newDefaultDelay.add(DISTRIBUTION_END);
        _changeState(OriginatorStakingState.STAKING);
    }

    /**
     * @notice Function to stake tokens
     * @param _onBehalfOf Address to stake to
     * @param _amount Amount to stake
     **/
    function stake(address _onBehalfOf, uint256 _amount) external override notZeroAmount(_amount) onlyOnStakingState {
        require(!hasReachedGoal(), 'GOAL_HAS_REACHED');

        if (TOKEN_TO_STAKE.balanceOf(address(this)).add(_amount) > stakingGoal) {
            _amount = stakingGoal.sub(TOKEN_TO_STAKE.balanceOf(address(this)));
        }
        uint256 balanceOfUser = balanceOf(_onBehalfOf);
        uint256 accruedRewards =
            _updateUserAssetInternal(_onBehalfOf, address(this), balanceOfUser, totalSupply());
        if (accruedRewards != 0) {
            emit RewardsAccrued(_onBehalfOf, accruedRewards);
            stakerRewardsToClaim[_onBehalfOf] = stakerRewardsToClaim[_onBehalfOf].add(accruedRewards);
        }

        _mint(_onBehalfOf, _amount);
        IERC20Upgradeable(TOKEN_TO_STAKE).safeTransferFrom(msg.sender, address(this), _amount);

        emit Staked(msg.sender, _onBehalfOf, _amount);
    }

    /**
     * @dev Redeems staked tokens, and stop earning rewards
     * @param _to Address to redeem to
     * @param _amount Amount to redeem
     **/
    function redeem(address _to, uint256 _amount) external override notZeroAmount(_amount) {
        require(_checkRedeemEligibilityState(), 'WRONG_STATE');
        require(balanceOf(msg.sender) != 0, 'SENDER_BALANCE_ZERO');

        uint256 balanceOfMessageSender = balanceOf(msg.sender);

        uint256 amountToRedeem =
            (_amount > balanceOfMessageSender) ? balanceOfMessageSender : _amount;

        _updateCurrentUnclaimedRewards(msg.sender, balanceOfMessageSender, true);

        _burn(msg.sender, amountToRedeem);

        IERC20Upgradeable(TOKEN_TO_STAKE).safeTransfer(_to, amountToRedeem);

        emit Redeem(msg.sender, _to, amountToRedeem);
    }

    /**
     * @notice method to withdraw deposited amount.
     * @param _positionId Position ID to withdraw
     */
    function withdrawProposerStake(uint256 _positionId) external override {
        require(state == OriginatorStakingState.STAKING_END, 'ONLY_ON_STAKING_END_STATE');
        require(msg.sender == liquidityPositionOwner, 'WITHDRAW_PERMISSION_DENIED');
        require(INonfungiblePositionManager(LP_MANAGER).ownerOf(_positionId) == address(this), 'INVALID_LP_TO_WITHDRAW');

        INonfungiblePositionManager(LP_MANAGER).safeTransferFrom(address(this), liquidityPositionOwner, _positionId);
        emit Withdraw(liquidityPositionOwner, _positionId);
    }

    /**
     * @dev Claims an `amount` from Rewards reserve to the address `to`
     * @param _to Address to stake for
     * @param _amount Amount to stake
     **/
    function claimRewards(address payable _to, uint256 _amount) external override {
        uint256 newTotalRewards = _updateCurrentUnclaimedRewards(msg.sender, balanceOf(msg.sender), false);
        uint256 amountToClaim = (_amount == type(uint256).max) ? newTotalRewards : _amount;

        stakerRewardsToClaim[msg.sender] = newTotalRewards.sub(amountToClaim, 'INVALID_AMOUNT');
        require(REWARDS_VAULT.transfer(_to, amountToClaim), 'ERROR_TRANSFER_FROM_VAULT');

        emit RewardsClaimed(msg.sender, _to, amountToClaim);
    }

    /**
     * Function to add an extra emissions per second corresponding to staker rewards when a lending project by this originator
     * is funded.
     * @param _extraEmissionsPerSecond  emissions per second to be added to current ones.
     * @param _lendingContractAddress lending contract address is relationated with this rewards
     */
    function startProjectFundedRewards(uint128 _extraEmissionsPerSecond, address _lendingContractAddress) external override onlyOnStakingState {
        AssetData storage currentDistribution = assets[address(this)];
        uint128 currentEmission = currentDistribution.emissionPerSecond;

        uint128 newEmissionsPerSecond = currentDistribution.emissionPerSecond.add(_extraEmissionsPerSecond);
        DistributionTypes.AssetConfigInput[] memory newAssetConfig = new DistributionTypes.AssetConfigInput[](1);
        newAssetConfig[0] = DistributionTypes.AssetConfigInput({
            emissionPerSecond: newEmissionsPerSecond,
            totalStaked: totalSupply(),
            underlyingAsset: address(this)
        });
        configureAssets(newAssetConfig);

        emit StartRewardsProjectFunded(currentEmission, _extraEmissionsPerSecond, _lendingContractAddress);
    }

    /**
     * Function to end extra emissions per second corresponding to staker rewards when a lending project by this originator
     * is funded.
     * @param _extraEmissionsPerSecond  emissions per second to be added to current ones.
     * @param _lendingContractAddress lending contract address is relationated with this rewards.
     */
    function endProjectFundedRewards(uint128 _extraEmissionsPerSecond, address _lendingContractAddress) external override onlyOnStakingState {
        AssetData storage currentDistribution = assets[address(this)];
        uint128 currentEmission = currentDistribution.emissionPerSecond;
        uint128 newEmissionsPerSecond = currentDistribution.emissionPerSecond.sub(_extraEmissionsPerSecond);
        DistributionTypes.AssetConfigInput[] memory newAssetConfig = new DistributionTypes.AssetConfigInput[](1);
        newAssetConfig[0] = DistributionTypes.AssetConfigInput({
            emissionPerSecond: newEmissionsPerSecond,
            totalStaked: totalSupply(),
            underlyingAsset: address(this)
        });
        configureAssets(newAssetConfig);
        emit EndRewardsProjectFunded(currentEmission, _extraEmissionsPerSecond, _lendingContractAddress);
    }

    /**
     * @notice Amount to substract of the contract when state is default
     * @param _positionId amount to substract
     */
    function liquidateProposerStake(uint256 _positionId) external override onlyGovernance {
        require(state == OriginatorStakingState.DEFAULT, 'ONLY_ON_DEFAULT');
        require(INonfungiblePositionManager(LP_MANAGER).ownerOf(_positionId) == address(this), 'INVALID_LP_TO_LIQUIDATE');
        INonfungiblePositionManager(LP_MANAGER).safeTransferFrom(address(this), msg.sender, _positionId);
        emit LiquidateStake(liquidityPositionOwner, _positionId);
    }

    /**
     * @notice Function to declare contract on staking end state
     * Only governance could change to this state
     **/
    function declareStakingEnd() external override onlyGovernance onlyOnStakingState {
        _endDistributionIfNeeded();
        _changeState(OriginatorStakingState.STAKING_END);
    }

    /**
     * @notice Function to declare as DEFAULT
     * @param _defaultedAmount uint256
     **/
    function declareDefault(uint256 _defaultedAmount) external override onlyGovernance onlyOnStakingState {
        require(block.timestamp >= defaultDate, 'DEFAULT_DATE_NOT_REACHED');
        defaultedAmount = _defaultedAmount;
        _endDistributionIfNeeded();
        _changeState(OriginatorStakingState.DEFAULT);
    }

    /**
     * @dev Return the total rewards pending to claim by an staker
     * @param _staker The staker address
     * @return The rewards
     */
    function getTotalRewardsBalance(address _staker) external override view returns (uint256) {
        DistributionTypes.UserStakeInput[] memory userStakeInputs =
            new DistributionTypes.UserStakeInput[](1);
        userStakeInputs[0] = DistributionTypes.UserStakeInput({
            underlyingAsset: address(this),
            stakedByUser: balanceOf(_staker),
            totalStaked: totalSupply()
        });
        return stakerRewardsToClaim[_staker].add(_getUnclaimedRewards(_staker, userStakeInputs));
    }

    /// @notice Upon receiving a Uniswap V3 ERC721, creates the token deposit setting owner to `from`. Also stakes token
    /// in one or more incentives if properly formatted `data` has a length > 0.
    /// @inheritdoc IERC721ReceiverUpgradeable
    function onERC721Received(
        address,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) external override returns (bytes4) {
        require(
            msg.sender == address(INonfungiblePositionManager(LP_MANAGER)),
            'NOT_A_UNIV3_NFT'
        );
        return this.onERC721Received.selector;
    }

    /**
     * @notice Check if fulfilled the objective (Only valid on STAKING state!!)
     */
    function hasReachedGoal() public override notZeroAmount(stakingGoal) view returns (bool) {
        if (totalSupply() >= stakingGoal) {
            return true;
        }
        return  false;
    }

    /**
     * @notice Function to transfer participation amount (originator or auditor)
     */
    function _depositProposers(uint256 _percentage, uint256 _goalAmount, uint256 _positionId) internal {
        uint256 totalAmount = _goalAmount.mul(10000).div(10000 - _percentage);
        uint256 proposersAmount = _calculatePercentage(totalAmount, _percentage);
        depositAmount = _calculatePositionAmount(_positionId);
        require(proposersAmount <= depositAmount.add(ERROR_MARGIN), 'POSITION_AMOUNT_IS_INSUFFICIENT');
        address owner = INonfungiblePositionManager(LP_MANAGER).ownerOf(_positionId);
        if (owner != address(this)) {
            INonfungiblePositionManager(LP_MANAGER).safeTransferFrom(owner, address(this), _positionId);
        }
    }

    /**
     * @dev Internal ERC20 _transfer of the tokenized staked tokens
     * @param _from Address to transfer from
     * @param _to Address to transfer to
     * @param _amount Amount to transfer
     **/
    function _transfer(
        address _from,
        address _to,
        uint256 _amount
    ) internal override {
        uint256 balanceOfFrom = balanceOf(_from);
        // Sender
        _updateCurrentUnclaimedRewards(_from, balanceOfFrom, true);

        // Recipient
        if (_from != _to) {
            uint256 balanceOfTo = balanceOf(_to);
            _updateCurrentUnclaimedRewards(_to, balanceOfTo, true);
        }

        super._transfer(_from, _to, _amount);
    }

    /**
     * @dev Check if the state of contract is suitable to redeem
     */
    function _checkRedeemEligibilityState() internal view returns (bool) {
        if (state == OriginatorStakingState.STAKING_END) {
            return true;
        } else if (state == OriginatorStakingState.DEFAULT && defaultedAmount <= depositAmount) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @dev Updates the user state related with his accrued rewards
     * @param _user Address of the user
     * @param _userBalance The current balance of the user
     * @param _updateStorage Boolean flag used to update or not the stakerRewardsToClaim of the user
     * @return The unclaimed rewards that were added to the total accrued
     **/
    function _updateCurrentUnclaimedRewards(
        address _user,
        uint256 _userBalance,
        bool _updateStorage
    ) internal returns (uint256) {
        uint256 accruedRewards =
            _updateUserAssetInternal(_user, address(this), _userBalance, totalSupply());
        uint256 unclaimedRewards = stakerRewardsToClaim[_user].add(accruedRewards);

        if (accruedRewards != 0) {
            if (_updateStorage) {
                stakerRewardsToClaim[_user] = unclaimedRewards;
            }
            emit RewardsAccrued(_user, accruedRewards);
        }

        return unclaimedRewards;
    }

    /**
     * @notice Function to calculate a percentage of an amount
     * @param _amount Amount to calculate the percentage of
     * @param _percentage Percentage to calculate of this amount
     * @return (amount)
     */
    function _calculatePercentage(uint256 _amount, uint256 _percentage) internal pure returns (uint256) {
        return uint256(_amount.mul(_percentage).div(10000));
    }

    /**
     * @notice Function to get amount of tokens on position
     * @param _positionId position ID
     */
    function _calculatePositionAmount(uint256 _positionId) internal view returns (uint256){
        (,,address token0,address token1,,int24 tickLower,int24 tickUpper,uint128 liquidity,,,,) = INonfungiblePositionManager(LP_MANAGER).positions(_positionId);
        require(tickLower <= MIN_TICK, 'MIN_TICK_MUCH_BE_WHOLE_RANGE_OF_POOL');
        require(tickUpper >= MAX_TICK, 'MAX_TICK_MUCH_BE_WHOLE_RANGE_OF_POOL');
        require(token0 == address(IERC20Upgradeable(TOKEN_TO_STAKE)) || token1 == address(IERC20Upgradeable(TOKEN_TO_STAKE)), 'POSITION_DO_NOT_HAVE_TOKEN_TO_STAKE');

        (uint160 sqrtRatioX96,,,,,,) = IUniswapV3Pool(POOL).slot0();
        (uint256 amount0, uint256 amount1) = LiquidityAmounts.getAmountsForLiquidity(
            sqrtRatioX96,
            TickMath.getSqrtRatioAtTick(tickLower),
            TickMath.getSqrtRatioAtTick(tickUpper),
            liquidity
        );
        if (token0 == address(IERC20Upgradeable(TOKEN_TO_STAKE))){
            return amount0.mul(2);
        } else {
            return amount1.mul(2);
        }
    }

    /**
     * @notice Function to change contract state
     * @param _newState New contract state
     **/
    function _changeState(OriginatorStakingState _newState) internal {
        state = _newState;
        emit StateChange(uint256(_newState));
    }

    /**
     * @notice Function to change DISTRIBUTION_END if timestamp is less than the initial one
     **/
    function _endDistributionIfNeeded() internal {
        if (block.timestamp <= DISTRIBUTION_END) {
            _changeDistributionEndDate(block.timestamp);
        }
    }
}
