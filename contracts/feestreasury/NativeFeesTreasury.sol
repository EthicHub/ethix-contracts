// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import './FeesTreasury.sol';
import '../reserve/IReserve.sol';
import '../reserve/NativeReserve.sol';
import '@openzeppelin/contracts/utils/ReentrancyGuard.sol';

/**
 * @title NativeFeesTreasury
 * @author Ethichub
 */
contract NativeFeesTreasury is FeesTreasury {
    function initialize(uint256 _minPeriodDistribution, address payable _recipient)
        public
        override
        initializer
    {
        __Ownable_init();
        super.initialize(_minPeriodDistribution, _recipient);
        reserve = new NativeReserve();
        lastDistribution = block.timestamp;
    }
}
