// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import '../reserve/IReserve.sol';

import '@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol';
import '@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/proxy/Initializable.sol';

/**
 * @title FeesTreasury
 * @author Ethichub
 */
abstract contract FeesTreasury is Initializable, OwnableUpgradeable {
    uint256 public lastDistribution;
    uint256 public minPeriodDistribution;
    uint256 constant MAX_PERIOD_DISTRIBUTION = 5259600; //2 months n seconds

    address payable public recipient;

    IReserve public reserve;

    event Distribute(uint256 distributionDate);

    function initialize(uint256 _minPeriodDistribution, address payable _recipient)
        public
        virtual
        initializer
    {
        __Ownable_init();
        setMinPeriodDistribution(_minPeriodDistribution);
        setRecipient(_recipient);
    }

    function distribute() external onlyOwner {
        require(
            block.timestamp >= lastDistribution + minPeriodDistribution,
            'FeesTreasury: Not enough time passed since last distribution'
        );
        require(address(reserve).balance > 0, 'FeesTreasury: No token balance to transfer');

        lastDistribution = block.timestamp;
        reserve.transfer(recipient, address(reserve).balance);

        emit Distribute(block.timestamp);
    }

    function setMinPeriodDistribution(uint256 _minPeriodDistribution) public onlyOwner {
        require(
            _minPeriodDistribution <= MAX_PERIOD_DISTRIBUTION,
            'FeesTreasury: _minPeriodDistribution bigger than MAX_PERIOD_DISTRIBUTION'
        );
        minPeriodDistribution = _minPeriodDistribution;
    }

    function setRecipient(address payable _recipient) public onlyOwner {
        require(_recipient != address(0), 'FeesTreasury: Invalid recipient');
        recipient = _recipient;
    }
}
