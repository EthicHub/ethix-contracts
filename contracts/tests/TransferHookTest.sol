// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import '../token/bases/staking/interfaces/ITransferHook.sol';

contract TransferHookTest is ITransferHook {
    event MockHookEvent();

    function onTransfer(
        address from,
        address to,
        uint256 amount
    ) external override {
        emit MockHookEvent();
    }
}
