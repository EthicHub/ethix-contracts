// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import '@openzeppelin/contracts-upgradeable/proxy/Initializable.sol';
import '../token/ERCs/ERC677/IERC677Receiver.sol';

contract ERC677ReceiverTest is IERC677Receiver {
    address public from;
    uint256 public amount;
    bytes public data;

    function onTokenTransfer(
        address _from,
        uint256 _amount,
        bytes calldata _data
    ) external override returns (bool) {
        from = _from;
        amount = _amount;
        data = _data;
        return true;
    }
}
