// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import '@openzeppelin/contracts-upgradeable/proxy/Initializable.sol';
import '../token/ERCs/ERC677/ERC677Upgradeable.sol';

contract ERC677Test is Initializable, ERC677Upgradeable {
    function __ERC677Test_init() public initializer {
        __ERC677_init(msg.sender, 100 * 10**18, 'Test ERC677', 'TERC677');
    }
}
