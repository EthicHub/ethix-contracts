// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import '@openzeppelin/contracts/token/ERC20/IERC20.sol';

contract BPoolTest {
    uint256 spotPrice = 100;

    function swapExactAmountIn(
        address,
        uint256,
        address _tokenOut,
        uint256 _tokenAmountOut,
        uint256
    ) public returns (uint256, uint256) {
        IERC20(_tokenOut).transfer(msg.sender, _tokenAmountOut);
        return (_tokenAmountOut, spotPrice);
    }

    function swapExactAmountOut(
        address,
        uint256,
        address _tokenOut,
        uint256 _tokenAmountOut,
        uint256
    ) public returns (uint256, uint256) {
        IERC20(_tokenOut).transfer(msg.sender, _tokenAmountOut);
        return (_tokenAmountOut, spotPrice);
    }

    function getSpotPrice(address, address) public view returns (uint256) {
        return spotPrice;
    }

    function setSpotPrice(uint256 _spotPrice) public {
        spotPrice = _spotPrice;
    }
}
