// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import '../reserve/VestingReserve.sol';

/**
 * @title VestingReserveTest
 * @author Ethichub
 */
contract VestingReserveTest is VestingReserve {
    constructor(
        IERC20 _token,
        uint256 _startTime,
        uint256 _endTime,
        uint256 _editAddressUntil
    ) VestingReserve(_token, _startTime, _endTime, _editAddressUntil) {}

    function initialize() external override {
        require(!initialized, 'VestingReserve: Already initialized');
        require(
            token.transferFrom(msg.sender, address(this), 1000000000000000000),
            'VestingReserve: Cannot transfer tokens from sender.'
        );
        locked[0x70997970C51812dc3A010C7d01b50e0d17dc79C8] = 500000000000000000;
        locked[0x3C44CdDdB6a900fa2b585dd299e03d12FA4293BC] = 500000000000000000;

        initialized = true;
    }
}
