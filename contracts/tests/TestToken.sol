// SPDX-License-Identifier: gpl-3.0

pragma solidity 0.7.5;

import '@openzeppelin/contracts/token/ERC20/ERC20.sol';

contract TestToken is ERC20 {
    constructor() ERC20('TestToken', 'TestToken') {
        _mint(msg.sender, 100000000 * 10**18);
    }
}
