const { task } = require('hardhat/config')
const { promptForApproval, promptContractSelection } = require('../utils/promptUtils')
const DeployResultWriter = require('../utils/deployResultWriter')
const calculateTokensPerSecond = require('../utils/rewardCalculator')


task("configure-rewards", "Configure Rewards on Staking Token")
.addParam('tokensDay', 'Tokens per day in Ether units')
.setAction(async ({ tokensDay } ) => {

  const ethers = hre.ethers
  const writer = new DeployResultWriter(hre.network.name)
  const deployedAs = await promptContractSelection(writer)
  const deploymentData = writer.currentDeployment[deployedAs]
  if (!deploymentData) {
    throw new Error(`${deployedAs} does not exist in .deployment for network ${hre.network.name}`)
  }
  const stkTokenAddress = deploymentData.address
  console.log('Connecting to contracts...')
  console.log(deploymentData.address)
  const StakedToken = await ethers.getContractFactory('StakedToken')
  const stkToken = await StakedToken.attach(stkTokenAddress)

  const emissionPerSecond = calculateTokensPerSecond(tokensDay)
  await promptForApproval(`You will set ${tokensDay} ETHIX per day rewards for contract ${stkTokenAddress}`)

  console.log('tokens per day: ' + tokensDay)
  const totalStaked = await stkToken.totalSupply.call()
  console.log('total Staked: ' + ethers.utils.formatEther(totalStaked))

  console.log('Configuring assets...')
  const config = {
    emissionPerSecond: emissionPerSecond,
    totalStaked: totalStaked.toString(),
    underlyingAsset: stkTokenAddress,
  }
  console.log(config)
  const tx = await stkToken.configureAssets([config])
  await tx.wait()
  console.log(tx)
  writer.updateContract(deployedAs, 'emissionsPerDay', tokensDay)
  console.log('Configured')

});

module.exports = {}