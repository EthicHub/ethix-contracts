const { task } = require('hardhat/config')
const DeployResultWriter = require('../utils/deployResultWriter')
const { promptForApproval, promptForDate, promptOriginatorData, promptContractSelection} = require('../utils/promptUtils')
const { DateTime, Interval } = require('luxon')


task('originator:deploy', 'Deploys Originator Staking contract')
.addParam('name', 'OriginatorStaking ERC20 name')
.addParam('symbol', 'OriginatorStaking ERC20 symbol')
.addParam('emissionManager', 'Emission Manager address')
.setAction(async ({ name, symbol, emissionManager }, hre ) => {
  const ethers = hre.ethers
  console.log('Connecting to network...', hre.network.name)
  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const rewardsVaultAddress = writer.currentDeployment.RewardsReserve.address
  const originatorFactoryAddress = writer.currentDeployment.OriginatorStakingFactory.address
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const rewardsVault = await ERC20Reserve.attach(rewardsVaultAddress)
  const OriginatorFactory = await ethers.getContractFactory('OriginatorStakingFactory')
  const originatorFactory = await OriginatorFactory.attach(originatorFactoryAddress)
  console.log('Connected!')
  const dateString = await promptForDate('Pick a UTC date for ETHIX rewards end (will calculate distributionDuration)')
  const distributionEndDate = DateTime.fromISO(dateString.toISOString()).toUTC()
  const now = DateTime.now().toUTC()
  const duration = Interval.fromDateTimes(now, distributionEndDate)
  const distributionDuration = duration.length('seconds').toFixed(0)

  await promptForApproval(`

    You are going to create an OriginatorStaking contracts with the following config:
    name: ${name}
    symbol: ${symbol}
    emissionManager: ${emissionManager}
    distributionDuration: ${distributionDuration} seconds (${duration.length('days')} days)
    distribution end date (might change, deppends on tx approval): ${distributionEndDate.toString()} 

  `)
  console.log('Deploying originatorStk...')
  const tx = await originatorFactory.createOriginator(
      name,
      symbol,
      ethixAddress,
      rewardsVaultAddress,
      emissionManager,
      distributionDuration
  )
  const receipt = await tx.wait()

  console.log(`Originator Staking '${name}' deployed to: ${receipt.events[3].args.originator}`)
  writer.addContract(symbol, receipt.events[3].args.originator, true)
  writer.updateContract(symbol, 'emissionManagerAddress', emissionManager)
  writer.updateContract(symbol, 'distributionDuration', distributionDuration)

  //Assign originatorStk as TRANSFER_ROLE in Rewards reserve in mainnet or celo
  //For other networks assign it manually!!!
  if (hre.network.name == 'mainnet') {
    console.log('Enabling ETHIX rewards...')
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStk.address)
    console.log(`${symbol} is TRANSFER_ROLE for ${rewardsVault.address}`)
  }
});

task('originator:approve', 'Approve Originator and Auditor for the OriginatorS')
.setAction(async ({ originatorStakingAddress, symbol }, hre ) => {
  const ethers = hre.ethers
  const writer = new DeployResultWriter(hre.network.name)
  const originatorStakingSymbol = await promptContractSelection(writer)
  const originatorStakingData = writer.currentDeployment[originatorStakingSymbol]

  //Attention!! Make sure to put in order the pk in secret, and proposers have balance to pay gas
  var [owner, originator, auditor] = await ethers.getSigners()
  console.log('Connecting to contracts...', originatorStakingData.address)
  const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
  const originatorStaking = await OriginatorStaking.attach(originatorStakingData.address)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Connected')

  if (hre.network.name == 'hardhat') {
    // Send 1 native coin to auditor and originator
    console.log('Testnet deploy: '+hre.network.name)
    console.log('originatorAddress: ', originator.address)
    console.log('auditorAddress: ', auditor.address)

    var tx = null
    tx = await owner.sendTransaction({to:auditor.address, value:ethers.utils.parseEther('1')})
    await tx.wait()
    console.log('Tx sending to auditor: ' + tx.hash)
    tx = await owner.sendTransaction({to:originator.address, value:ethers.utils.parseEther('1')})
    await tx.wait()
    console.log('Tx sending to originator: ' + tx.hash)
    console.log('Transferred native coin to auditor and originator.')
  }

  console.log('Configuring originator approve: ' + originator.address)
  var tx = null
  tx = await ethix.connect(originator).approve(originatorStaking.address, ethers.constants.MaxUint256)
  await tx.wait()
  console.log('Tx Originator approve: ' + tx.hash)

  console.log('Configuring auditor approve: ' + auditor.address)
  tx = await ethix.connect(auditor).approve(originatorStaking.address, ethers.constants.MaxUint256)
  await tx.wait()
  console.log('Tx Auditor approve: ' + tx.hash)
  console.log('Configured approves.')
});

task('originator:setup', 'Setups Originator skin in the game and period')
//.addParam('originatorStakingAddress', 'Ethereum address of the deployed OriginatorStaking')
//.addParam('symbol', 'OriginatorStaking ERC20 symbol')
.setAction(async ({ originatorStakingAddress, symbol }, hre ) => {
  const ethers = hre.ethers
  const writer = new DeployResultWriter(hre.network.name)
  const originatorStakingSymbol = await promptContractSelection(writer)
  const originatorStakingData = writer.currentDeployment[originatorStakingSymbol]
  var [owner] = await ethers.getSigners()
  console.log('Connecting to contracts...', originatorStakingData.address)
  const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
  const originatorStaking = await OriginatorStaking.attach(originatorStakingData.address)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Connected')
  // TODO get from prompt
  const originatorData = await promptOriginatorData()
  const {
    auditorPercentage,
    originatorPercentage,
    stakingGoal,
    defaultDelay,
  } = originatorData

  var {
    auditorAddress,
    originatorAddress,
    governanceAddress
  } = originatorData

  var message = '\n\nYou will configure Originator Staking with the following parameters: \n\n'
  Object.keys(originatorData).forEach( key => {
    message = `${message}
    ${key}: ${originatorData[key]}`
  })
  message = message + '\n\n'
  await promptForApproval(message)

  console.log('\nConfiguring setupTerms originatorStaking...')
  
  const setupTx = await originatorStaking.setUpTerms(
    auditorAddress,
    originatorAddress,
    governanceAddress,
    auditorPercentage,
    originatorPercentage,
    stakingGoal,
    defaultDelay
  )
  await setupTx.wait()
  console.log(setupTx)
  
  // TODO update terms in writer
  Object.keys(originatorData).forEach( key => {
    writer.updateContract(originatorStakingSymbol, key, originatorData[key])
  })
  
  console.log('Configured!!')
});


module.exports = {}