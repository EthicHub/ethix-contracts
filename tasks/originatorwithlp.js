/*
    Task to Originator Staking With LP (deploy, approve and setup).

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


const { task } = require('hardhat/config')
const DeployResultWriter = require('../utils/deployResultWriter')
const { promptForApproval, promptForDate, promptOriginatorWithLPData, promptContractSelection} = require('../utils/promptUtils')
const { DateTime, Interval } = require('luxon')
const { BigNumber, ContractFactory } = require('ethers')
const NFT_POSITION_MANAGER = require('@uniswap/v3-periphery/artifacts/contracts/NonfungiblePositionManager.sol/NonfungiblePositionManager.json')


task('originatorwithlp:deploy', 'Deploys Originator Staking With LP contract')
.addParam('name', 'OriginatorStakingWithLP ERC20 name')
.addParam('symbol', 'OriginatorStakingWithLP ERC20 symbol')
.addParam('emissionManager', 'Emission Manager address')
.setAction(async ({ name, symbol, emissionManager }, hre ) => {
  const ethers = hre.ethers
  console.log('Connecting to network...', hre.network.name)
  const writer = new DeployResultWriter(hre.network.name)
  console.log('Connecting to contracts...')
  const ethixAddress = writer.currentDeployment.EthixToken.address
  const uniV3PoolAddress = writer.currentDeployment.UniswapV3Pool.address
  const nftPositionManagerAddress = writer.currentDeployment.NonfungiblePositionManager.address
  const rewardsVaultAddress = writer.currentDeployment.RewardsReserve.address
  const originatorFactoryAddress = writer.currentDeployment.OriginatorStakingWithLPFactory.address
  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  const rewardsVault = await ERC20Reserve.attach(rewardsVaultAddress)
  const OriginatorFactory = await ethers.getContractFactory('OriginatorStakingWithLPFactory')
  const originatorFactory = await OriginatorFactory.attach(originatorFactoryAddress)
  console.log('Connected!')
  const dateString = await promptForDate('Pick a UTC date for ETHIX rewards end (will calculate distributionDuration)')
  const distributionEndDate = DateTime.fromISO(dateString.toISOString()).toUTC()
  const now = DateTime.now().toUTC()
  const duration = Interval.fromDateTimes(now, distributionEndDate)
  const distributionDuration = duration.length('seconds').toFixed(0)

  await promptForApproval(`

    You are going to create an OriginatorStakingWithLP contracts with the following config:
    name: ${name}
    symbol: ${symbol}
    emissionManager: ${emissionManager}
    distributionDuration: ${distributionDuration} seconds (${duration.length('days')} days)
    distribution end date (might change, deppends on tx approval): ${distributionEndDate.toString()}

  `)
  console.log('Deploying originatorStkWithLP...')
  const tx = await originatorFactory.createOriginatorWithLP(
    name,
    symbol,
    ethixAddress,
    rewardsVaultAddress,
    emissionManager,
    distributionDuration,
    uniV3PoolAddress,
    nftPositionManagerAddress
  )
  const receipt = await tx.wait()

  console.log(`Originator Staking '${name}' deployed to: ${receipt.events[3].args.originator}`)
  writer.addContract(symbol, receipt.events[3].args.originator, true)
  writer.updateContract(symbol, 'emissionManagerAddress', emissionManager)
  writer.updateContract(symbol, 'distributionDuration', distributionDuration)

  //Assign originatorStkWithLP as TRANSFER_ROLE in Rewards reserve in mainnet or celo
  //For other networks assign it manually!!!
  if (hre.network.name == 'mainnet' || hre.network.name == 'celo') {
    console.log('Enabling ETHIX rewards...')
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStkWithLP.address)
    console.log(`${symbol} is TRANSFER_ROLE for ${rewardsVault.address}`)
  }
});

task('originatorwithlp:approve', 'Approve transfer nftPostion to OriginatorStaking contract')
.addParam('nftPositionId', 'OriginatorStakingWithLP LiquidityPostion ID ')
.setAction(async ({ nftPositionId }, hre ) => {
  const ethers = hre.ethers
  const writer = new DeployResultWriter(hre.network.name)
  const originatorStakingWithLPSymbol = await promptContractSelection(writer)
  const originatorStakingWithLPData = writer.currentDeployment[originatorStakingWithLPSymbol]

  //Attention!! Make sure to put in order the pk in secret, and proposers have balance to pay gas
  var [deployer, ownerLP] = await ethers.getSigners()
  console.log('Connecting to contracts...', originatorStakingWithLPData.address)
  const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
  const originatorStakingWithLP = await OriginatorStakingWithLP.attach(originatorStakingWithLPData.address)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Connected')

  const nftPositionManagerAddress = writer.currentDeployment.NonfungiblePositionManager.address
  const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, ownerLP)
  const nftPositionManager = await NFTPositionManager.attach(nftPositionManagerAddress)

  if (ownerLP.address == await nftPositionManager.ownerOf(nftPositionId)) {
    console.log('Configuring ownerLP to approve: ' + ownerLP.address)
    var tx = null
    tx = await nftPositionManager.connect(ownerLP).approve(originatorStakingWithLP.address, nftPositionId)
    await tx.wait()
    console.log('Tx ownerLP approve: ' + tx.hash)
    console.log('Configured approves.')
  } else {
    console.log('Should configure ownerLP on signers to approve transfer nftPostion to OriginatorStaking contract')
    console.log('NFT owner is: '+ await nftPositionManager.ownerOf(nftPositionId))
    console.log('Signer ownerLP: '+ ownerLP.address)
  }
});

task('originatorwithlp:setup', 'Setups Originator skin in the game and period')
.setAction(async ({}, hre ) => {
  const ethers = hre.ethers
  const writer = new DeployResultWriter(hre.network.name)
  const originatorStakingWithLPSymbol = await promptContractSelection(writer)
  const originatorStakingWithLPData = writer.currentDeployment[originatorStakingWithLPSymbol]
  var [owner] = await ethers.getSigners()
  console.log('Connecting to contracts...', originatorStakingWithLPData.address)
  const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
  const originatorStakingWithLP = await OriginatorStakingWithLP.attach(originatorStakingWithLPData.address)

  const ethixAddress = writer.currentDeployment.EthixToken.address
  const EthixToken = await ethers.getContractFactory('EthixToken')
  const ethix = await EthixToken.attach(ethixAddress)
  console.log('Connected')

  // TODO get from prompt
  const originatorData = await promptOriginatorWithLPData()
  const {
    auditorPercentage,
    originatorPercentage,
    stakingGoal,
    defaultDelay,
    nftPositionId
  } = originatorData

  var {
    auditorAddress,
    originatorAddress,
    governanceAddress,
  } = originatorData

  auditorAmount = BigNumber.from(stakingGoal).mul(auditorPercentage).div(10000)
  originatorAmount = BigNumber.from(stakingGoal).mul(originatorPercentage).div(10000)

  var message = '\n\nYou will configure Originator Staking with the following parameters: \n\n'
  Object.keys(originatorData).forEach( key => {
    message = `${message}
    ${key}: ${originatorData[key]}`
  })
  message = message + '\n\n'
  await promptForApproval(message)

  console.log('\nConfiguring setupTerms originatorStakingWithLP...')

  const setupTx = await originatorStakingWithLP.setUpTerms(
    auditorAddress,
    originatorAddress,
    governanceAddress,
    auditorPercentage,
    originatorPercentage,
    BigNumber.from(stakingGoal).sub(auditorAmount.add(originatorAmount)),
    defaultDelay,
    nftPositionId
  )
  await setupTx.wait()
  console.log(setupTx)

  // TODO update terms in writer
  Object.keys(originatorData).forEach( key => {
    writer.updateContract(originatorStakingWithLPSymbol, key, originatorData[key])
  })

  console.log('Configured!!')
});


module.exports = {}