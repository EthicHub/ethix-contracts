const { ethers } = require('ethers')
const { task } = require('hardhat/config')

require('@nomiclabs/hardhat-waffle')
require('@openzeppelin/hardhat-upgrades')
require('@tenderly/hardhat-tenderly')
require("@nomiclabs/hardhat-etherscan")
require("@nomiclabs/hardhat-solhint")
require('./tasks/configure-rewards')
require('./tasks/originator')
require('./tasks/originatorwithlp')

let secrets = require('./.secrets.json')

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task('accounts', 'Prints the list of accounts', async () => {
  const accounts = await ethers.getSigners()

  for (const account of accounts) {
    console.log(account.address)
  }
})


// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.6.8",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
      },
      {
        version: "0.7.5",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
      },
      {
        version: "0.7.6",
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
      }
    ]
  },
  tenderly: {
    username: 'EthicHubTeam',
    project: 'ethix',
  },
  networks: {
    goerli: {
      url: secrets.goerli.node_url,
      chainId: 5,
      accounts: {
        mnemonic: secrets.goerli.mnemonic
      },
      gasPrice: 50000000000, // 50Gwei
    },
    sepolia: {
      url: secrets.sepolia.node_url,
      chainId: 11155111,
      accounts: {
        mnemonic: secrets.sepolia.mnemonic
      },
      gasMultiplier: 2.5
    },
    chiado: {
      url: secrets.chiado.node_url,
      chainId: 10200,
      accounts: {
        mnemonic: secrets.chiado.mnemonic
      },
      gasPrice: 50000000000, // 50Gwei
    },
    alfajores: {
      url: secrets.alfajores.node_url,
      chainId: 44787,
      accounts: {
        mnemonic: secrets.alfajores.mnemonic
      },
      gasPrice: 10000000000, // 10Gwei
    },
    xdai: {
      url: secrets.gnosis.node_url,
      chainId: 100,
      accounts: secrets.gnosis.pks,
      gasPrice: 1000000000, // 1Gwei
    },
    mainnet: {
      url: secrets.mainnet.node_url,
      chainId: 1,
      accounts: secrets.mainnet.pks,
      gasMultiplier: 2.5
    },
    celo: {
      url: secrets.celo.node_url,
      chainId: 42220,
      accounts: secrets.celo.pks,
      gasPrice: 10000000000, // 10Gwei
    },
  },
  etherscan: {
    apiKey: {
      mainnet: secrets.mainnet.api_key,
      goerli: secrets.goerli.api_key,
      sepolia: secrets.sepolia.api_key,
      celo: secrets.celo.api_key,
      alfajores: secrets.alfajores.api_key,
      chiado: secrets.chiado.api_key
    },
    customChains: [
      {
        network: "alfajores",
        chainId: 44787,
        urls: {
          apiURL: "https://api-alfajores.celoscan.io/api",
          browserURL: "https://alfajores.celoscan.io"
        }
      },
      {
        network: "celo",
        chainId: 42220,
        urls: {
          apiURL: "https://api.celoscan.io/api",
          browserURL: "https://celoscan.io"
        }
      },
      {
        network: "chiado",
        chainId: 10200,
        urls: {
          apiURL: "https://blockscout.com/gnosis/chiado/api",
          browserURL: "https://blockscout.com/gnosis/chiado"
        }
      }
    ]
  }
}
