![logo](https://www.ethichub.com/hubfs/EthicHub%20template/Logo%20(4).svg)

# Repo for Ethix Token contracts and Compensation System for [EthicHub](https://ethichub.com)

Smart Contracts for 
- Ethix Token 
- Vesting contracts and Reserves
- EthicHub's Compensation System 


The Compensation System is designed to:
- Receive a percentage of the lending projects from the crowdlending platform, buy ETHIX in the system's Balancer Pool and stake them in the system, which will reduce circulating supply.
- If needed, sell part of the staked Ethix in said pool to cover for lending project's defaults
- Allow users to participate in the system by Staking Ethix or other tokens (Liqudity Mining)

Active at https://ethix.ethichub.com

Docs: https://docs-ethix.ethichub.com/v/english/


## Development

- Run prettier with `npm run prettier`

## Deployment

- Run hardhat scripts in script folder in order. Results in .deployment folder