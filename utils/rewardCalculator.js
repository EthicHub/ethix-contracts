const { parseEther } = require('ethers/lib/utils')

module.exports = (tokensPerDayString) => {
  const tokensPerDay = parseEther(tokensPerDayString)
  const result = tokensPerDay.div(24).div(60).div(60)
  return result.toString()
}