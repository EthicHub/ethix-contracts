const fs = require('fs')
var path = require('path')
const root = path.resolve('./')
const DIR = path.join(root, '.deployment')
const beautify = require('json-beautify')

const needsInit = (x) => {
  return x === null || x === undefined || x === {}
}
const FIRST_HARDHAT_ADDRESS = '0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266'
const TESTNET_DEPLOY_ADDRESS = '0x0C9bA5FD1dFe97E8287020f892b360602aef362D'

module.exports = class DeployResultWriter {
  constructor(network) {
    this._initDir()
    this.network = network
    this.filePath = path.join(DIR, `${this.network}.json`)
    this._loadDeployment()
  }
  _initDir() {
    if (!fs.existsSync(DIR)) {
      fs.mkdirSync(DIR)
    }
  }
  _getStableCoin() {
    switch (this.network) {
      case 'mainnet':
        return {
          address: '0x6b175474e89094c44da98b954eedeac495271d0f',
          isProxy: false,
        }
      case 'goerli':
        return {
          address: '0x2139cc303b72cf1cd4cdf92afa3afc11cd129493',
          isProxy: false,
        }
      case 'sepolia':
        return {
          address: '0x3e622317f8C93f7328350cF0B56d9eD4C620C5d6',
          isProxy: false,
        }
      case 'celo':
        return {
          address: '0x765DE816845861e75A25fCA122bb6898B8B1282a',
          isProxy: false,
        }
      case 'alfajores':
        return {
          address: '0x874069Fa1Eb16D44d622F2e0Ca25eeA172369bC1',
          isProxy: false,
        }
      case 'hardhat':
        const stableCoin = this.currentDeployment.StableCoin
        return stableCoin ? stableCoin : {}
      default:
        throw new Error('Unknown or unsupported network: ' + this.network)
    }
  }
  _getEthixToken() {
    switch (this.network) {
      case 'mainnet':
        return {
          address: '0xFd09911130e6930Bf87F2B0554c44F400bD80D3e',
          isProxy: true,
        }
      case 'goerli':
        return {
          address: '0x253c9D1eE871fA3592d1bF6CeaB58695f98F7Fa7',
          isProxy: true,
        }
      case 'sepolia':
        return {
          address: '0xf63eeab5ce6c49654708442ea1afb7b56693b734',
          isProxy: true,
        }
      case 'xdai':
        return {
          address: '0xec3f3e6d7907acda3a7431abd230196cda3fbb19',
          isProxy: true,
        }
      case 'celo':
        return {
          address: '0x9995cc8F20Db5896943Afc8eE0ba463259c931ed',
          isProxy: true,
        }
      case 'alfajores':
        return {
          address: '0x4620D7a5F58f77eeE69A38AfdAa8f2FfB10b42b6',
          isProxy: true,
        }
      case 'chiado':
        return {
          address: '0xA4760b85Feed521ff7a087F716B0C5d8c13c3E34',
          isProxy: false,
        }
      case 'hardhat':
        const EthixToken = this.currentDeployment.EthixToken
        return EthixToken ? EthixToken : {}
      default:
        throw new Error('Unknown or unsupported network: ' + this.network)
    }
  }
  _getSmartPoolFactory() {
    switch (this.network) {
      case 'mainnet':
      case 'goerli':
      case 'sepolia':
      case 'hardhat':
      case 'local':
        return {}
      default:
        throw new Error('Unknown or unsupported network: ' + this.network)
    }
  }
  _getCorePoolFactory() {
    switch (this.network) {
      case 'mainnet':
      case 'goerli':
      case 'sepolia':
      case 'hardhat':
      case 'local':
        return {}
      default:
        throw new Error('Unknown or unsupported network: ' + this.network)
    }
  }
  _getVestingReserve() {
    switch (this.network) {
      case 'mainnet':
      case 'goerli':
      case 'sepolia':
      case 'hardhat':
      case 'local':
        return {}
      default:
        throw new Error('Unknown or unsupported network: ' + this.network)
    }
  }
  _getNativeFeesTreasury() {
    switch (this.network) {
      case 'xdai':
        return {
          address: null,
          isProxy: false,
          minPeriodDistribution: 604800, // 1 week
          recipient: '0x71c4c995671f73e522282998d5915c89aa9e7d57',
        }
      case 'sokol':
        return {
          address: null,
          isProxy: false,
          minPeriodDistribution: 1,
          recipient: '0x4e2dc3e9a5ffdcf00623cd306a52a7339a19e1db',
        }
      default:
        return {
          address: null,
          isProxy: false,
          minPeriodDistribution: 1,
          recipient: FIRST_HARDHAT_ADDRESS,
        }
    }
  }
  _getStkEthix() {
    switch (this.network) {
      case 'celo':
        return {
          address: null,
          isProxy: true,
          ethixGovernanceAddress: this._getGovernanceAddress(),
          coolDownSeconds: 864000, // 10 days
          unstakeWindow: 172800, // 2 days
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: 315360000, // 10 year un sec
          emissionsPerSecond: 1736111111111111, // 150 tokens each day
          totalStaked: 0,
        }
      case 'mainnet':
      case 'chiado':
      case 'sepolia':
      case 'goerli':
      case 'hardhat':
      case 'local':
        return {
          address: null,
          isProxy: true,
          ethixGovernanceAddress: this._getGovernanceAddress(),
          coolDownSeconds: 180,
          unstakeWindow: 180,
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: 1314000000, // 41.7 year un sec
          emissionsPerSecond: 11574074074074074, // 1000 tokens each day
          totalStaked: 0,
        }
      default:
        throw new Error('_getStkBEthixDai or unsupported network: ' + this.network)
    }
  }
  _getStkBEthixDai() {
    switch (this.network) {
      case 'mainnet':
        return {
          address: null,
          isProxy: true,
          ethixGovernanceAddress: this._getGovernanceAddress(),
          coolDownSeconds: 864000, // 10 days
          unstakeWindow: 172800, // 2 days
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: 1314000000,
          emissionsPerSecond: 11574074074074074, // 1000 tokens each day
        }
      case 'sepolia':
      case 'goerli':
      case 'hardhat':
      case 'local':
        return {
          address: null,
          isProxy: true,
          ethixGovernanceAddress: this._getGovernanceAddress(),
          coolDownSeconds: 180,
          unstakeWindow: 180,
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: 1314000000,
          emissionsPerSecond: 11574074074074074 // 1000 tokens each day
        }
      default:
        throw new Error('Unknown or unsupported network: ' + network)
    }
  }
  _getStkUEthixEth() {
    switch (this.network) {
      case 'mainnet':
        return {
          address: null,
          isProxy: true,
          ethixGovernanceAddress: this._getGovernanceAddress(),
          coolDownSeconds: 864000, // 10 days
          unstakeWindow: 172800, // 2 days
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: 1314000000,
          emissionsPerSecond: 23148148148148148, // 2000 tokens each day
        }
      case 'goerli':
      case 'sepolia':
      case 'hardhat':
      case 'local':
        return {
          address: null,
          isProxy: true,
          ethixGovernanceAddress: this._getGovernanceAddress(),
          coolDownSeconds: 180,
          unstakeWindow: 180,
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: 1314000000,
          emissionsPerSecond: 23148148148148148, // 2000 tokens each day
        }
      default:
        throw new Error('Unknown or unsupported network: ' + network)
    }
  }
  _getOriginatorStkEthix() {
    switch (this.network) {
      case 'mainnet':
        return {
          address: null,
          isProxy: true,
          governanceAddress: '0xA8f0A7dC8695c2233D847c7E34719830cdFaa4E1',
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: 31536000, //1 year un sec
          emissionsPerDay: '60', // 60 tokens each day
          auditorAddress: '0xe7424B957E972dC4C74805347e6EdA1b9C2Fa72f',
          originatorAddress: '0x91e5a544a12aA32eC41cAab09AbD83e0F1674387',
          auditorPercentage: '2000',
          originatorPercentage: '2000',
          stakingGoal: '37500000000000000000000', // 37.5k ethix
          defaultDelay: '7776000', // 3 months in sec
        }
      case 'goerli':
      case 'sepolia':
      case 'alfajores':
      case 'hardhat':
      case 'local':
        return {
          address: null,
          isProxy: true,
          governanceAddress: this._getGovernanceAddress(),
          emissionManagerAddress: this._getEmissionManagerAddress(),
          distributionDuration: '86400', // 1 day in seconds
          emissionsPerDay: '250', // 250 tokens each day
          auditorAddress: '0x2f80d1b0C9fb63216eB0b4207cE498DFaDd05424',
          originatorAddress: '0x63dCeF5Dc11a5fb6cF63983a1f422E232C6ed66e',
          auditorPercentage: '2000',
          originatorPercentage: '2000',
          stakingGoal: '100000000000000000000', // 100 ETHIX
          defaultDelay: '86400', // 1 day in seconds
        }
      default:
        throw new Error('Unknown or unsupported network: ' + network)
    }
  }
  _getGovernanceAddress() {
    switch (this.network) {
      case 'mainnet':
        return '0x9378A4932aB933fb893192dc1B163A37e807B185'
      case 'celo':
        return '0xB721E887FA79Ce52f9F86958C128fa9eD3f1c5D3'
      case 'goerli':
      case 'sepolia':
      case 'chiado':
        return '0x88A013548Dc83996FE7C75562d08ac4e518c1897'
      case 'local':
      case 'hardhat':
        return FIRST_HARDHAT_ADDRESS
    }
  }

  _getEmissionManagerAddress() {
    switch (this.network) {
      case 'mainnet':
        return this._getGovernanceAddress()
      case 'celo':
        return '0x9378A4932aB933fb893192dc1B163A37e807B185'
      case 'sepolia':
      case 'goerli':
      case 'chiado':
        return TESTNET_DEPLOY_ADDRESS
      case 'local':
      case 'hardhat':
        return FIRST_HARDHAT_ADDRESS
    }
  }

  _getCompensationSystemManager() {
    switch (this.network) {
      case 'mainnet':
        return {
          address: null,
          isProxy: true,
          maxCompensation: '5000000000000000000000',
        }
      case 'goerli':
      case 'sepolia':
      case 'hardhat':
      case 'local':
        return {
          address: null,
          isProxy: true,
          maxCompensation: '3000000000000000000000',
        }
    }
  }

  _initLayer2() {
    needsInit(this.currentDeployment.NativeFeesTreasury)
      ? (this.currentDeployment.NativeFeesTreasury = this._getNativeFeesTreasury())
      : null
  }

  _initLayer1() {
    needsInit(this.currentDeployment.StableCoin) ? (this.currentDeployment.StableCoin = this._getStableCoin()) : null
    needsInit(this.currentDeployment.balancer)
      ? (this.currentDeployment.balancer = {
        corePoolFactory: this._getCorePoolFactory(),
        smartPoolFactory: this._getSmartPoolFactory(),
      })
      : null
    needsInit(this.currentDeployment.CompensationSystemManager)
      ? (this.currentDeployment.CompensationSystemManager = this._getCompensationSystemManager())
      : null
    needsInit(this.currentDeployment.VestingReserve)
      ? (this.currentDeployment.VestingReserve = this._getVestingReserve())
      : null
    needsInit(this.currentDeployment.StakedETHIX)
      ? (this.currentDeployment.StakedETHIX = this._getStkEthix())
      : null
    needsInit(this.currentDeployment.StakedBETHIXDAI)
      ? (this.currentDeployment.StakedBETHIXDAI = this._getStkBEthixDai())
      : null
    needsInit(this.currentDeployment.StakedUETHIXETH)
      ? (this.currentDeployment.StakedUETHIXETH = this._getStkUEthixEth())
      : null
    needsInit(this.currentDeployment.OriginatorStaking)
      ? (this.currentDeployment.OriginatorStaking = this._getOriginatorStkEthix())
      : null

  }
  _loadDeployment() {
    if (fs.existsSync(this.filePath, fs.R_OK)) {
      this.currentDeployment = JSON.parse(fs.readFileSync(this.filePath))
    } else {
      this.currentDeployment = {}
    }
    if (this.network === 'local') {
      this._initLayer1()
      this._initLayer2()
    } else if (this.network !== 'chiado' && this.network !== 'xdai') {
      this._initLayer1()
    } else {
      this._initLayer2()
    }
    this._saveDeploymentToFile()
  }
  _saveDeploymentToFile() {
    fs.writeFileSync(this.filePath, beautify(this.currentDeployment, null, 2, 80))
  }
  addContract(name, address, isProxy) {
    if (!this.currentDeployment[name]) {
      this.currentDeployment[name] = {}
    }
    this.currentDeployment[name].address = address
    this.currentDeployment[name].isProxy = isProxy
    this._saveDeploymentToFile()
  }
  updateContract(name, key, value) {
    if (!this.currentDeployment[name]) {
      throw new Error('contract not deployed: ' + name)
    }
    this.currentDeployment[name][key] = value
    this._saveDeploymentToFile()
  }
}
