const chalk = require('chalk')
const prompts = require('prompts');
const ethers = require('ethers')

const promptForApproval = async (message) => {
  console.log('⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️\n')
  console.log(chalk.yellow(message));
  console.log('⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️  ⚠️\n')

  const response = await prompts({
    type: 'confirm',
    name: 'value',
    message: 'Are you sure?',
  })
  if (!response.value) {
    console.log("I'll take that as a NO")
    process.exit(0)
  }

}

const promptContractSelection = async (writer) => {

  const response = await prompts({
    type: 'autocomplete',
    name: 'value',
    message: 'Enter contract name (as saved in .deployment)',
    choices: Object.keys(writer.currentDeployment).map(x => {
      return { title: x }
    }),
    initial: true
  })

  return response.value
}

const promptForDate = async (message) => {
  const response = await prompts({
    type: 'date',
    name: 'value',
    message: message,
    validate: date => date <= Date.now() ? 'No time travel allowed' : true
  })

  return response.value
}

const formatAddress = (address) => ethers.utils.getAddress(address)
const validateAddress = (address) => {
  try {
    formatAddress(address)
  } catch(error) {
    console.log(error.reason)
    return false
  }
  return true
}
const formatPercentage = (percentage) => `${(Number(percentage)*100).toFixed(0)}`
const validNumberNonZero = (stringNumber) => !Number.isNaN(Number(stringNumber))&& Number(stringNumber) !== 0
const validatePercentage = (percentage) => validNumberNonZero(percentage) && Number(formatPercentage(percentage)) < 10000
const formatEther2Wei = (value) => {
  try {
    return ethers.utils.parseEther(`${value}`).toString()
  } catch(err) {
    console.log(err)
    return ''
  }
}

const promptOriginatorData = () => {
  const questions = [
    {
      type: 'text',
      name: 'auditorPercentage',
      message: 'Enter AUDITOR percentage (as in 40, 20.5)',
      format: formatPercentage,
      validate: validatePercentage
    },
    {
      type: 'text',
      name: 'originatorPercentage',
      message: 'Enter ORIGINATOR percentage (as in 40, 20.5)',
      format: formatPercentage,
      validate: validatePercentage
    },
    {
      type: 'number',
      name: 'stakingGoal',
      message: 'Enter staking goal (in ETHIX, ETH units)',
      format: formatEther2Wei,
      validate: validNumberNonZero
    },
    {
      type: 'number',
      name: 'defaultDelay',
      message: 'Enter default delay in days (how long since end of distribution to trigger token sales)',
      format: days => (days * 24 * 60 * 60).toFixed(0),
      validate:  validNumberNonZero,
    },
    {
      type: 'text',
      name: 'auditorAddress',
      message: 'Enter AUDITOR public address (0x123....)',
      format: formatAddress,
      validate: validateAddress,
    },
    {
      type: 'text',
      name: 'originatorAddress',
      message: 'Enter ORIGINATOR public address (0x123....)',
      format: formatAddress,
      validate: validateAddress,
    },
    {
      type: 'text',
      name: 'governanceAddress',
      message: 'Enter GOVERNANCE public address (0x123....)',
      format: formatAddress,
      validate: validateAddress,
    }
  ]

  return prompts(questions)
}

const promptOriginatorWithLPData = () => {
  const questions = [
    {
      type: 'text',
      name: 'auditorPercentage',
      message: 'Enter AUDITOR percentage (as in 40, 20.5)',
      format: formatPercentage,
      validate: validatePercentage
    },
    {
      type: 'text',
      name: 'originatorPercentage',
      message: 'Enter ORIGINATOR percentage (as in 40, 20.5)',
      format: formatPercentage,
      validate: validatePercentage
    },
    {
      type: 'number',
      name: 'stakingGoal',
      message: 'Enter staking goal (in ETHIX, ETH units)',
      format: formatEther2Wei,
      validate: validNumberNonZero
    },
    {
      type: 'number',
      name: 'defaultDelay',
      message: 'Enter default delay in days (how long since end of distribution to trigger token sales)',
      format: days => (days * 24 * 60 * 60).toFixed(0),
      validate:  validNumberNonZero,
    },
    {
      type: 'text',
      name: 'auditorAddress',
      message: 'Enter AUDITOR public address (0x123....)',
      format: formatAddress,
      validate: validateAddress,
    },
    {
      type: 'text',
      name: 'originatorAddress',
      message: 'Enter ORIGINATOR public address (0x123....)',
      format: formatAddress,
      validate: validateAddress,
    },
    {
      type: 'text',
      name: 'governanceAddress',
      message: 'Enter GOVERNANCE public address (0x123....)',
      format: formatAddress,
      validate: validateAddress,
    },
    {
      type: 'number',
      name: 'nftPositionId',
      message: 'Enter NFT Uniswap position ID',
      format: number => Number(number),
      validate:  validNumberNonZero,
    }
  ]

  return prompts(questions)
}

module.exports = {
  promptContractSelection,
  promptForApproval,
  promptForDate,
  promptOriginatorData,
  promptOriginatorWithLPData
}