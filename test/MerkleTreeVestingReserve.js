const { expect } = require('chai')
const { ethers } = require('hardhat')
const { timeLatest, evmRevert, evmSnapshot } = require('./helpers/utils')
const { setSnapshotId, snapshotId } = require('./helpers/make-suite')
const parseEther = ethers.utils.parseEther
const BalanceTree = require('../utils/merkle/balance-tree')
const parseBalanceMap = require('../utils/merkle/parse-balance-map')

const BigNumber = ethers.BigNumber

function generateLeafs(n, tokenAmount) {
  let leafs = []
  for (let i = 0; i < n; i++) {
    let acc = ethers.Wallet.createRandom()
    // lowercase is important!
    leafs.push({ account: acc.address.toLowerCase(), amount: tokenAmount })
  }
  return leafs
}

describe('MerkleTreeVestingReserve', function () {
  let Reserve
  let Token

  let owner
  let addr1
  let holder1
  let token
  let tree
  let totalVested
  let leafs

  before(async () => {
    setSnapshotId(await evmSnapshot())
  })

  beforeEach(async () => {
    Reserve = await ethers.getContractFactory('MerkleTreeVestingReserve')
    Token = await ethers.getContractFactory('TestToken')
    ;[owner, addr1, holder1, holder2] = await ethers.getSigners()

    token = await Token.deploy()
    let amountPerWallet = parseEther('10')
    leafs = generateLeafs(8, amountPerWallet)
    leafs.push({ account: addr1.address, amount: BigNumber.from(100) })
    leafs.push({ account: holder1.address, amount: BigNumber.from(100) })
    tree = new BalanceTree(leafs)
    totalVested = amountPerWallet.mul('10')
  })

  it('Deploys', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), 0, tree.getHexRoot(), totalVested)
    expect(await reserve.merkleRoot()).to.eq(tree.getHexRoot())
  })

  it('First claims half', async () => {
    const now = await timeLatest()
    const end = now.add(10000)
    const reserve = await deployReserve(now, end, 0, tree.getHexRoot(), totalVested)
    ethers.provider.send('evm_increaseTime', [5000])
    ethers.provider.send('evm_mine')
    for (let i = 0; i < leafs.length; i++) {
      // string like "0x821aea9a577a9b44299b9c15c88cf3087f3b5544 99000000"
      let leaf = leafs[i]
      let proof = tree.getProof(i, leaf.account, leaf.amount)

      let halfTokens = BigNumber.from(leaf.amount).div(2)
      let airdropContractBalance = await token.balanceOf(reserve.address)

      let userTokenBalance = await token.balanceOf(leaf.account)

      const tx = await reserve.firstClaim(i, leaf.account, leaf.amount, proof)

      expect(await token.balanceOf(reserve.address)).to.lte(airdropContractBalance.sub(halfTokens))
      expect(await token.balanceOf(leaf.account)).to.gte(userTokenBalance.add(halfTokens))
    }
  })

  it('claim adding', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), 0, tree.getHexRoot(), totalVested)
    let leaf = leafs[8]
    let proof = tree.getProof(8, leaf.account, leaf.amount)
    await reserve.verifyAndAddEntry(8, leaf.account, leaf.amount, proof)
    ethers.provider.send('evm_increaseTime', [100000])
    ethers.provider.send('evm_mine')
    const vested = await reserve.vestedOf(addr1.address)
    await reserve.connect(addr1).claim(vested)

    const balance = await token.balanceOf(addr1.address)
    const difference = vested - balance

    expect(difference < 1000000)
  })

  it('fails verifying twice', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), 0, tree.getHexRoot(), totalVested)
    let leaf = leafs[8]
    let proof = tree.getProof(8, leaf.account, leaf.amount)
    await reserve.verifyAndAddEntry(8, leaf.account, leaf.amount, proof)
    await expect(reserve.verifyAndAddEntry(8, leaf.account, leaf.amount, proof)).to.be.revertedWith(
      'MerkeTreeeVestingReserve: account already claimed before, use claim methods'
    )

  })

  it('fails firstClaiming twice adding', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), 0, tree.getHexRoot(), totalVested)
    let leaf = leafs[8]
    let proof = tree.getProof(8, leaf.account, leaf.amount)
    ethers.provider.send('evm_increaseTime', [100000])
    ethers.provider.send('evm_mine')
    await reserve.firstClaim(8, leaf.account, leaf.amount, proof)
    expect(await reserve.isVerified(8)).to.eq(true)
    await expect(reserve.firstClaim(8, leaf.account, leaf.amount, proof)).to.be.revertedWith(
      'MerkeTreeeVestingReserve: account already claimed before, use claim methods'
    )

  })

  it('changeTokenOwnership', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(1000000), tree.getHexRoot(), totalVested)

    let leaf = leafs[8]
    let proof = tree.getProof(8, leaf.account, leaf.amount)

    await reserve.verifyAndAddEntry(8, leaf.account, leaf.amount, proof)
    const locked1 = await reserve.lockedOf(leaf.account)
    expect(locked1).to.equal('100')
    const locked2 = await reserve.lockedOf(holder1.address)
    expect(locked2).to.equal('0')

    await reserve.changeTokenOwnership(leaf.account, holder1.address)
    const locked1After = await reserve.lockedOf(leaf.account)
    expect(locked1After).to.equal('0')
    const locked2After = await reserve.lockedOf(holder1.address)
    expect(locked2After).to.equal('100')
  })


  async function deployReserve(startTime, endTime, editAddressUntil, merkleRoot, tokens) {
    const reserve = await Reserve.deploy(token.address, startTime, endTime, editAddressUntil, merkleRoot)

    await token.transfer(reserve.address, tokens)

    return reserve
  }

  after(async () => {
    await evmRevert(snapshotId)
  })
})
