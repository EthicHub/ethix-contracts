const { expect } = require('chai')
const { ethers } = require('hardhat')

const provider = waffle.provider

describe('NativeFeesTreasury', function () {
  let Treasury

  let owner
  let addr1
  let treasury

  beforeEach(async () => {
    Treasury = await ethers.getContractFactory('NativeFeesTreasury')
    ;[owner, addr1, holder1] = await ethers.getSigners()

    treasury = await Treasury.deploy()
    await treasury.initialize(0, addr1.address)

    let signer = await provider.getSigner(owner.address)
    await signer.sendTransaction({
      to: await treasury.reserve(),
      value: 100,
    })
  })

  it('distribute fails if not owner', async () => {
    expect(treasury.connect(addr1).distribute()).to.be.revertedWith(
      'Ownable: caller is not the owner'
    )
  })

  it('distribute', async () => {
    const balanceBefore = await provider.getBalance(addr1.address)
    await treasury.distribute()
    const balanceAfter = await provider.getBalance(addr1.address)
    expect(balanceAfter.sub(balanceBefore)).to.equal(100)
  })

  it('distribute fails if minPeriodDistribution has not passed since last distribution', async () => {
    await treasury.setMinPeriodDistribution(100)
    expect(treasury.distribute()).to.be.revertedWith(
      'FeesTreasury: Not enough time passed since last distribution'
    )
  })

  it('setMinPeriodDistribution fails if not owner', async () => {
    expect(treasury.connect(addr1).setMinPeriodDistribution(1)).to.be.revertedWith(
      'Ownable: caller is not the owner'
    )
  })

  it('setMinPeriodDistribution', async () => {
    await treasury.setMinPeriodDistribution(1)
    expect(await treasury.minPeriodDistribution()).to.equal(1)
  })

  it('setRecipient fails if not owner', async () => {
    expect(treasury.connect(addr1).setRecipient(addr1.address)).to.be.revertedWith(
      'Ownable: caller is not the owner'
    )
  })

  it('setMinPeriodDistribution', async () => {
    await treasury.setRecipient(addr1.address)
  })
})
