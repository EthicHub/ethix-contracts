const { valueToZDBigNumber } = require('../helpers/bignumber')
const hre = require('hardhat')
const { expect } = require('chai')

module.exports = {
  waitForTx: async (tx) => {
    return await tx.wait()
  },
  evmSnapshot: async () => await hre.network.provider.send('evm_snapshot', []),
  evmRevert: async (id) => await hre.network.provider.send('evm_revert', [id]),
  getUserIndex: async (distributionManager, user, asset) => {
    return await distributionManager.getUserAssetData(user, asset)
  },
  getRewards: (balance, assetIndex, userIndex, precision = 18) => {
    let returnBigNumber = valueToZDBigNumber(balance)
      .multipliedBy(valueToZDBigNumber(assetIndex).minus(userIndex.toString()))
      .dividedBy(valueToZDBigNumber(10).exponentiatedBy(precision))
    return ethers.BigNumber.from(returnBigNumber.toString())
  },
  timeLatest: async () => {
    const block = await ethers.provider.getBlock('latest')
    return ethers.BigNumber.from(block.timestamp)
  },
  /**
   *
   * @dev fills signers with token. MUST be called in every staking test
   * @param token {ERC20} token connected to a signer (signer needs to have enough balance.)
   * @param signers {[Signer]} array of signers
   */
  fillWalletsWithToken: async (token, signers) => {
    const receipts = await Promise.all(
      await signers.map(
        async (signer) => await token.transfer(signer.address, ethers.utils.parseEther('100'))
      )
    )
    // console.log(receipts)
    // console.log((await token.balanceOf(signers[4].address)).toString())
  },
  eventChecker: (event, name, args) => {
    expect(event.event).to.be.equal(name, `Incorrect event emitted`)
    if (event.args) {
      expect(event.args.length || 0 / 2).to.be.equal(args.length, `${name} signature are wrong`)
      args.forEach((arg, index) => {
        expect(event.args && event.args[index].toString()).to.be.equal(
          arg.toString(),
          `${name} has incorrect value on position ${index}`
        )
      })
    }
  },
  linkLibraries: ({ bytecode, linkReferences }, libraries) => {
    Object.keys(linkReferences).forEach((fileName) => {
      Object.keys(linkReferences[fileName]).forEach((contractName) => {
        if (!libraries.hasOwnProperty(contractName)) {
          throw new Error(`Missing link library name ${contractName}`)
        }
        const address = ethers.utils
          .getAddress(libraries[contractName])
          .toLowerCase()
          .slice(2)
        linkReferences[fileName][contractName].forEach(
          ({ start, length }) => {
            const start2 = 2 + start * 2
            const length2 = length * 2
            bytecode = bytecode
              .slice(0, start2)
              .concat(address)
              .concat(bytecode.slice(start2 + length2, bytecode.length))
          }
        )
      })
    })
    return bytecode
  }
}
