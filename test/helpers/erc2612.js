const { ethers } = require('ethers')

// Thanks Hermez network and Edu Antuña for the help (NOTE: they use Uniswap style permit, we use DAI)
// https://github.com/hermeznetwork/contracts/blob/60ed57a2c9feaff13374224cf518aa066773ccd9/test/hermez/helpers/erc2612.js

const PERMIT_TYPEHASH = ethers.utils.keccak256(
  ethers.utils.toUtf8Bytes(
    'Permit(address holder,address spender,uint256 nonce,uint256 expiry,bool allowed)'
  )
)
const VERBOSE = false
async function createPermitDigest(token, holder, spender, nonce, expiry, allowed) {
  const chainId = (await token.getChainId()).toNumber()
  const name = await token.name()

  let _domainSeparator = ethers.utils.keccak256(
    ethers.utils.defaultAbiCoder.encode(
      ['bytes32', 'bytes32', 'bytes32', 'uint256', 'address'],
      [
        ethers.utils.keccak256(
          ethers.utils.toUtf8Bytes(
            'EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)'
          )
        ),
        ethers.utils.keccak256(ethers.utils.toUtf8Bytes(name)),
        ethers.utils.keccak256(ethers.utils.toUtf8Bytes('1')),
        chainId,
        token.address,
      ]
    )
  )
  if (VERBOSE) {
    console.log('generator-----')
    console.log('PERMIT_TYPEHASH')
    console.log(PERMIT_TYPEHASH)
    console.log('DOMAIN_SEPARATOR')
    console.log(_domainSeparator)
    console.log('--------------------')
  }

  return ethers.utils.solidityKeccak256(
    ['bytes1', 'bytes1', 'bytes32', 'bytes32'],
    [
      '0x19',
      '0x01',
      _domainSeparator,
      ethers.utils.keccak256(
        ethers.utils.defaultAbiCoder.encode(
          ['bytes32', 'address', 'address', 'uint256', 'uint256', 'bool'],
          [PERMIT_TYPEHASH, holder, spender, nonce, expiry, allowed]
        )
      ),
    ]
  )
}

module.exports = {
  createPermitDigest,
  PERMIT_TYPEHASH,
}
