const hre = require('hardhat')

let snapshotId = '0x1'

const setSnapshotId = (id) => {
  if (hre.network.name === 'hardhatevm') {
    snapshotId = id
    console.log('snapshotId', snapshotId)
  }
}

module.exports = {
  snapshotId: snapshotId,
  setSnapshotId: setSnapshotId,
}
