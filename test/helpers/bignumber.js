const { BigNumber } = require('bignumber.js')

const BigNumberZD = BigNumber.clone({
  DECIMAL_PLACES: 0,
  ROUNDING_MODE: BigNumber.ROUND_DOWN,
})

module.exports = {
  valueToBigNumber: function (amount) {
    return new BigNumber(amount.toString())
  },
  valueToZDBigNumber: function (amount) {
    return new BigNumberZD(amount.toString())
  },
}
