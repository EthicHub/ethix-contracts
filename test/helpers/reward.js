const { waitForTx, getUserIndex, getRewards, eventChecker } = require('../helpers/utils')
const { expect } = require('chai')
const compareRewardsAtAction = async (
  stakeToken,
  userAddress,
  actions,
  shouldReward,
  assetConfig
) => {
  const underlyingAsset = stakeToken.address
  // To prevent coverage to fail, add 5 seconds per comparisson.
  await ethers.provider.send('evm_increaseTime', [5])
  const rewardsBalanceBefore = await stakeToken.getTotalRewardsBalance(userAddress)
  // Configure assets of stake token
  const assetConfiguration = assetConfig
    ? {
        ...assetConfig,
        underlyingAsset,
      }
    : {
        emissionPerSecond: '100',
        totalStaked: await stakeToken.totalSupply(),
        underlyingAsset,
      }
  await stakeToken.configureAssets([assetConfiguration])
  const userBalance = await stakeToken.balanceOf(userAddress)
  // Get index before actions
  const userIndexBefore = await getUserIndex(stakeToken, userAddress, underlyingAsset)
  // Dispatch actions that can or not update the user index
  const receipts = await Promise.all(await actions().map(async (action) => waitForTx(await action)))
  // Get index after actions
  const userIndexAfter = await getUserIndex(stakeToken, userAddress, underlyingAsset)
  // Compare calculated JS rewards versus Solidity user rewards
  const rewardsBalanceAfter = await stakeToken.getTotalRewardsBalance(userAddress)

  const expectedAccruedRewards = await getRewards(userBalance, userIndexAfter, userIndexBefore)

  expect(rewardsBalanceAfter).to.equal(rewardsBalanceBefore.add(expectedAccruedRewards))

  // Explicit check rewards when the test case expects rewards to the user
  if (shouldReward) {
    expect(expectedAccruedRewards).to.be.gt(0)
  } else {
    expect(expectedAccruedRewards).to.be.equal(0)
    expect(rewardsBalanceAfter).to.be.equal(rewardsBalanceBefore)
  }

  // Check the reward event values if any in the latest tx receipt
  if (expectedAccruedRewards.gt('0')) {
    const latestReceipt = receipts[receipts.length - 1]
    const eventAccrued = latestReceipt.events
      ? latestReceipt.events.find(({ event }) => event === 'RewardsAccrued')
      : null
    if (eventAccrued) {
      eventChecker(eventAccrued, 'RewardsAccrued', [userAddress, expectedAccruedRewards.toString()])
    } else {
      assert.fail('RewardsAccrued event must be emitted')
    }
  }
}

const compareRewardsAtTransfer = async (
  stakeToken,
  from,
  to,
  amount,
  fromShouldReward,
  toShouldReward,
  assetConfig
) => {
  // Increase time to prevent coverage to fail
  await ethers.provider.send('evm_increaseTime', [5])

  const fromAddress = from.address
  const toAddress = to.address
  const underlyingAsset = stakeToken.address
  const fromSavedBalance = await stakeToken.balanceOf(fromAddress)
  const toSavedBalance = await stakeToken.balanceOf(toAddress)
  const fromSavedRewards = await stakeToken.getTotalRewardsBalance(fromAddress)

  const toSavedRewards = await stakeToken.getTotalRewardsBalance(toAddress)

  // Get index before actions
  const fromIndexBefore = await getUserIndex(stakeToken, fromAddress, underlyingAsset)
  const toIndexBefore = await getUserIndex(stakeToken, toAddress, underlyingAsset)

  // Load actions that can or not update the user index
  const actions = () => [stakeToken.connect(from).transfer(toAddress, amount)]

  // Fire reward comparator
  await compareRewardsAtAction(stakeToken, fromAddress, actions, fromShouldReward, assetConfig)

  // Check rewards after transfer

  // Get index after actions
  const fromIndexAfter = await getUserIndex(stakeToken, fromAddress, underlyingAsset)
  const toIndexAfter = await getUserIndex(stakeToken, toAddress, underlyingAsset)

  // FROM: Compare calculated JS rewards versus Solidity user rewards
  const fromRewardsBalanceAfter = await stakeToken.getTotalRewardsBalance(fromAddress)

  const fromExpectedAccruedRewards = getRewards(fromSavedBalance, fromIndexAfter, fromIndexBefore)
  expect(fromRewardsBalanceAfter).to.equal(fromSavedRewards.add(fromExpectedAccruedRewards))

  // TO: Compare calculated JS rewards versus Solidity user rewards
  const toRewardsBalanceAfter = await stakeToken.getTotalRewardsBalance(toAddress)

  const toExpectedAccruedRewards = getRewards(toSavedBalance, toIndexAfter, toIndexBefore)
  expect(toRewardsBalanceAfter).to.equal(toSavedRewards.add(toExpectedAccruedRewards))

  // Explicit check rewards when the test case expects rewards to the user
  if (fromShouldReward) {
    expect(fromExpectedAccruedRewards).to.be.gt(0)
  } else {
    expect(fromExpectedAccruedRewards).to.be.equal(0)
  }

  // Explicit check rewards when the test case expects rewards to the user
  if (toShouldReward) {
    expect(toExpectedAccruedRewards).to.be.gt(0)
  } else {
    expect(toExpectedAccruedRewards).to.be.equal(0)
  }

  // Expect new balances
  if (fromAddress === toAddress) {
    expect(fromSavedBalance).to.be.equal(toSavedBalance)
  } else {
    const fromNewBalance = await stakeToken.balanceOf(fromAddress)
    const toNewBalance = await stakeToken.balanceOf(toAddress)
    expect(fromNewBalance).to.be.equal(fromSavedBalance.sub(amount).toString())
    expect(toNewBalance).to.be.equal(toSavedBalance.add(amount).toString())
  }
}

module.exports = {
  compareRewardsAtAction: compareRewardsAtAction,
  compareRewardsAtTransfer: compareRewardsAtTransfer,
}
