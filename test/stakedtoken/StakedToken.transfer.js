const { expect } = require('chai')
const { ethers } = require('hardhat')

const { COOLDOWN_SECONDS, UNSTAKE_WINDOW, DISTRIBUTION_DURATION } = require('../helpers/constants')

const { compareRewardsAtAction, compareRewardsAtTransfer } = require('../helpers/reward')
const { timeLatest, evmRevert, evmSnapshot } = require('../helpers/utils')
const { setSnapshotId, snapshotId } = require('../helpers/make-suite')

describe('StakedEthix. Transfers', () => {
  let staker1,
    staker2,
    staker3,
    staker4,
    staker5,
    ethix,
    stkEthix,
    addr1,
    addr2,
    rewardsVault,
    emissionManager

  before(async () => {
    setSnapshotId(await evmSnapshot())

    const StakedETHIX = await ethers.getContractFactory('StakedETHIX')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const TransferHookTest = await ethers.getContractFactory('TransferHookTest')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let network = await ethers.getDefaultProvider().getNetwork()
    let signers = await ethers.getSigners()
    ;[
      staker1,
      staker2,
      staker3,
      staker4,
      staker5,
      ethix,
      stkEthix,
      addr1,
      addr2,
      emissionManager,
    ] = signers

    ethix = await EthixToken.deploy()
    await ethix.initialize()

    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    const transferHook = await TransferHookTest.deploy()

    stkEthix = await StakedETHIX.deploy()
    await stkEthix.initialize(
      ethix.address,
      transferHook.address,
      COOLDOWN_SECONDS,
      UNSTAKE_WINDOW,
      rewardsVault.address,
      staker1.address,
      DISTRIBUTION_DURATION
    )
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), stkEthix.address)
  })

  it('User 1 stakes 50 ETHIX', async () => {
    const amount = ethers.utils.parseEther('50')

    const actions = () => [
      ethix.connect(staker1).approve(stkEthix.address, amount),
      stkEthix.connect(staker1).stake(staker1.address, amount),
    ]
    await compareRewardsAtAction(stkEthix, staker1.address, actions)
  })

  it('User 1 transfers 50 stkETHIX to User 5', async () => {
    const amount = ethers.utils.parseEther('50')

    await compareRewardsAtTransfer(stkEthix, staker1, staker5, amount, true, false)
  })

  it('User 5 transfers 50 stkETHIX to himself', async () => {
    const amount = ethers.utils.parseEther('50')
    await compareRewardsAtTransfer(stkEthix, staker5, staker5, amount, true, true)
  })

  it('User 5 transfers 50 stkETHIX to user 2, with rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('50')

    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    await compareRewardsAtTransfer(stkEthix, staker5, staker2, amount, false, false, assetConfig)
  })

  it('User 3 stakes and transfers 50 stkETHIX to user 2, with rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('50')
    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    const actions = () => [
      ethix.connect(staker3).approve(stkEthix.address, amount),
      stkEthix.connect(staker3).stake(staker3.address, amount),
    ]

    await compareRewardsAtAction(stkEthix, staker3.address, actions, false, assetConfig)
    await compareRewardsAtTransfer(stkEthix, staker3, staker2, amount, false, false, assetConfig)
  })

  it('Activate cooldown of User 3, transfer entire amount from User 3 to User 3, cooldown of User 2 should be reset', async () => {
    const amount = await stkEthix.balanceOf(staker2.address)

    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    await stkEthix.connect(staker2).cooldown()
    const cooldownActivationTimestamp = await (await timeLatest()).toString()

    const cooldownTimestamp = await stkEthix.stakersCooldowns(staker2.address)
    expect(cooldownTimestamp.gt('0')).to.be.ok
    expect(cooldownTimestamp.toString()).to.equal(cooldownActivationTimestamp)

    await compareRewardsAtTransfer(stkEthix, staker2, staker3, amount, false, false, assetConfig)

    // Expect cooldown time to reset after sending the entire balance of sender
    const cooldownTimestampAfterTransfer = await (
      await stkEthix.stakersCooldowns(staker2.address)
    ).toString()
    expect(cooldownTimestampAfterTransfer).to.equal('0')
  })

  it('Transfer balance from User 3 to user 2 cooldown of User 2 should be reset if User3 cooldown expired', async () => {
    const amount = ethers.utils.parseEther('10')

    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // First enable cooldown for sender
    await stkEthix.connect(staker3).cooldown()

    // Then enable cooldown for receiver
    await ethix.connect(staker2).approve(stkEthix.address, amount)
    await stkEthix.connect(staker2).stake(staker2.address, amount)
    await stkEthix.connect(staker2).cooldown()
    const receiverCooldown = await stkEthix.stakersCooldowns(staker3.address)

    // Increase time to an invalid time for cooldown
    await ethers.provider.send('evm_increaseTime', [
      receiverCooldown.add(COOLDOWN_SECONDS).add(UNSTAKE_WINDOW).add(1).toNumber(),
    ])
    await ethers.provider.send('evm_mine', [])

    // Transfer staked  ethix from sender to receiver, it will also transfer the cooldown status from sender to the receiver
    await compareRewardsAtTransfer(stkEthix, staker3, staker2, amount, false, false, assetConfig)

    // Receiver cooldown should be set to zero
    const stakerCooldownTimestampBefore = await stkEthix.stakersCooldowns(staker2.address)
    expect(stakerCooldownTimestampBefore.eq(0)).to.be.ok
  })

  it('Transfer balance from User 3 to user 2, cooldown of User 2 should be the same if User3 cooldown is less than User2 cooldown', async () => {
    const amount = ethers.utils.parseEther('10')

    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // Enable cooldown for sender
    await stkEthix.connect(staker3).cooldown()
    await ethers.provider.send('evm_increaseTime', [5])

    // Enable enable cooldown for receiver
    await stkEthix.connect(staker2).cooldown()
    const receiverCooldown = await (await stkEthix.stakersCooldowns(staker2.address)).toString()

    // Transfer staked  ethix from sender to receiver, it will also transfer the cooldown status from sender to the receiver
    await compareRewardsAtTransfer(stkEthix, staker3, staker2, amount, false, false, assetConfig)

    // Receiver cooldown should be like before
    const receiverCooldownAfterTransfer = await (
      await stkEthix.stakersCooldowns(staker2.address)
    ).toString()
    expect(receiverCooldownAfterTransfer).to.be.equal(receiverCooldown)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
