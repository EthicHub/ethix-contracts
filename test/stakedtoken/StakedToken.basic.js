const { expect } = require('chai')
const { ethers } = require('hardhat')

const {
  COOLDOWN_SECONDS,
  UNSTAKE_WINDOW,
  MAX_UINT_AMOUNT,
  DISTRIBUTION_DURATION,
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { compareRewardsAtAction } = require('../helpers/reward')
const { getUserIndex, getRewards, timeLatest, evmRevert, evmSnapshot } = require('../helpers/utils')
const { BigNumber } = require('ethers')

describe('StakedEthix. Basics', () => {
  let staker, staker2, ethix, stkEthix, addr1, addr2, rewardsVault, emissionManager

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const StakedETHIX = await ethers.getContractFactory('StakedETHIX')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    const TransferHookTest = await ethers.getContractFactory('TransferHookTest')

    let network = await ethers.getDefaultProvider().getNetwork()
    let signers = await ethers.getSigners()

    ;[staker, staker2, staker3, ethix, stkEthix, addr1, addr2, addr3, emissionManager] = signers

    ethix = await EthixToken.deploy()
    await ethix.initialize()
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    const transferHook = await TransferHookTest.deploy()

    stkEthix = await StakedETHIX.deploy()
    await stkEthix.initialize(
      ethix.address,
      transferHook.address,
      COOLDOWN_SECONDS,
      UNSTAKE_WINDOW,
      rewardsVault.address,
      staker.address,
      DISTRIBUTION_DURATION
    )
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), stkEthix.address)
  })

  it('Initial configuration after initialize() is correct', async () => {
    expect(await stkEthix.name()).to.equal('Staked ETHIX')
    expect(await stkEthix.symbol()).to.equal('stkETHIX')
    expect(await stkEthix.decimals()).to.equal(18)
    expect(await stkEthix.STAKED_TOKEN()).to.equal(ethix.address)
    expect(await stkEthix.COOLDOWN_SECONDS()).to.equal(COOLDOWN_SECONDS)
    expect(await stkEthix.UNSTAKE_WINDOW()).to.equal(UNSTAKE_WINDOW)
    expect(await stkEthix.REWARDS_VAULT()).to.equal(rewardsVault.address)
  })

  it('Reverts trying to stake 0 amount', async () => {
    expect(stkEthix.connect(staker).stake(staker.address, 0)).to.be.revertedWith(
      'INVALID_ZERO_AMOUNT'
    )
  })

  it('Reverts trying to activate cooldown with 0 staked amount', async () => {
    expect(stkEthix.connect(staker).cooldown()).to.be.revertedWith('INVALID_BALANCE_ON_COOLDOWN')
  })

  it('User 1 stakes 50 ETHIX: receives 50 stkETHIX, StakedEthix balance of ETHIX is 50 and his rewards to claim are 0', async () => {
    const amount = ethers.utils.parseEther('50')

    const saveBalanceBefore = await stkEthix.balanceOf(staker.address)

    // Prepare actions for the test case
    const actions = () => [
      ethix.connect(staker).approve(stkEthix.address, amount),
      stkEthix.connect(staker).stake(staker.address, amount),
    ]

    // Check rewards
    await compareRewardsAtAction(stkEthix, staker.address, actions)

    // Stake token tests
    expect((await stkEthix.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount.toString()).toString()
    )
    expect((await ethix.balanceOf(stkEthix.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount.toString()).toString()
    )
    expect((await stkEthix.balanceOf(staker.address)).toString()).to.be.equal(amount)
    expect((await ethix.balanceOf(stkEthix.address)).toString()).to.be.equal(amount)
  })

  it('User 1 stakes 20 ETHIX more: his total stkETHIX balance increases, stkETHIX balance of Ethix increases and his reward until now get accumulated', async () => {
    const amount = ethers.utils.parseEther('20')

    console.log(amount.toString())
    const saveBalanceBefore = await stkEthix.balanceOf(staker.address)
    const actions = () => [
      ethix.connect(staker).approve(stkEthix.address, amount),
      stkEthix.connect(staker).stake(staker.address, amount),
    ]

    // Checks rewards
    await compareRewardsAtAction(stkEthix, staker.address, actions, true)

    // Extra test checks
    expect((await stkEthix.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount)
    )
    expect((await ethix.balanceOf(stkEthix.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount)
    )
  })

  it('User 1 claim half rewards ', async () => {
    // Increase time for bigger rewards
    ethers.provider.send('evm_increaseTime', [1000])
    ethers.provider.send('evm_mine')

    const halfRewards = (await stkEthix.stakerRewardsToClaim(staker.address)).div(2)
    console.log('-->halfRewards', halfRewards.toString())
    const saveUserBalance = await ethix.balanceOf(staker.address)

    await stkEthix.connect(staker).claimRewards(staker.address, halfRewards)

    const userBalanceAfterActions = await ethix.balanceOf(staker.address)
    expect(userBalanceAfterActions.eq(saveUserBalance.add(halfRewards))).to.be.ok
  })

  it('User 1 tries to claim higher reward than current rewards balance', async () => {
    const saveUserBalance = await ethix.balanceOf(staker.address)

    // Try to claim more amount than accumulated
    await expect(
      stkEthix.connect(staker).claimRewards(staker.address, ethers.utils.parseEther('10000'))
    ).to.be.revertedWith('INVALID_AMOUNT')

    const userBalanceAfterActions = await ethix.balanceOf(staker.address)
    expect(userBalanceAfterActions.eq(saveUserBalance)).to.be.ok
  })

  it('User 1 claim all rewards', async () => {
    const userAddress = staker.address
    const underlyingAsset = stkEthix.address

    const userBalance = await stkEthix.balanceOf(userAddress)
    const userEthixBalance = await ethix.balanceOf(userAddress)

    const userRewards = await stkEthix.stakerRewardsToClaim(userAddress)

    // Get index before actions
    const userIndexBefore = await getUserIndex(stkEthix, userAddress, underlyingAsset)

    // Claim rewards
    await stkEthix.connect(staker).claimRewards(staker.address, MAX_UINT_AMOUNT)

    // Get index after actions
    const userIndexAfter = await getUserIndex(stkEthix, userAddress, underlyingAsset)

    const expectedAccruedRewards = getRewards(userBalance, userIndexAfter, userIndexBefore)
    const userEthixBalanceAfterAction = (await ethix.balanceOf(userAddress)).toString()

    expect(userEthixBalanceAfterAction).to.be.equal(
      userEthixBalance.add(userRewards).add(expectedAccruedRewards).toString()
    )
  })

  it('User 5 stakes 50 ETHIX, with the rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('50')

    // Disable rewards via config
    const assetsConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // Checks rewards
    const actions = () => [
      ethix.connect(addr2).approve(stkEthix.address, amount),
      stkEthix.connect(addr2).stake(addr2.address, amount),
    ]

    await compareRewardsAtAction(stkEthix, addr2.address, actions, false, assetsConfig)

    // Check expected stake balance for second staker
    expect(await stkEthix.balanceOf(addr2.address)).to.equal(amount)

    // Expect rewards balance to still be zero
    const rewardsBalance = await stkEthix.getTotalRewardsBalance(addr2.address)
    expect(rewardsBalance).to.be.equal('0')
  })

  it('User 2 stakes 30 ETHIX more, with the rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('30')

    // Keep rewards disabled via config
    const assetsConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // Checks rewards
    const actions = () => [
      ethix.connect(addr2).approve(stkEthix.address, amount),
      stkEthix.connect(addr2).stake(addr2.address, amount),
    ]

    await compareRewardsAtAction(stkEthix, addr2.address, actions, false, assetsConfig)

    // Expect rewards balance to still be zero
    const rewardsBalance = await stkEthix.getTotalRewardsBalance(addr2.address)
    expect(rewardsBalance).to.be.equal('0')
  })

  it('Validates staker cooldown with stake() while being on valid unstake window', async () => {
    const amount1 = ethers.utils.parseEther('50')
    const amount2 = ethers.utils.parseEther('20')

    // Checks rewards
    const actions = () => [
      ethix.connect(staker3).approve(stkEthix.address, amount1.add(amount2)),
      stkEthix.connect(staker3).stake(staker3.address, amount1),
    ]

    await compareRewardsAtAction(stkEthix, staker3.address, actions, false)

    await stkEthix.connect(staker3).cooldown()

    const cooldownActivationTimestamp = await timeLatest()
    const cooldownPlus1000 = BigNumber.from(COOLDOWN_SECONDS).add(ethers.BigNumber.from(1000))
    const timeJump = cooldownActivationTimestamp.add(cooldownPlus1000)
    // We fast-forward time to just after the unstake window
    await ethers.provider.send('evm_mine', [timeJump.toNumber()])

    const stakerCooldownTimestampBefore = ethers.BigNumber.from(
      (await stkEthix.stakersCooldowns(staker3.address)).toString()
    )
    await stkEthix.connect(staker3).stake(staker3.address, amount2)
    const latestTimestamp = await timeLatest()
    const expectedCooldownTimestamp = amount2
      .mul(latestTimestamp.toString())
      .add(amount1.mul(stakerCooldownTimestampBefore.toString()))
      .div(amount2.add(amount1))
    expect(expectedCooldownTimestamp.toString()).to.be.equal(
      (await stkEthix.stakersCooldowns(staker3.address)).toString()
    )
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
