const { expect } = require('chai')
const { ethers } = require('hardhat')
const BigNumber = ethers.BigNumber
const { COOLDOWN_SECONDS, UNSTAKE_WINDOW, DISTRIBUTION_DURATION } = require('../helpers/constants')

const { setSnapshotId, snapshotId } = require('../helpers/make-suite')

const { timeLatest, evmRevert, evmSnapshot } = require('../helpers/utils')

describe('StakedEthix. Redeem', () => {
  let deployer, staker1, staker2, ethix, stkEthix, addr1, addr2, rewardsVault, emissionManager

  before(async () => {
    setSnapshotId(await evmSnapshot())

    const StakedETHIX = await ethers.getContractFactory('StakedETHIX')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const TransferHookTest = await ethers.getContractFactory('TransferHookTest')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let network = await ethers.getDefaultProvider().getNetwork()
    let signers = await ethers.getSigners()

    ;[deployer, staker1, staker2, addr1, addr2, emissionManager] = signers

    ethix = await EthixToken.deploy()
    await ethix.initialize()

    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    const transferHook = await TransferHookTest.deploy()

    stkEthix = await StakedETHIX.deploy()
    await stkEthix.initialize(
      ethix.address,
      transferHook.address,
      COOLDOWN_SECONDS,
      UNSTAKE_WINDOW,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    )
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), stkEthix.address)
  })

  it('Reverts trying to redeem 0 amount', async () => {
    const amount = '0'

    await expect(stkEthix.connect(staker1).redeem(staker1.address, amount)).to.be.revertedWith(
      'INVALID_ZERO_AMOUNT'
    )
  })

  it('User 1 stakes 50 ETHIX', async () => {
    const amount = ethers.utils.parseEther('50')

    await ethix.connect(staker1).approve(stkEthix.address, amount)
    await stkEthix.connect(staker1).stake(staker1.address, amount)
  })

  it('User 1 tries to redeem without activating the cooldown first', async () => {
    const amount = ethers.utils.parseEther('50')

    await expect(stkEthix.connect(staker1).redeem(staker1.address, amount)).to.be.revertedWith(
      'UNSTAKE_WINDOW_FINISHED'
    )
  })

  it('User 1 activates the cooldown, but is not able to redeem before the COOLDOWN_SECONDS passed', async () => {
    const amount = ethers.utils.parseEther('50')

    await stkEthix.connect(staker1).cooldown()

    const startedCooldownAt = await stkEthix.stakersCooldowns(staker1.address)
    const currentTime = await timeLatest()
    const remainingCooldown = startedCooldownAt.add(COOLDOWN_SECONDS).sub(currentTime)

    ethers.provider.send('evm_increaseTime', [remainingCooldown.div('2').toNumber()])
    ethers.provider.send('evm_mine')

    await expect(stkEthix.connect(staker1).redeem(staker1.address, amount)).to.be.revertedWith(
      'INSUFFICIENT_COOLDOWN'
    )

    // We fast-forward time to just before COOLDOWN_SECONDS
    ethers.provider.send('evm_mine', [
      startedCooldownAt.add(ethers.BigNumber.from(COOLDOWN_SECONDS).sub(1)).toNumber(),
    ])

    await expect(stkEthix.connect(staker1).redeem(staker1.address, amount)).to.be.revertedWith(
      'INSUFFICIENT_COOLDOWN'
    )

    // We fast-forward time to just after the unstake window
    ethers.provider.send('evm_mine', [
      startedCooldownAt.add(BigNumber.from(COOLDOWN_SECONDS).add(UNSTAKE_WINDOW).add(1)).toNumber(),
    ])

    await expect(stkEthix.connect(staker1).redeem(staker1.address, amount)).to.be.revertedWith(
      'UNSTAKE_WINDOW_FINISHED'
    )
  })

  it('User 1 activates the cooldown again, and tries to redeem a bigger amount that he has staked, receiving the balance', async () => {
    const amount = ethers.utils.parseEther('1000')

    await stkEthix.connect(staker1).cooldown()
    const startedCooldownAt = await stkEthix.stakersCooldowns(staker1.address)
    const currentTime = await timeLatest()

    const remainingCooldown = startedCooldownAt.add(COOLDOWN_SECONDS).sub(currentTime)

    ethers.provider.send('evm_increaseTime', [remainingCooldown.add(1).toNumber()])
    ethers.provider.send('evm_mine')

    const ethixBalanceBefore = await ethix.balanceOf(staker1.address)
    const stkEthixBalanceBefore = await stkEthix.balanceOf(staker1.address)

    await stkEthix.connect(staker1).redeem(staker1.address, amount)

    const ethixBalanceAfter = await ethix.balanceOf(staker1.address)
    const stkEthixBalanceAfter = await stkEthix.balanceOf(staker1.address)

    expect(ethixBalanceAfter.sub(stkEthixBalanceBefore)).to.be.equal(ethixBalanceBefore)
    expect(stkEthixBalanceAfter).to.be.equal(0)
  })

  it('User 1 activates the cooldown again, and redeems within the unstake period', async () => {
    const amount = ethers.utils.parseEther('50')

    await ethix.connect(staker1).approve(stkEthix.address, amount)
    await stkEthix.connect(staker1).stake(staker1.address, amount)

    await stkEthix.connect(staker1).cooldown()
    const startedCooldownAt = await stkEthix.stakersCooldowns(staker1.address)

    const currentTime = await timeLatest()

    const remainingCooldown = startedCooldownAt.add(COOLDOWN_SECONDS).sub(currentTime)

    ethers.provider.send('evm_increaseTime', [remainingCooldown.add(1).toNumber()])
    ethers.provider.send('evm_mine')

    const ethixBalanceBefore = await ethix.balanceOf(staker1.address)

    await stkEthix.connect(staker1).redeem(staker1.address, amount)
    const ethixBalanceAfter = await ethix.balanceOf(staker1.address)

    expect(ethixBalanceAfter.sub(amount.toString()).toString()).to.be.equal(
      ethixBalanceBefore.toString()
    )
  })

  it('User 2 stakes 50 ETHIX, activates the cooldown and redeems half of the amount', async () => {
    const amount = ethers.utils.parseEther('50')

    await ethix.connect(staker2).approve(stkEthix.address, amount)
    await stkEthix.connect(staker2).stake(staker2.address, amount)

    await stkEthix.connect(staker2).cooldown()

    const cooldownActivationTimestamp = await timeLatest()

    await ethers.provider.send('evm_mine', [
      cooldownActivationTimestamp.add(ethers.BigNumber.from(COOLDOWN_SECONDS).add(1)).toNumber(),
    ])

    const ethixBalanceBefore = await ethix.balanceOf(staker2.address)

    await stkEthix.connect(staker2).redeem(staker2.address, ethers.utils.parseEther('50').div(2))

    const ethixBalanceAfter = await ethix.balanceOf(staker2.address)
    expect(ethixBalanceAfter.sub(amount)).to.be.equal(ethixBalanceBefore.div(2))
  })

  it('User 2 stakes 50 ETHIX, activates the cooldown and redeems with rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('50')

    await ethix.connect(staker2).approve(stkEthix.address, amount)
    await stkEthix.connect(staker2).stake(staker2.address, amount)

    await stkEthix.connect(staker2).cooldown()

    const cooldownActivationTimestamp = await timeLatest()

    await ethers.provider.send('evm_mine', [
      cooldownActivationTimestamp.add(ethers.BigNumber.from(COOLDOWN_SECONDS).add(1)).toNumber(),
    ])

    const ethixBalanceBefore = await ethix.balanceOf(staker2.address)

    await stkEthix.connect(staker2).redeem(staker2.address, amount)

    const ethixBalanceAfter = await ethix.balanceOf(staker2.address)

    expect(ethixBalanceAfter.sub(amount.toString()).toString()).to.be.equal(
      ethixBalanceBefore.toString()
    )
  })

  it('Edit unstake window 1 hour / 3600 seconds, redeem rewards enabled when user 1 is on unstake window', async () => {
    const unstakeWindowSeconds = 3600
    await stkEthix.connect(emissionManager).setUnstakeWindow(unstakeWindowSeconds)

    const amount = ethers.utils.parseEther('50')

    await ethix.connect(staker1).approve(stkEthix.address, amount)
    await stkEthix.connect(staker1).stake(staker1.address, amount)

    await stkEthix.connect(staker1).cooldown()

    const cooldownActivationTimestamp = await timeLatest()
    const timeJump = cooldownActivationTimestamp.add(COOLDOWN_SECONDS).add(1)
    await ethers.provider.send('evm_mine', [timeJump.toNumber()])

    const ethixBalanceBefore = await ethix.balanceOf(staker1.address)

    await stkEthix.connect(staker1).redeem(staker1.address, amount)

    const ethixBalanceAfter = await ethix.balanceOf(staker1.address)
    expect(ethixBalanceAfter.sub(amount)).to.be.equal(ethixBalanceBefore)
  })

  it('Edit unstake window 1 hour / 3600 seconds, redeem rewards not enabled because edit another user who is not emmision manager', async () => {
    const unstakeWindowSeconds = 3600

    await expect(stkEthix.connect(staker2).setUnstakeWindow(unstakeWindowSeconds)).to.be.revertedWith(
      'ONLY_EMISSION_MANAGER'
    )
  })

  it('Edit unstake window to 1 hour / 3600 seconds, redeem rewards not enabled when user 1 is out of the unstake window', async () => {
    const unstakeWindowSeconds = 3600
    await stkEthix.connect(emissionManager).setUnstakeWindow(unstakeWindowSeconds)

    const amount = ethers.utils.parseEther('50')

    await ethix.connect(staker1).approve(stkEthix.address, amount)
    await stkEthix.connect(staker1).stake(staker1.address, amount)

    await stkEthix.connect(staker1).cooldown()

    const cooldownActivationTimestamp = await timeLatest()
    const timeJump = cooldownActivationTimestamp.add(COOLDOWN_SECONDS).add(unstakeWindowSeconds).add(1)
    await ethers.provider.send('evm_mine', [timeJump.toNumber()])

    await expect(stkEthix.connect(staker1).redeem(staker1.address, amount)).to.be.revertedWith(
      'UNSTAKE_WINDOW_FINISHED'
    )
  })

  it('Edit cooldown to one day / 86400 seconds, redeem rewards enabled when pass 1 day', async () => {
    const cooldownSeconds = 86400
    await stkEthix.connect(emissionManager).setCooldown(cooldownSeconds)

    const amount = ethers.utils.parseEther('50')

    ethix.connect(staker1).approve(stkEthix.address, amount),
    stkEthix.connect(staker1).stake(staker1.address, amount)

    await stkEthix.connect(staker1).cooldown()

    const cooldownActivationTimestamp = await timeLatest()
    const timeJump = cooldownActivationTimestamp.add(cooldownSeconds)
    await ethers.provider.send('evm_mine', [timeJump.toNumber()])

    const ethixBalanceBefore = await ethix.balanceOf(staker1.address)

    await stkEthix.connect(staker1).redeem(staker1.address, amount)

    const ethixBalanceAfter = await ethix.balanceOf(staker1.address)
    expect(ethixBalanceAfter.sub(amount)).to.be.equal(ethixBalanceBefore)
  })

  it('Edit cooldown to one day / 86400 seconds, redeem rewards not enabled because edit another user who is not emmision manager', async () => {
    const cooldownSeconds = 86400

    await expect(stkEthix.connect(staker2).setCooldown(cooldownSeconds)).to.be.revertedWith(
      'ONLY_EMISSION_MANAGER'
    )
  })

  it('Edit cooldown to one day / 86400 seconds, redeem rewards not enabled when pass only initial COOLDOWN == 1h', async () => {
    const cooldownSeconds = 86400
    await stkEthix.connect(emissionManager).setCooldown(cooldownSeconds)

    const amount = ethers.utils.parseEther('50')

    ethix.connect(staker1).approve(stkEthix.address, amount),
    stkEthix.connect(staker1).stake(staker1.address, amount)

    await stkEthix.connect(staker1).cooldown()

    const cooldownActivationTimestamp = await timeLatest()
    const timeJump = cooldownActivationTimestamp.add(COOLDOWN_SECONDS).add(1)
    await ethers.provider.send('evm_mine', [timeJump.toNumber()])

    await expect(stkEthix.connect(staker1).redeem(staker1.address, amount)).to.be.revertedWith(
      'INSUFFICIENT_COOLDOWN'
    )
  })

  it('Edit cooldown to 30 min, redeem rewards enabled to user 1 when pass new cooldown time', async () => {
    const amount = ethers.utils.parseEther('50')
    await ethix.transfer(staker1.address, amount)

    await ethix.connect(staker1).approve(stkEthix.address, amount)
    await stkEthix.connect(staker1).stake(staker1.address, amount)

    await stkEthix.connect(staker1).cooldown()

    const cooldownActivationTimestamp = await timeLatest()
    const timeJump = cooldownActivationTimestamp.add(2000).add(1)
    await ethers.provider.send('evm_mine', [timeJump.toNumber()])

    const cooldownSeconds = 1800
    await stkEthix.connect(emissionManager).setCooldown(cooldownSeconds)

    const ethixBalanceBefore = await ethix.balanceOf(staker1.address)

    await stkEthix.connect(staker1).redeem(staker1.address, amount)

    const ethixBalanceAfter = await ethix.balanceOf(staker1.address)
    expect(ethixBalanceAfter.sub(amount)).to.be.equal(ethixBalanceBefore)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
