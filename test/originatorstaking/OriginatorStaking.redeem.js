/*
    Originator Staking redeem test.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber } = require('ethers')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  APY_OBJETIVE
} = require('../helpers/constants')

const { setSnapshotId, snapshotId } = require('../helpers/make-suite')

const { evmRevert, evmSnapshot } = require('../helpers/utils')

const STAKING = 1
const STAKING_END = 2

describe('OriginatorStaking. Redeem', () => {
  let emissionManager, staker, staker2, ethix, addr1, auditor, originator, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker, staker2, ethix, addr1, auditor, originator, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))


    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)

    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )

    const emissionPerSecondExpected = BigNumber.from(STAKING_GOAL_ETHIX).mul(APY_OBJETIVE).div(10000).div(31536000)
    const config = {
      emissionPerSecond: emissionPerSecondExpected,
      totalStaked: STAKING_GOAL_ETHIX,
      underlyingAsset: originatorStaking.address
    }

    await originatorStaking.connect(emissionManager).configureAssets([config])
  })

  it('Reverts trying to redeem 0 amount', async () => {
    const amount = '0'

    expect(await originatorStaking.state()).to.be.equal(STAKING)
    await expect(originatorStaking.connect(staker).redeem(staker.address, amount)).to.be.revertedWith(
      'INVALID_ZERO_AMOUNT'
    )
  })

  it('User 1 stakes 20 ETHIX', async () => {
    const amount = ethers.utils.parseEther('20')
    const stakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await ethix.connect(staker).approve(originatorStaking.address, amount)
    await originatorStaking.connect(staker).stake(staker.address, amount)

    const stakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(stakerBalanceAfter.toString()).to.be.equal(stakerBalanceBefore.sub(amount.toString()))
    expect(originatorStakingBalanceAfter.toString()).to.be.equal(originatorStakingBalanceBefore.add(amount.toString()))
  })

  it('User 1 tries to redeem in STAKING state', async () => {
    const amount = ethers.utils.parseEther('20')

    expect(originatorStaking.connect(staker).redeem(staker.address, amount)).to.be.revertedWith(
      'WRONG_STATE'
    )
  })

  it('User 2 tries to stake 50 ETHIX, but stakes only 20 due to stakingGoal', async () => {
    const amount = ethers.utils.parseEther('50')
    const staker2BalanceBefore = await ethix.balanceOf(staker2.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)
    const availableStakeAmount = BigNumber.from(STAKING_GOAL_ETHIX).sub(originatorStakingBalanceBefore)

    await ethix.connect(staker2).approve(originatorStaking.address, amount)
    await originatorStaking.connect(staker2).stake(staker2.address, amount)

    const staker2BalanceAfter = await ethix.balanceOf(staker2.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(staker2BalanceAfter.toString()).to.be.equal((staker2BalanceBefore.sub(availableStakeAmount)).toString())
    expect(originatorStakingBalanceAfter.toString()).to.be.equal((originatorStakingBalanceBefore.add(availableStakeAmount)).toString())
  })

  it('Compare APY and APY objective', async () => {
    const stakingGoal = await originatorStaking.stakingGoal()
    expect(await originatorStaking.hasReachedGoal()).to.be.true
    contractBalance = await ethix.balanceOf(originatorStaking.address)
    expect(contractBalance).to.be.equal(stakingGoal)

    const currentDistribution = await originatorStaking.assets(originatorStaking.address)
    const currentEmission = currentDistribution.emissionPerSecond;
    const apy = currentEmission.mul(31536000).mul(10000).div(contractBalance)

    expect(apy.toNumber()).to.be.within(BigNumber.from(APY_OBJETIVE).sub('1').toNumber(), BigNumber.from(APY_OBJETIVE).add('1').toNumber()) // [range 0.01 percentage] 
  })

  it('only emission manager can change DISTRIBUTION_END date', async () => {
    const distributionPeriodSeconds = 3600 // 1 hour
    await expect(originatorStaking.connect(staker2).changeDistributionEndDate(distributionPeriodSeconds)).to.be.revertedWith(
      'ONLY_EMISSION_MANAGER'
    )
  })

  it('can change DISTRIBUTION_END date to 1 day before the initial DISTRIBUTION_END value', async () => {
    const initialDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    const distributionPeriodSeconds = 86400 // 1 day

    await originatorStaking.connect(emissionManager).changeDistributionEndDate(initialDistributionEnd.sub(distributionPeriodSeconds))
    const newDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    expect(newDistributionEnd).to.be.equal(initialDistributionEnd.sub(distributionPeriodSeconds))
  })

  it('can change DISTRIBUTION_END date to 1 day after the initial DISTRIBUTION_END value', async () => {
    const initialDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    const distributionPeriodSeconds = 86400 // 1 day

    await originatorStaking.connect(emissionManager).changeDistributionEndDate(initialDistributionEnd.add(distributionPeriodSeconds))
    const newDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    expect(newDistributionEnd).to.be.equal(initialDistributionEnd.add(distributionPeriodSeconds))
  })

  it('can declare STAKING_END before DISTRIBUTION_END date', async () => {
    const initialDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    const distributionPeriodSeconds = 3600 // 1 hour

    await ethers.provider.send('evm_increaseTime', [distributionPeriodSeconds])
    await ethers.provider.send('evm_mine')

    await originatorStaking.connect(governance).declareStakingEnd()

    const newDistributionEnd = await originatorStaking.DISTRIBUTION_END()

    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
    expect(newDistributionEnd).not.to.be.equal(initialDistributionEnd)
    expect(initialDistributionEnd).to.be.above(newDistributionEnd)
  })

  it('User 1 tries to redeem a bigger amount that he has staked, receiving the balance', async () => {
    const amount = ethers.utils.parseEther('1000')

    const ethixStakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingStakerBalanceBefore = await originatorStaking.balanceOf(staker.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await originatorStaking.connect(staker).redeem(staker.address, amount)

    const ethixStakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingStakerBalanceAfter = await originatorStaking.balanceOf(staker.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixStakerBalanceAfter.sub(originatorStakingStakerBalanceBefore)).to.be.equal(ethixStakerBalanceBefore)
    expect(originatorStakingStakerBalanceAfter).to.be.equal(0)
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.sub(originatorStakingStakerBalanceBefore))
  })

  it('User 1 tries to redeem again when his/her balance on the contract is already 0', async () => {
    const amount = ethers.utils.parseEther('20')
    expect(originatorStaking.connect(staker).redeem(staker.address, amount))
    .to.be.revertedWith('SENDER_BALANCE_ZERO')
  })

  it('User 2 tries to redeem half of what staked previously (10 Ethix)', async () => {
    const ethixStaker2BalanceBefore = await ethix.balanceOf(staker2.address)
    const originatorStakingStaker2BalanceBefore = await originatorStaking.balanceOf(staker2.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await originatorStaking.connect(staker2).redeem(staker2.address, originatorStakingStaker2BalanceBefore.div(2))

    const ethixStaker2BalanceAfter = await ethix.balanceOf(staker2.address)
    const originatorStakingStaker2BalanceAfter = await originatorStaking.balanceOf(staker2.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixStaker2BalanceAfter).to.be.equal(ethixStaker2BalanceBefore.add(originatorStakingStaker2BalanceBefore.div(2)))
    expect(originatorStakingStaker2BalanceAfter).to.be.equal(originatorStakingStaker2BalanceBefore.sub(originatorStakingStaker2BalanceBefore.div(2)))
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.sub(originatorStakingStaker2BalanceBefore.div(2)))
  })


  after(async () => {
    await evmRevert(snapshotId)
  })
})
