/*
    Originator Staking STAKING_END and DEFAULT states test.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { expect } = require('chai')
const { ethers } = require('hardhat')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  DEFAULT_DEBT_AMOUNT
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { evmRevert, evmSnapshot } = require('../helpers/utils')

const STAKING = 1
const STAKING_END = 2
const DEFAULT = 3

const AUDITOR_ROLE = ethers.utils.id('AUDITOR_ROLE')
const ORIGINATOR_ROLE = ethers.utils.id('ORIGINATOR_ROLE')
const GOVERNANCE_ROLE = ethers.utils.id('GOVERNANCE_ROLE')

describe('OriginatorStaking. DEFAULT state', () => {
  let emissionManager, staker, originator, auditor, ethix, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker, originator, auditor, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))


    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )
  })

  it('can not declare as DEFAULT because DEFAULT_DATE is not valid', async () => {
    expect(await originatorStaking.state()).to.be.equal(STAKING)

    expect(originatorStaking.connect(governance).declareDefault(DEFAULT_DEBT_AMOUNT))
    .to.be.revertedWith('DEFAULT_DATE_NOT_REACHED')
  })

  it('staker stakes 20 ETHIX', async () => {
    const amount = ethers.utils.parseEther('20')
    const stakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await ethix.connect(staker).approve(originatorStaking.address, amount)
    await originatorStaking.connect(staker).stake(staker.address, amount)

    const stakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(stakerBalanceAfter.toString()).to.be.equal(stakerBalanceBefore.sub(amount.toString()))
    expect(originatorStakingBalanceAfter.toString()).to.be.equal(originatorStakingBalanceBefore.add(amount.toString()))
  })

  it('only governance role can declare as DEFAULT', async () => {
    await ethers.provider.send('evm_increaseTime', [259200]) // 3 days
    await ethers.provider.send('evm_mine')

    expect(await originatorStaking.state()).to.be.equal(STAKING)

    expect(originatorStaking.connect(emissionManager).declareDefault(DEFAULT_DEBT_AMOUNT))
    .to.be.revertedWith('ONLY_GOVERNANCE')
  })

  it('can not liquidate because state is not default', async () => {
    expect(await originatorStaking.state()).to.not.be.equal(DEFAULT)
    expect(originatorStaking.connect(governance).liquidateProposerStake(
      ethers.utils.parseEther('100'),
      AUDITOR_ROLE)).to.be.revertedWith('ONLY_ON_DEFAULT')
  })

  it('can declare contract as DEFAULT, DISTRIBUTION_END and DEFAULT_DATE already finished', async () => {
    const distributionEnd = await originatorStaking.DISTRIBUTION_END()
    const defaultDate = await originatorStaking.DEFAULT_DATE()
    const calculatedDistributionEnd = defaultDate.sub(DEFAULT_DELAY)
    expect(distributionEnd.toNumber()).to.be.equal(calculatedDistributionEnd.toNumber())
    expect(await originatorStaking.state()).to.be.equal(STAKING)

    await originatorStaking.connect(governance).declareDefault(DEFAULT_DEBT_AMOUNT)

    const newDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    expect(await originatorStaking.defaultedAmount()).to.be.equal(DEFAULT_DEBT_AMOUNT)
    expect(newDistributionEnd).to.be.equal(distributionEnd)
    expect(await originatorStaking.state()).to.be.equal(DEFAULT)
  })

  it('staker can redeem in DEFAULT state because defaulted amount is less than proposer deposit amount', async () => {
    const ethixStakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingStakerBalanceBefore = await originatorStaking.balanceOf(staker.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await originatorStaking.connect(staker).redeem(staker.address, ethixStakerBalanceBefore)

    const ethixStakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingStakerBalanceAfter = await originatorStaking.balanceOf(staker.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixStakerBalanceAfter.sub(originatorStakingStakerBalanceBefore)).to.be.equal(ethixStakerBalanceBefore)
    expect(originatorStakingStakerBalanceAfter).to.be.equal(0)
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.sub(originatorStakingStakerBalanceBefore))
  })

  it('auditor can liquidate partially, 10 of 20 Ethix', async () => {
    const amount = ethers.utils.parseEther('10')
    const auditorBalanceBefore = await ethix.balanceOf(auditor.address)
    const governanceBalanceBefore = await ethix.balanceOf(governance.address)
    const OSBalanceBefore = await ethix.balanceOf(originatorStaking.address)
    const auditorDepositAmountBefore = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorDepositAmountBefore = await originatorStaking.proposerBalances(ORIGINATOR_ROLE)
    
    await originatorStaking.connect(governance).liquidateProposerStake(
      amount,
      AUDITOR_ROLE
    )

    const auditorBalanceAfter = await ethix.balanceOf(auditor.address)
    const governanceBalanceAfter = await ethix.balanceOf(governance.address)
    const auditorDepositAmountAfter = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorDepositAmountAfter = await originatorStaking.proposerBalances(ORIGINATOR_ROLE)

    expect(auditorBalanceAfter.toString()).to.equal(auditorBalanceBefore.toString())
    expect(await ethix.balanceOf(originatorStaking.address)).to.equal(OSBalanceBefore.sub(amount))
    expect(auditorDepositAmountAfter).to.equal(auditorDepositAmountBefore.sub(amount))
    expect(originatorDepositAmountAfter).to.equal(originatorDepositAmountBefore)
    expect(governanceBalanceAfter).to.equal(governanceBalanceBefore.add(amount))
  })

  it('originator can liquidate all, 40 of 40 Ethix', async () => {
    const amount = ethers.utils.parseEther('40')
    const originatorBalanceBefore = await ethix.balanceOf(originator.address)
    const governanceBalanceBefore = await ethix.balanceOf(governance.address)
    const OSBalanceBefore = await ethix.balanceOf(originatorStaking.address)
    const auditorDepositAmountBefore = await originatorStaking.proposerBalances(AUDITOR_ROLE)

    await originatorStaking.connect(governance).liquidateProposerStake(
      amount,
      ORIGINATOR_ROLE
    )

    const originatorBalanceAfter = await ethix.balanceOf(originator.address)
    const governanceBalanceAfter = await ethix.balanceOf(governance.address)
    const auditorDepositAmountAfter = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorDepositAmountAfter = await originatorStaking.proposerBalances(ORIGINATOR_ROLE)
    
    expect(originatorBalanceAfter.toString()).to.equal(originatorBalanceBefore.toString())
    expect(await ethix.balanceOf(originatorStaking.address)).to.equal(OSBalanceBefore.sub(amount))
    expect(auditorDepositAmountAfter).to.equal(auditorDepositAmountBefore)
    expect(originatorDepositAmountAfter).to.equal(0)
    expect(governanceBalanceAfter).to.equal(governanceBalanceBefore.add(amount))

  })

  it('auditor can not liquidate more than deposited', async () => {
    const amount = ethers.utils.parseEther('40')

    expect(originatorStaking.connect(governance).liquidateProposerStake(
      amount, AUDITOR_ROLE)).to.be.revertedWith('INVALID_LIQUIDATE_AMOUNT')
  })

  it('only governance can liquidate', async () => {
    expect(originatorStaking.connect(emissionManager).liquidateProposerStake(
      ethers.utils.parseEther('100'),
      AUDITOR_ROLE)).to.be.revertedWith('ONLY_GOVERNANCE')
  })

  it('can not liquidate 0 amount', async () => {
    expect(originatorStaking.connect(governance).liquidateProposerStake(
      ethers.utils.parseEther('0'),
      AUDITOR_ROLE)).to.be.revertedWith('INVALID_ZERO_AMOUNT')
  })

  it('wrong proposer role can not liquidate', async () => {
    expect(originatorStaking.connect(governance).liquidateProposerStake(
      ethers.utils.parseEther('100'),
      GOVERNANCE_ROLE)).to.be.revertedWith('INVALID_PROPOSER_ROLE')
  })

  it('can not declare STAKING_END in DEFAULT state', async () => {
    expect(originatorStaking.connect(governance).declareStakingEnd()).to.be.revertedWith(
      'ONLY_ON_STAKING_STATE'
    )
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
describe('OriginatorStaking. DEFAULT state with stakingGoal as debt amount', () => {
  let emissionManager, staker, originator, auditor, ethix, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker, originator, auditor, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))


    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )
  })

  it('staker stakes 20 ETHIX', async () => {
    const amount = ethers.utils.parseEther('20')
    const stakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await ethix.connect(staker).approve(originatorStaking.address, amount)
    await originatorStaking.connect(staker).stake(staker.address, amount)

    const stakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(stakerBalanceAfter.toString()).to.be.equal(stakerBalanceBefore.sub(amount.toString()))
    expect(originatorStakingBalanceAfter.toString()).to.be.equal(originatorStakingBalanceBefore.add(amount.toString()))
  })

  it('can declare contract as DEFAULT, DISTRIBUTION_END and DEFAULT_DATE already finished', async () => {
    const distributionEnd = await originatorStaking.DISTRIBUTION_END()
    const defaultDate = await originatorStaking.DEFAULT_DATE()
    const calculatedDistributionEnd = defaultDate.sub(DEFAULT_DELAY)
    expect(distributionEnd.toNumber()).to.be.equal(calculatedDistributionEnd.toNumber())
    expect(await originatorStaking.state()).to.be.equal(STAKING)

    await ethers.provider.send('evm_increaseTime', [259200]) // 3 days
    await ethers.provider.send('evm_mine')

    const stakingGoal = await originatorStaking.stakingGoal()
    const defaultedAmountBefore = await originatorStaking.defaultedAmount()
    await originatorStaking.connect(governance).declareDefault(stakingGoal)

    const defaultedAmountAfter = await originatorStaking.defaultedAmount()
    expect(defaultedAmountBefore).to.be.equal(0)
    expect(defaultedAmountAfter).to.be.equal(stakingGoal)
    expect(await originatorStaking.state()).to.be.equal(DEFAULT)
  })

  it('staker can not redeem in DEFAULT state because defaulted amount is more than proposer deposit amount', async () => {
    const ethixStakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingStakerBalanceBefore = await originatorStaking.balanceOf(staker.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    expect(originatorStaking.connect(staker).redeem(staker.address, ethixStakerBalanceBefore)).to.be.revertedWith(
      'WRONG_STATE'
    )
    const ethixStakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingStakerBalanceAfter = await originatorStaking.balanceOf(staker.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixStakerBalanceAfter).to.be.equal(ethixStakerBalanceBefore)
    expect(originatorStakingStakerBalanceAfter).to.be.equal(originatorStakingStakerBalanceBefore)
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})

describe('OriginatorStaking. STAKING_END state', () => {
  let emissionManager, staker, originator, auditor, ethix, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker, originator, auditor, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStaking.connect(governance).declareStakingEnd()
  })

  it('only can be declared as DEFAULT in STAKING state', async () => {
    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
    expect(originatorStaking.connect(governance).declareDefault(DEFAULT_DEBT_AMOUNT))
    .to.be.revertedWith('ONLY_ON_STAKING_STATE')
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
