/*
    Originator Staking transfer test.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { ethers } = require('hardhat')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY
} = require('../helpers/constants')

const { compareRewardsAtAction, compareRewardsAtTransfer } = require('../helpers/reward')
const { evmRevert, evmSnapshot } = require('../helpers/utils')
const { setSnapshotId, snapshotId } = require('../helpers/make-suite')

describe('OriginatorStaking. Transfers', () => {
  let emissionManager, staker, staker2, staker3, originator, auditor, ethix, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker, staker2, staker3, originator, auditor, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))


    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )
  })

  it('User 1 stakes 30 ETHIX', async () => {
    const amount = ethers.utils.parseEther('30')

    const actions = () => [
      ethix.connect(staker).approve(originatorStaking.address, amount),
      originatorStaking.connect(staker).stake(staker.address, amount),
    ]
    await compareRewardsAtAction(originatorStaking, staker.address, actions)
  })

  it('User 1 transfers 30 originatorStaking to User 2', async () => {
    const amount = ethers.utils.parseEther('30')
    const balance = await originatorStaking.balanceOf(staker.address)
    await compareRewardsAtTransfer(originatorStaking, staker, staker2, amount, true, false)
  })

  it('User 2 transfers 30 originatorStaking to himself', async () => {
    const amount = ethers.utils.parseEther('30')
    await compareRewardsAtTransfer(originatorStaking, staker2, staker2, amount, true, true)
  })

  it('User 2 transfers 30 originatorStaking to user 2, with rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('30')

    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }
    await compareRewardsAtTransfer(originatorStaking, staker2, staker2, amount, false, false, assetConfig)
  })

  it('User 3 stakes and transfers 10 originatorStaking to user 2, with rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('10')
    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    const actions = () => [
      ethix.connect(staker3).approve(originatorStaking.address, amount),
      originatorStaking.connect(staker3).stake(staker3.address, amount),
    ]

    await compareRewardsAtAction(originatorStaking, staker3.address, actions, false, assetConfig)
    await compareRewardsAtTransfer(originatorStaking, staker3, staker2, amount, false, false, assetConfig)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
