const { expect } = require('chai')
const { ethers } = require('hardhat')
const chai = require('chai')
const { solidity } = require('ethereum-waffle')

chai.use(solidity)

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { evmRevert, evmSnapshot } = require('../helpers/utils')
const { BigNumber } = require('ethers')

describe('OriginatorStaking. Rewards increment ', () => {
  let emissionManager, originator, auditor, lendingContract, ethix, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, originator, auditor, lendingContract, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))


    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )
  })



  it('Adds correctly to current rewards when project funding', async () => {
    const assetConfiguration = {
      emissionPerSecond: '100',
      totalStaked: await originatorStaking.totalSupply(),
      underlyingAsset: originatorStaking.address,
    }
    await originatorStaking.configureAssets([assetConfiguration])

    const assetConf = await originatorStaking.assets(originatorStaking.address)
    expect(assetConf.emissionPerSecond).to.equal(BigNumber.from('100'))

    await expect(originatorStaking.startProjectFundedRewards('50', lendingContract.address))
      .to.emit(originatorStaking, 'StartRewardsProjectFunded')
      .withArgs(BigNumber.from('100'), BigNumber.from('50'), lendingContract.address)

    const newAssetConfs = await originatorStaking.assets(originatorStaking.address)
    expect(newAssetConfs.emissionPerSecond).to.equal(BigNumber.from('150'))

  })

  it('Adds removes to current rewards when ending period', async () => {
    const assetConfiguration = {
      emissionPerSecond: '150',
      totalStaked: await originatorStaking.totalSupply(),
      underlyingAsset: originatorStaking.address,
    }
    await originatorStaking.configureAssets([assetConfiguration])

    const assetConf = await originatorStaking.assets(originatorStaking.address)
    expect(assetConf.emissionPerSecond).to.equal(BigNumber.from('150'))

    await expect(originatorStaking.endProjectFundedRewards('50', lendingContract.address))
      .to.emit(originatorStaking, 'EndRewardsProjectFunded')
      .withArgs(BigNumber.from('150'), BigNumber.from('50'), lendingContract.address)

    const newAssetConfs = await originatorStaking.assets(originatorStaking.address)
    expect(newAssetConfs.emissionPerSecond).to.equal(BigNumber.from('100'))

  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
