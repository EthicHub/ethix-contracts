/*
    Originator Staking basic test.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber } = require('ethers')

const {
  MAX_UINT_AMOUNT,
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { compareRewardsAtAction } = require('../helpers/reward')
const { getUserIndex, getRewards, evmRevert, evmSnapshot, timeLatest } = require('../helpers/utils')

const UNINITIALIZED = 0
const STAKING = 1
const STAKING_END = 2
const DEFAULT = 3

const AUDITOR_ROLE = ethers.utils.id('AUDITOR_ROLE')
const ORIGINATOR_ROLE = ethers.utils.id('ORIGINATOR_ROLE')
const EMISSION_MANAGER_ROLE = ethers.utils.id('EMISSION_MANAGER')
const GOVERNANCE_ROLE = ethers.utils.id('GOVERNANCE_ROLE')

describe('OriginatorStaking. Basics', () => {
  let emissionManager, staker, originator, auditor, ethix, addr2, addr3, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker, originator, auditor, ethix, addr2, addr3, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
  })

  it('Initial configuration after initialize() is correct', async () => {
    expect(await originatorStaking.name()).to.equal('OriginatorTest')
    expect(await originatorStaking.symbol()).to.equal('OT')
    expect(await originatorStaking.decimals()).to.equal(18)
    expect(await originatorStaking.STAKED_TOKEN()).to.equal(ethix.address)
    expect(await originatorStaking.REWARDS_VAULT()).to.equal(rewardsVault.address)
    expect(await originatorStaking.hasRole(EMISSION_MANAGER_ROLE, emissionManager.address)).to.be.true
  })

  it('Not set staking goal yet', async () => {
    expect(originatorStaking.hasReachedGoal())
      .to.be.revertedWith('INVALID_ZERO_AMOUNT')
  })

  it('Reverts trying to stake on diferent state of STAKING', async () => {
    expect(await originatorStaking.state()).to.be.equal(UNINITIALIZED)
    expect(originatorStaking.connect(staker).stake(staker.address, ethers.utils.parseEther('50'))).to.be.revertedWith(
      'ONLY_ON_STAKING_STATE'
    )
  })

  it('can setup terms', async () => {
    const amount = ethers.utils.parseEther('100')
    const auditorBalanceBefore = await ethix.balanceOf(auditor.address)
    const originatorBalanceBefore = await ethix.balanceOf(originator.address)

    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    expect(await originatorStaking.state()).to.be.equal(UNINITIALIZED)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )

    const auditorDepositAmount = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorDepositAmount = await originatorStaking.proposerBalances(ORIGINATOR_ROLE)
    const auditorTransferredAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
    const originatorTransferredAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)
    const auditorBalanceAfter = await ethix.balanceOf(auditor.address)
    const originatorBalanceAfter = await ethix.balanceOf(originator.address)

    expect(await originatorStaking.hasRole(AUDITOR_ROLE, auditor.address)).to.be.true
    expect(await originatorStaking.hasRole(ORIGINATOR_ROLE, originator.address)).to.be.true
    expect(await originatorStaking.hasRole(GOVERNANCE_ROLE, governance.address)).to.be.true
    expect(auditorDepositAmount.toString()).to.equal(auditorTransferredAmount.toString())
    expect(originatorDepositAmount.toString()).to.equal(originatorTransferredAmount.toString())
    expect(auditorBalanceAfter).to.equal(auditorBalanceBefore.sub(auditorTransferredAmount))
    expect(originatorBalanceAfter).to.equal(originatorBalanceBefore.sub(originatorTransferredAmount))
    expect(await ethix.balanceOf(originatorStaking.address)).to.equal(auditorDepositAmount.add(originatorDepositAmount))
    expect(await originatorStaking.state()).to.be.equal(STAKING)
  })

  it('Validates change distribution period', async () => {
    const currentTime = await timeLatest()
    const firstDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    expect(firstDistributionEnd.toNumber()).to.be.within(currentTime.add(DISTRIBUTION_DURATION - 10).toNumber(), currentTime.add(DISTRIBUTION_DURATION + 10).toNumber()) // [1 day - 10 seconds, 1 day + 10 seconds]

    const distributionPeriodSeconds = 31536000
    await originatorStaking.connect(emissionManager).changeDistributionEndDate(firstDistributionEnd.add(distributionPeriodSeconds))
    const nextDistributionEnd = await originatorStaking.DISTRIBUTION_END()
    expect(nextDistributionEnd.toNumber()).to.be.within(firstDistributionEnd.add(31536000).toNumber(), firstDistributionEnd.add(31536002).toNumber()) // [1 year - 2 seconds, 1 year + 2 seconds]
  })

  it('Not reached goal', async () => {
    expect(await originatorStaking.hasReachedGoal()).to.be.false
  })

  it('only emission manager can set up terms', async () => {
    expect(originatorStaking.connect(auditor).setUpTerms(auditor.address, originator.address, governance.address,
      AUDITOR_SETUP_PERCENTAGE, ORIGINATOR_SETUP_PERCENTAGE, STAKING_GOAL_ETHIX, DEFAULT_DELAY))
      .to.be.revertedWith('ONLY_EMISSION_MANAGER')
  })

  it('can not set up terms with same addresses', async () => {
    expect(originatorStaking.connect(emissionManager).setUpTerms(auditor.address, auditor.address, governance.address,
      AUDITOR_SETUP_PERCENTAGE, ORIGINATOR_SETUP_PERCENTAGE, STAKING_GOAL_ETHIX, DEFAULT_DELAY))
      .to.be.revertedWith('PROPOSERS_CANNOT_BE_THE_SAME')
  })

  it('can not set up terms with 0 percentage', async () => {
    expect(originatorStaking.connect(emissionManager).setUpTerms(auditor.address, originator.address, governance.address,
      0, ORIGINATOR_SETUP_PERCENTAGE, STAKING_GOAL_ETHIX, DEFAULT_DELAY))
      .to.be.revertedWith('INVALID_PERCENTAGE_ZERO')
  })

  it('can not set up terms with 0 referenceGoal', async () => {
    expect(originatorStaking.connect(emissionManager).setUpTerms(auditor.address, originator.address, governance.address,
      AUDITOR_SETUP_PERCENTAGE, ORIGINATOR_SETUP_PERCENTAGE, 0, DEFAULT_DELAY))
      .to.be.revertedWith('INVALID_ZERO_AMOUNT')
  })

  it('Reverts trying to stake 0 amount', async () => {
    expect(originatorStaking.connect(emissionManager).stake(staker.address, 0)).to.be.revertedWith(
      'INVALID_ZERO_AMOUNT'
    )
  })

  it('User 1 stakes 30 ETHIX: receives 30 stkETHIX, StakedEthix balance of ETHIX is 30 and his rewards to claim are 0', async () => {
    const amount = ethers.utils.parseEther('30')

    const saveBalanceBefore = await originatorStaking.balanceOf(staker.address)
    const proposersBalance = (await originatorStaking.proposerBalances(AUDITOR_ROLE))
    .add(await originatorStaking.proposerBalances(ORIGINATOR_ROLE))
    expect(await originatorStaking.state()).to.be.equal(STAKING)

    // Prepare actions for the test case
    const actions = () => [
      ethix.connect(staker).approve(originatorStaking.address, amount),
      originatorStaking.connect(staker).stake(staker.address, amount),
    ]

    // Check rewards
    await compareRewardsAtAction(originatorStaking, staker.address, actions)

    // Stake token tests
    expect((await originatorStaking.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount.toString()).toString()
    )
    expect((await ethix.balanceOf(originatorStaking.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount.toString()).add(proposersBalance).toString()
    )
    expect((await originatorStaking.balanceOf(staker.address)).toString()).to.be.equal(amount)
    expect((await ethix.balanceOf(originatorStaking.address)).toString()).to.be.equal(amount.add(proposersBalance))
  })

  it('User 1 stakes 5 ETHIX more: his total stkETHIX balance increases, stkETHIX balance of Ethix increases and his reward until now get accumulated', async () => {
    expect(await originatorStaking.hasReachedGoal()).to.be.false
    const amount = ethers.utils.parseEther('5')

    const saveBalanceBefore = await originatorStaking.balanceOf(staker.address)
    const proposersBalance = (await originatorStaking.proposerBalances(AUDITOR_ROLE))
    .add(await originatorStaking.proposerBalances(ORIGINATOR_ROLE))
    const actions = () => [
      ethix.connect(staker).approve(originatorStaking.address, amount),
      originatorStaking.connect(staker).stake(staker.address, amount),
    ]
    
    // Checks rewards
    await compareRewardsAtAction(originatorStaking, staker.address, actions, true)

    // Extra test checks
    expect((await originatorStaking.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount)
    )
    expect((await ethix.balanceOf(originatorStaking.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount).add(proposersBalance)
    )
  })

  it('User 2 stakes 1 ETHIX, with the rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('1')

    // Disable rewards via config
    const assetsConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // Checks rewards
    const actions = () => [
      ethix.connect(addr2).approve(originatorStaking.address, amount),
      originatorStaking.connect(addr2).stake(addr2.address, amount),
    ]

    await compareRewardsAtAction(originatorStaking, addr2.address, actions, false, assetsConfig)

    // Check expected stake balance for second staker
    expect(await originatorStaking.balanceOf(addr2.address)).to.equal(amount)

    // Expect rewards balance to still be zero
    const rewardsBalance = await originatorStaking.getTotalRewardsBalance(addr2.address)
    expect(rewardsBalance).to.be.equal('0')
  })

  it('User 3 stakes 2 ETHIX more, with the rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('2')

    // Keep rewards disabled via config
    const assetsConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // Checks rewards
    const actions = () => [
      ethix.connect(addr3).approve(originatorStaking.address, amount),
      originatorStaking.connect(addr3).stake(addr3.address, amount),
    ]

    await compareRewardsAtAction(originatorStaking, addr3.address, actions, false, assetsConfig)

    // Expect rewards balance to still be zero
    const rewardsBalance = await originatorStaking.getTotalRewardsBalance(addr3.address)
    expect(rewardsBalance).to.be.equal('0')
  })

  it('User 1 stakes 10 ETHIX more, but only stake 2 to cap the goal: his total stkETHIX balance increases, stkETHIX balance of Ethix increases and his reward until now get accumulated', async () => {
    expect(await originatorStaking.hasReachedGoal()).to.be.false
    const amount = ethers.utils.parseEther('10')
    const user2StakedAmount = ethers.utils.parseEther('2')
    const user3StakedAmount = ethers.utils.parseEther('1')

    const saveBalanceBefore = await originatorStaking.balanceOf(staker.address)
    const proposersBalance = (await originatorStaking.proposerBalances(AUDITOR_ROLE))
    .add(await originatorStaking.proposerBalances(ORIGINATOR_ROLE))
    const actions = () => [
      ethix.connect(staker).approve(originatorStaking.address, amount),
      originatorStaking.connect(staker).stake(staker.address, amount),
    ]
    const stakingGoal = await originatorStaking.stakingGoal()
    const balanceStaked = await ethix.balanceOf(originatorStaking.address)
    const amountStaked = stakingGoal.sub(balanceStaked)

    // Checks rewards
    await compareRewardsAtAction(originatorStaking, staker.address, actions, true)

    // Extra test checks
    expect((await originatorStaking.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amountStaked)
    )
    expect((await ethix.balanceOf(originatorStaking.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amountStaked).add(proposersBalance).add(user2StakedAmount).add(user3StakedAmount)
    )
  })

  it('Reached goal', async () => {
    expect(await originatorStaking.hasReachedGoal()).to.be.true
  })

  it('Should not stake when goal reached', async() => {
    expect(await originatorStaking.hasReachedGoal()).to.be.true
    const amount = ethers.utils.parseEther('10')
    ethix.connect(staker).approve(originatorStaking.address, amount)
    expect(originatorStaking.connect(staker).stake(staker.address, amount)).to.be.revertedWith('GOAL_HAS_REACHED')
  })

  it('Should not liquidate when state != DEFAULT', async() => {
    expect(await originatorStaking.state()).to.be.not.equal(DEFAULT)
    const amount = ethers.utils.parseEther('10')
    expect(originatorStaking.connect(governance).liquidateProposerStake(amount, AUDITOR_ROLE)).to.be.revertedWith('ONLY_ON_DEFAULT')
  })

  it('Should not withdraw when state != STAKING_END', async() => {
    expect(await originatorStaking.state()).to.be.not.equal(STAKING_END)
    const amount = ethers.utils.parseEther('10')
    expect(originatorStaking.connect(auditor).withdrawProposerStake(amount)).to.be.revertedWith('ONLY_ON_STAKING_END_STATE')
  })

  it('Only governance declare staking end', async () => {
    expect(await originatorStaking.hasReachedGoal()).to.be.true
    expect(await originatorStaking.state()).to.be.equal(STAKING)
    expect(originatorStaking.connect(emissionManager).declareStakingEnd())
      .to.be.revertedWith('ONLY_GOVERNANCE')
  })

  it('Should declare staking end', async () => {
    expect(await originatorStaking.hasReachedGoal()).to.be.true
    expect(await originatorStaking.state()).to.be.equal(STAKING)
    await originatorStaking.connect(governance).declareStakingEnd()
    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
  })

  it('Should not stake when state is stake end', async() => {
    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
    const amount = ethers.utils.parseEther('10')
    ethix.connect(staker).approve(originatorStaking.address, amount)
    expect(originatorStaking.connect(staker).stake(staker.address, amount)).to.be.revertedWith('ONLY_ON_STAKING_STATE')
  })

  it('Should redeem when state is stake end', async() => {
    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
    const amount = ethers.utils.parseEther('10')
    const beforeRedeemAmount = await ethix.balanceOf(staker.address)
    await originatorStaking.connect(staker).redeem(staker.address, amount)
    const afterRedeemAmount = await ethix.balanceOf(staker.address)
    expect(afterRedeemAmount).to.be.equal(beforeRedeemAmount.add(amount))
  })

  it('Should withdraw when state is stake end', async() => {
    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
    const auditorPercentage = BigNumber.from(AUDITOR_SETUP_PERCENTAGE)
    const stakingGoal = BigNumber.from(STAKING_GOAL_ETHIX)
    const amount = stakingGoal.mul(auditorPercentage).div(10000)
    const beforeWithdrawAmount = await ethix.balanceOf(auditor.address)
    await originatorStaking.connect(auditor).withdrawProposerStake(amount)
    const afterWithdrawAmount = await ethix.balanceOf(auditor.address)
    expect(afterWithdrawAmount).to.be.equal(beforeWithdrawAmount.add(amount))
  })

  it('User 1 claim half rewards ', async () => {
    const distributionEnd = await originatorStaking.DISTRIBUTION_END()
    // Increase time for bigger rewards
    ethers.provider.send('evm_increaseTime', [distributionEnd.toNumber()])
    ethers.provider.send('evm_mine')

    const halfRewards = (await originatorStaking.stakerRewardsToClaim(staker.address)).div(2)
    console.log('-->halfRewards', halfRewards.toString())
    const saveUserBalance = await ethix.balanceOf(staker.address)

    await originatorStaking.connect(staker).claimRewards(staker.address, halfRewards)

    const userBalanceAfterActions = await ethix.balanceOf(staker.address)
    expect(userBalanceAfterActions.eq(saveUserBalance.add(halfRewards))).to.be.ok
  })

  it('User 1 tries to claim higher reward than current rewards balance', async () => {
    const saveUserBalance = await ethix.balanceOf(staker.address)

    // Try to claim more amount than accumulated
    await expect(
      originatorStaking.connect(staker).claimRewards(staker.address, ethers.utils.parseEther('10000'))
    ).to.be.revertedWith('INVALID_AMOUNT')

    const userBalanceAfterActions = await ethix.balanceOf(staker.address)
    expect(userBalanceAfterActions.eq(saveUserBalance)).to.be.ok
  })

  it('User 1 claim all rewards', async () => {
    const userAddress = staker.address
    const underlyingAsset = originatorStaking.address

    const userBalance = await originatorStaking.balanceOf(userAddress)
    const userEthixBalance = await ethix.balanceOf(userAddress)

    const userRewards = await originatorStaking.stakerRewardsToClaim(userAddress)

    // Get index before actions
    const userIndexBefore = await getUserIndex(originatorStaking, userAddress, underlyingAsset)

    // Claim rewards
    await originatorStaking.connect(staker).claimRewards(staker.address, MAX_UINT_AMOUNT)

    // Get index after actions
    const userIndexAfter = await getUserIndex(originatorStaking, userAddress, underlyingAsset)

    const expectedAccruedRewards = getRewards(userBalance, userIndexAfter, userIndexBefore)
    const userEthixBalanceAfterAction = (await ethix.balanceOf(userAddress)).toString()

    expect(userEthixBalanceAfterAction).to.be.equal(
      userEthixBalance.add(userRewards).add(expectedAccruedRewards).toString()
    )
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
