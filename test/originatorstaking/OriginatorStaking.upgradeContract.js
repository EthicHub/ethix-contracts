/*
    Originator Staking upgrade test.

    Copyright (C) 2022 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber } = require('ethers')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  NEW_ORIGINATOR_SETUP_PERCENTAGE,
  NEW_STAKING_GOAL_ETHIX,
  NEW_DISTRIBUTION_DURATION,
  EMMISIONS_PER_SECOND,
} = require('../helpers/constants')
const { timeLatest } = require('../helpers/utils')

const STAKING = 1
const STAKING_END = 2

const AUDITOR_ROLE = ethers.utils.id('AUDITOR_ROLE')
const ORIGINATOR_ROLE = ethers.utils.id('ORIGINATOR_ROLE')
const EMISSION_MANAGER_ROLE = ethers.utils.id('EMISSION_MANAGER')

describe('OriginatorStaking. Test Upgrade contract', () => {
  let emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance

  it('Upgrade OriginatorStaking contract', async () => {
    const OriginatorStaking_v_1_0 = await ethers.getContractFactory('OriginatorStaking_v_1_0')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Originator Staking
    originatorStaking_v_1_0 = await upgrades.deployProxy(OriginatorStaking_v_1_0, [
      'OriginatorTest',
      'OT',
      18,
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking_v_1_0.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking_v_1_0.address, amount)
    await ethix.connect(auditor).approve(originatorStaking_v_1_0.address, amount)

    // Set up Originator Staking terms
    await originatorStaking_v_1_0.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )
    expect(await originatorStaking_v_1_0.stakingGoal()).to.equal(STAKING_GOAL_ETHIX)
    const expected_ethix = BigNumber.from(AUDITOR_SETUP_PERCENTAGE).add(ORIGINATOR_SETUP_PERCENTAGE).mul(STAKING_GOAL_ETHIX).div(10000)
    expect(await ethix.balanceOf(originatorStaking_v_1_0.address)).to.equal(expected_ethix)

    // Set rewards emmision
    const totalStaked = await originatorStaking_v_1_0.totalSupply()
    const config = {
      emissionPerSecond: EMMISIONS_PER_SECOND,
      totalStaked: totalStaked.toString(),
      underlyingAsset: originatorStaking_v_1_0.address,
    }
    await originatorStaking_v_1_0.configureAssets([config])
    const asset = await originatorStaking_v_1_0.assets(originatorStaking_v_1_0.address)
    expect(asset.emissionPerSecond).to.equal(EMMISIONS_PER_SECOND)
    expect(totalStaked).to.equal(0)

    // Declare Stake End
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStaking_v_1_0.connect(governance).declareStakingEnd()
    expect(await originatorStaking_v_1_0.state()).to.be.equal(STAKING_END)

    // Deploy Originator Staking
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    originatorStaking = await upgrades.upgradeProxy(originatorStaking_v_1_0.address, OriginatorStaking)

    await originatorStaking.connect(governance).renewTerms(
      AUDITOR_SETUP_PERCENTAGE,
      NEW_ORIGINATOR_SETUP_PERCENTAGE,
      NEW_STAKING_GOAL_ETHIX,
      NEW_DISTRIBUTION_DURATION,
      DEFAULT_DELAY
    )

    // Compare
    expect(await originatorStaking.STAKED_TOKEN()).to.equal(ethix.address)
    expect(await originatorStaking.REWARDS_VAULT()).to.equal(rewardsVault.address)
    expect(await originatorStaking.decimals()).to.equal(18)
    expect(await originatorStaking.defaultedAmount()).to.equal(0)
    expect(await originatorStaking.name()).to.equal('OriginatorTest')
    expect(await originatorStaking.symbol()).to.equal('OT')
    expect(await originatorStaking.stakingGoal()).to.equal(NEW_STAKING_GOAL_ETHIX)
    expect(await originatorStaking.totalSupply()).to.equal(0)
    expect(await originatorStaking.getRoleMember(ORIGINATOR_ROLE, 0)).to.equal(originator.address)
    expect(await originatorStaking.getRoleMember(AUDITOR_ROLE, 0)).to.equal(auditor.address)
    expect(await originatorStaking.state()).to.equal(STAKING)
    expect(await originatorStaking.getRoleMember(EMISSION_MANAGER_ROLE, 0)).to.equal(emissionManager.address)
    const new_asset = await originatorStaking.assets(originatorStaking.address)
    expect(new_asset.emissionPerSecond).to.equal(asset.emissionPerSecond)
    expect(new_asset.lastUpdateTimestamp).to.equal(asset.lastUpdateTimestamp)
    expect(new_asset.index).to.equal(asset.index)
    const now = await timeLatest()
    expect(await originatorStaking.DEFAULT_DATE()).to.equal(BigNumber.from(DEFAULT_DELAY).add(NEW_DISTRIBUTION_DURATION).add(now))
    expect(await originatorStaking.DISTRIBUTION_END()).to.equal(BigNumber.from(NEW_DISTRIBUTION_DURATION).add(now))

    // Declare Stake End after upgrade and before DISTRIBUTION_END
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStaking.connect(governance).declareStakingEnd()
    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
  })
})