/*
    Originator Staking renewTerms test.

    Copyright (C) 2022 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber } = require('ethers')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  NEW_AUDITOR_SETUP_PERCENTAGE,
  NEW_ORIGINATOR_SETUP_PERCENTAGE,
  NEW_STAKING_GOAL_ETHIX,
  NEW_DISTRIBUTION_DURATION,
  EMMISIONS_PER_SECOND,
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { evmRevert, evmSnapshot, timeLatest } = require('../helpers/utils')

const STAKING = 1
const STAKING_END = 2

const AUDITOR_ROLE = ethers.utils.id('AUDITOR_ROLE')
const ORIGINATOR_ROLE = ethers.utils.id('ORIGINATOR_ROLE')

describe('OriginatorStaking. Renew terms in STAKING_END state', () => {
  let emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )

    const totalStaked = await originatorStaking.totalSupply()
    const config = {
      emissionPerSecond: EMMISIONS_PER_SECOND,
      totalStaked: totalStaked.toString(),
      underlyingAsset: originatorStaking.address,
    }
    await originatorStaking.configureAssets([config])

    await ethix.connect(staker1).approve(originatorStaking.address, amount)
    await originatorStaking.connect(staker1).stake(staker1.address, amount)
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStaking.connect(governance).declareStakingEnd()
  })

  it('Staking end state', async () => {
    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
  })

  it('Staker1 claims total of the rewards', async () => {
    const ethixStaker1BalanceBefore = await ethix.balanceOf(staker1.address)
    const staker1RewardsBalanceBefore = await originatorStaking.getTotalRewardsBalance(staker1.address)

    await originatorStaking.connect(staker1).claimRewards(staker1.address, staker1RewardsBalanceBefore)

    const ethixStaker1BalanceAfter = await ethix.balanceOf(staker1.address)
    const staker1RewardsBalanceAfter = await originatorStaking.getTotalRewardsBalance(staker1.address)
    expect(ethixStaker1BalanceAfter).to.be.equal(ethixStaker1BalanceBefore.add(staker1RewardsBalanceBefore))
    expect(staker1RewardsBalanceAfter).to.be.equal(0)
  })

  it('Staker1 redeems half of staked ethix', async () => {
    const ethixStaker1BalanceBefore = await ethix.balanceOf(staker1.address)
    const originatorStakingStaker1BalanceBefore = await originatorStaking.balanceOf(staker1.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await originatorStaking.connect(staker1).redeem(staker1.address, originatorStakingStaker1BalanceBefore.div(2))

    const ethixStaker1BalanceAfter = await ethix.balanceOf(staker1.address)
    const originatorStakingStaker1BalanceAfter = await originatorStaking.balanceOf(staker1.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixStaker1BalanceAfter).to.be.equal(ethixStaker1BalanceBefore.add(originatorStakingStaker1BalanceBefore.div(2)))
    expect(originatorStakingStaker1BalanceAfter).to.be.equal(originatorStakingStaker1BalanceBefore.sub(originatorStakingStaker1BalanceBefore.div(2)))
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.sub(originatorStakingStaker1BalanceBefore.div(2)))
  })

  it('Auditor withdraws half of participation (10 ethix)', async () => {
    const ethixAuditorBalanceBefore = await ethix.balanceOf(auditor.address)
    const auditorDepositedAmount = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await originatorStaking.connect(auditor).withdrawProposerStake(auditorDepositedAmount.div(2))

    const ethixAuditorBalanceAfter = await ethix.balanceOf(auditor.address)
    const auditorDepositedAmountAfter = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixAuditorBalanceAfter).to.be.equal(ethixAuditorBalanceBefore.add(auditorDepositedAmount.div(2)))
    expect(auditorDepositedAmountAfter).to.be.equal(auditorDepositedAmount.sub(auditorDepositedAmount.div(2)))
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.sub(auditorDepositedAmount.div(2)))
  })

  it('Auditor tries to withdraw more amount than what is left (20 ethix) but gets the rest', async () => {
    const amount = ethers.utils.parseEther('20')
    const ethixAuditorBalanceBefore = await ethix.balanceOf(auditor.address)
    const auditorDepositedAmount = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await originatorStaking.connect(auditor).withdrawProposerStake(amount)

    const ethixAuditorBalanceAfter = await ethix.balanceOf(auditor.address)
    const auditorDepositedAmountAfter = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixAuditorBalanceAfter).to.be.equal(ethixAuditorBalanceBefore.add(auditorDepositedAmount))
    expect(auditorDepositedAmountAfter).to.be.equal(0)
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.sub(auditorDepositedAmount))
  })

  it('Can renew terms', async () => {
    const ethixAuditorBalanceBefore = await ethix.balanceOf(auditor.address)
    const auditorDepositedAmount = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const ethixOriginatorBalanceBefore = await ethix.balanceOf(originator.address)
    const originatorDepositedAmount = await originatorStaking.proposerBalances(ORIGINATOR_ROLE)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)

    await originatorStaking.connect(governance).renewTerms(
      NEW_AUDITOR_SETUP_PERCENTAGE,
      NEW_ORIGINATOR_SETUP_PERCENTAGE,
      NEW_STAKING_GOAL_ETHIX,
      NEW_DISTRIBUTION_DURATION,
      DEFAULT_DELAY
    )

    const ethixAuditorBalanceAfter = await ethix.balanceOf(auditor.address)
    const auditorPercentageAmount = BigNumber.from(NEW_STAKING_GOAL_ETHIX).mul(NEW_AUDITOR_SETUP_PERCENTAGE).div(10000)
    const auditorTransferredAmount = auditorPercentageAmount.sub(auditorDepositedAmount)
    const auditorDepositedAmountAfter = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    const ethixOriginatorBalanceAfter = await ethix.balanceOf(originator.address)
    const originatorPercentageAmount = BigNumber.from(NEW_STAKING_GOAL_ETHIX).mul(NEW_ORIGINATOR_SETUP_PERCENTAGE).div(10000)
    const originatorTransferredAmount = originatorPercentageAmount.sub(originatorDepositedAmount)
    const originatorDepositedAmountAfter = await originatorStaking.proposerBalances(ORIGINATOR_ROLE)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)

    expect(ethixAuditorBalanceAfter).to.be.equal(ethixAuditorBalanceBefore.sub(auditorTransferredAmount))
    expect(auditorDepositedAmountAfter).to.be.equal(auditorDepositedAmount.add(auditorTransferredAmount))
    expect(ethixOriginatorBalanceAfter).to.be.equal(ethixOriginatorBalanceBefore.sub(originatorTransferredAmount))
    expect(originatorDepositedAmountAfter).to.be.equal(originatorDepositedAmount.add(originatorTransferredAmount))
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.add(auditorTransferredAmount).add(originatorTransferredAmount))
    expect(await originatorStaking.state()).to.be.equal(STAKING)

    const currentTime = await timeLatest()
    const newDistributionEnd = currentTime.add(NEW_DISTRIBUTION_DURATION)
    const newDefaultDate = currentTime.add(NEW_DISTRIBUTION_DURATION).add(DEFAULT_DELAY)
    expect(await originatorStaking.DISTRIBUTION_END()).to.be.equal(newDistributionEnd)
    const newDistribEnd = await originatorStaking.DISTRIBUTION_END()
    expect(await originatorStaking.DEFAULT_DATE()).to.be.equal(newDefaultDate)
  })

  it('Starker1 restarts earning rewards', async () => {
    const staker1RewardsBalanceBefore = await originatorStaking.getTotalRewardsBalance(staker1.address)

    await ethers.provider.send('evm_increaseTime', [10])
    await ethers.provider.send('evm_mine')

    const assets = await originatorStaking.assets(originatorStaking.address)
    const emissionPerSecond = assets[0]
    const staker1RewardsBalanceAfter = await originatorStaking.getTotalRewardsBalance(staker1.address)
    const expectedRewards = BigNumber.from(emissionPerSecond.mul(10))
    console.log('expected rewards for staker1--->', expectedRewards.toString())

    expect(staker1RewardsBalanceAfter).to.be.gt(staker1RewardsBalanceBefore)
  })

  it('Staker2 can stake to reach goal and check stakers balances and rewards', async () => {
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(staker2).approve(originatorStaking.address, amount)

    const staker2RewardsBalanceBefore = await originatorStaking.getTotalRewardsBalance(staker2.address)
    const ethixStaker2BalanceBefore = await ethix.balanceOf(staker2.address)
    const originatorStakingStaker1BalanceBefore = await originatorStaking.balanceOf(staker1.address)
    const originatorStakingStaker2BalanceBefore = await originatorStaking.balanceOf(staker2.address)
    const originatorStakingBalanceBefore = await ethix.balanceOf(originatorStaking.address)
    const stakingGoal = await originatorStaking.stakingGoal()
    const amountLeftToReachGoal = stakingGoal.sub(originatorStakingBalanceBefore)
    expect(amountLeftToReachGoal).to.be.equal(0)
    await expect(originatorStaking.connect(staker2).stake(staker2.address, amount)).to.be.revertedWith(
      'GOAL_HAS_REACHED'
    )

    await ethers.provider.send('evm_increaseTime', [60])
    await ethers.provider.send('evm_mine')

    const ethixStaker2BalanceAfter = await ethix.balanceOf(staker2.address)
    const originatorStakingStaker1BalanceAfter = await originatorStaking.balanceOf(staker1.address)
    const originatorStakingStaker2BalanceAfter = await originatorStaking.balanceOf(staker2.address)
    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)
    const staker2RewardsBalanceAfter = await originatorStaking.getTotalRewardsBalance(staker2.address)

    expect(staker2RewardsBalanceBefore).to.be.equal(0)
    expect(staker2RewardsBalanceAfter).to.be.equal(0)
    expect(staker2RewardsBalanceAfter).to.be.equal(staker2RewardsBalanceBefore)

    expect(await originatorStaking.hasReachedGoal()).to.be.true
    expect(ethixStaker2BalanceAfter).to.be.equal(ethixStaker2BalanceBefore.sub(amountLeftToReachGoal))
    expect(originatorStakingStaker2BalanceAfter).to.be.equal(originatorStakingStaker2BalanceBefore.add(amountLeftToReachGoal))
    expect(originatorStakingStaker1BalanceBefore).to.be.gt(0)
    expect(originatorStakingStaker1BalanceAfter).to.be.equal(originatorStakingStaker1BalanceBefore)
    expect(originatorStakingBalanceAfter).to.be.equal(originatorStakingBalanceBefore.add(amountLeftToReachGoal))
  })

  it('declare new staking end', async () => {
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStaking.connect(governance).declareStakingEnd()

    expect(await originatorStaking.state()).to.be.equal(STAKING_END)
  })

  it('all participants can redeem', async () => {
    const originatorStakingStaker1BalanceBefore = await originatorStaking.balanceOf(staker1.address)
    console.log('originatorStakingStaker1BalanceBefore', originatorStakingStaker1BalanceBefore.toString())
    await originatorStaking.connect(staker1).redeem(staker1.address, originatorStakingStaker1BalanceBefore)

    const auditorDepositedAmount = await originatorStaking.proposerBalances(AUDITOR_ROLE)
    console.log('auditorDepositedAmount', auditorDepositedAmount.toString())
    await originatorStaking.connect(auditor).withdrawProposerStake(auditorDepositedAmount)

    const originatorDepositedAmount = await originatorStaking.proposerBalances(ORIGINATOR_ROLE)
    console.log('originatorDepositedAmount', originatorDepositedAmount.toString())
    await originatorStaking.connect(originator).withdrawProposerStake(originatorDepositedAmount)

    const originatorStakingBalanceAfter = await ethix.balanceOf(originatorStaking.address)
    console.log('originatorStakingBalanceAfter', originatorStakingBalanceAfter.toString())
    expect(originatorStakingBalanceAfter).to.be.equal(0)

  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})

describe('OriginatorStaking. Renew terms and rewards', () => {
  let emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance

  before(async () => {
    setSnapshotId(await evmSnapshot())
    const OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    const EthixToken = await ethers.getContractFactory('EthixToken')
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    originatorStaking = await upgrades.deployProxy(OriginatorStaking, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStaking.address)
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(originator).approve(originatorStaking.address, amount)
    await ethix.connect(auditor).approve(originatorStaking.address, amount)
    await originatorStaking.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      STAKING_GOAL_ETHIX,
      DEFAULT_DELAY
    )

    const totalStaked = await originatorStaking.totalSupply()
    const config = {
      emissionPerSecond: EMMISIONS_PER_SECOND,
      totalStaked: totalStaked.toString(),
      underlyingAsset: originatorStaking.address,
    }
    await originatorStaking.configureAssets([config])

    await ethix.connect(staker1).approve(originatorStaking.address, amount)
    await originatorStaking.connect(staker1).stake(staker1.address, amount)
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStaking.connect(governance).declareStakingEnd()
    await originatorStaking.connect(governance).renewTerms(
      AUDITOR_SETUP_PERCENTAGE,
      NEW_ORIGINATOR_SETUP_PERCENTAGE,
      NEW_STAKING_GOAL_ETHIX,
      NEW_DISTRIBUTION_DURATION,
      DEFAULT_DELAY
    )
  })

  it('Starker1 continues earning rewards from previous staking period', async () => {
    const staker1RewardsBalanceBefore = await originatorStaking.getTotalRewardsBalance(staker1.address)

    await ethers.provider.send('evm_increaseTime', [1000])
    await ethers.provider.send('evm_mine')

    const assets = await originatorStaking.assets(originatorStaking.address)
    const emissionPerSecond = assets[0]
    const staker1RewardsBalanceAfter = await originatorStaking.getTotalRewardsBalance(staker1.address)
    const expectedRewards = BigNumber.from(emissionPerSecond.mul(1000).add(staker1RewardsBalanceBefore))
    console.log('expected rewards for staker1--->', expectedRewards.toString())

    expect(staker1RewardsBalanceBefore).to.be.gt(0)
    expect(staker1RewardsBalanceAfter).to.be.equal(expectedRewards)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
