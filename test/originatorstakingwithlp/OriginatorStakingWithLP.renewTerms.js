/*
    Originator Staking with LP renewTerms test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber, ContractFactory } = require('ethers')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  NEW_AUDITOR_SETUP_PERCENTAGE,
  NEW_ORIGINATOR_SETUP_PERCENTAGE,
  NEW_STAKING_GOAL_ETHIX,
  NEW_DISTRIBUTION_DURATION,
  EMMISIONS_PER_SECOND,
  POOL_FEES,
  DAI_ETHIX_RATIO,
  MIN_TICK,
  MAX_TICK
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { encodePriceSqrt } = require('../helpers/encodepricesqrt')
const { evmRevert, evmSnapshot, timeLatest, linkLibraries } = require('../helpers/utils')

const UNINITIALIZED = 0
const STAKING = 1
const STAKING_END = 2

const UNISWAP_V3_FACTORY = require('@uniswap/v3-core/artifacts/contracts/UniswapV3Factory.sol/UniswapV3Factory.json')
const NFT_DESCRIPTOR_LIBRARY = require('@uniswap/v3-periphery/artifacts/contracts/libraries/NFTDescriptor.sol/NFTDescriptor.json')
const NFT_DESCRIPTOR = require('@uniswap/v3-periphery/artifacts/contracts/NonfungibleTokenPositionDescriptor.sol/NonfungibleTokenPositionDescriptor.json')
const NFT_POSITION_MANAGER = require('@uniswap/v3-periphery/artifacts/contracts/NonfungiblePositionManager.sol/NonfungiblePositionManager.json')

describe('OriginatorStakingWithLP. Renew terms in STAKING_END state', () => {
  let emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)
  const newAuditorAmount = BigNumber.from(NEW_STAKING_GOAL_ETHIX).mul(NEW_AUDITOR_SETUP_PERCENTAGE).div(10000)
  const newOriginatorAmount = BigNumber.from(NEW_STAKING_GOAL_ETHIX).mul(NEW_ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())
    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
      newAmount0Desired = newAuditorAmount.add(newOriginatorAmount).div(2).div(DAI_ETHIX_RATIO)
      newAmount1Desired = newAuditorAmount.add(newOriginatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      newAmount0Desired = newAuditorAmount.add(newOriginatorAmount).div(2)
      newAmount1Desired = newAuditorAmount.add(newOriginatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // transfer ETHIX signers
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
        token0.address,
        token1.address,
        POOL_FEES,
        encodePrice
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    const now = await timeLatest()
    const mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: amount0Desired,
        amount1Desired: amount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    await originatorStakingWithLP.grantRole(ethers.utils.id('EMISSION_MANAGER'), deployer.address)
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStakingWithLP.address)

    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)

    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId,
    )

    const totalStaked = await originatorStakingWithLP.totalSupply()
    const config = {
      emissionPerSecond: EMMISIONS_PER_SECOND,
      totalStaked: totalStaked.toString(),
      underlyingAsset: originatorStakingWithLP.address,
    }
    await originatorStakingWithLP.configureAssets([config])

    const amount = ethers.utils.parseEther('100')
    await ethix.connect(staker1).approve(originatorStakingWithLP.address, amount)
    await originatorStakingWithLP.connect(staker1).stake(staker1.address, amount)
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStakingWithLP.connect(governance).declareStakingEnd()
  })

  it('Staking end state', async () => {
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING_END)
  })

  it('Staker1 claims total of the rewards', async () => {
    const ethixStaker1BalanceBefore = await ethix.balanceOf(staker1.address)
    const staker1RewardsBalanceBefore = await originatorStakingWithLP.getTotalRewardsBalance(staker1.address)

    await originatorStakingWithLP.connect(staker1).claimRewards(staker1.address, staker1RewardsBalanceBefore)

    const ethixStaker1BalanceAfter = await ethix.balanceOf(staker1.address)
    const staker1RewardsBalanceAfter = await originatorStakingWithLP.getTotalRewardsBalance(staker1.address)
    expect(ethixStaker1BalanceAfter).to.be.equal(ethixStaker1BalanceBefore.add(staker1RewardsBalanceBefore))
    expect(staker1RewardsBalanceAfter).to.be.equal(0)
  })

  it('Staker1 redeems half of staked ethix', async () => {
    const ethixStaker1BalanceBefore = await ethix.balanceOf(staker1.address)
    const originatorStakingWithLPStaker1BalanceBefore = await originatorStakingWithLP.balanceOf(staker1.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)

    await originatorStakingWithLP.connect(staker1).redeem(staker1.address, originatorStakingWithLPStaker1BalanceBefore.div(2))

    const ethixStaker1BalanceAfter = await ethix.balanceOf(staker1.address)
    const originatorStakingWithLPStaker1BalanceAfter = await originatorStakingWithLP.balanceOf(staker1.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(ethixStaker1BalanceAfter).to.be.equal(ethixStaker1BalanceBefore.add(originatorStakingWithLPStaker1BalanceBefore.div(2)))
    expect(originatorStakingWithLPStaker1BalanceAfter).to.be.equal(originatorStakingWithLPStaker1BalanceBefore.sub(originatorStakingWithLPStaker1BalanceBefore.div(2)))
    expect(originatorStakingWithLPBalanceAfter).to.be.equal(originatorStakingWithLPBalanceBefore.sub(originatorStakingWithLPStaker1BalanceBefore.div(2)))
  })

  it('Auditor could not withdraw the LP position', async () => {
    await expect(originatorStakingWithLP.connect(auditor).withdrawProposerStake(nftPositionId)).to.be.revertedWith('WITHDRAW_PERMISSION_DENIED')
  })

  it('Originator could not withdraw the LP position', async () => {
    await expect(originatorStakingWithLP.connect(auditor).withdrawProposerStake(nftPositionId)).to.be.revertedWith('WITHDRAW_PERMISSION_DENIED')
  })

  it('Could not renew terms with diferent LP', async () => {
    const now = await timeLatest()
    const mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: newAmount0Desired,
        amount1Desired: newAmount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    anotherPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)
    expect(await nftPositionManager.ownerOf(anotherPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, anotherPositionId)

    await expect(originatorStakingWithLP.connect(governance).renewTerms(
      AUDITOR_SETUP_PERCENTAGE,
      NEW_ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(NEW_STAKING_GOAL_ETHIX).sub(newAuditorAmount.add(newOriginatorAmount)),
      NEW_DISTRIBUTION_DURATION,
      DEFAULT_DELAY,
      anotherPositionId,
    )).to.be.revertedWith('INVALID_LP_TO_RENEW');
  })

  it('LP position owner withdraw the LP position', async () => {
    expect(await originatorStakingWithLP.liquidityPositionOwner()).to.be.equal(deployer.address)
    await originatorStakingWithLP.withdrawProposerStake(nftPositionId)
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
  })

  it('Could not renew terms if contract not longer have liquidity position', async () => {
    const now = await timeLatest()
    const increaseParams = {
        tokenId: nftPositionId,
        amount0Desired: newAmount0Desired,
        amount1Desired: newAmount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        deadline: now.add(5)
    }
    await nftPositionManager.increaseLiquidity(increaseParams)

    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)
    await expect(originatorStakingWithLP.connect(governance).renewTerms(
      AUDITOR_SETUP_PERCENTAGE,
      NEW_ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(NEW_STAKING_GOAL_ETHIX).sub(newAuditorAmount.add(newOriginatorAmount)),
      NEW_DISTRIBUTION_DURATION,
      DEFAULT_DELAY,
      nftPositionId,
    )).to.be.revertedWith('INVALID_LP_TO_RENEW');
  })

  it('Renew terms', async () => {
    // transfer NFT LP position to originator contract
    await nftPositionManager.transferFrom(deployer.address, originatorStakingWithLP.address, nftPositionId)
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(originatorStakingWithLP.address)

    const now = await timeLatest()
    const increaseParams = {
        tokenId: nftPositionId,
        amount0Desired: newAmount0Desired,
        amount1Desired: newAmount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        deadline: now.add(5)
    }
    await nftPositionManager.increaseLiquidity(increaseParams)

    await originatorStakingWithLP.connect(governance).renewTerms(
      AUDITOR_SETUP_PERCENTAGE,
      NEW_ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(NEW_STAKING_GOAL_ETHIX).sub(newAuditorAmount.add(newOriginatorAmount)),
      NEW_DISTRIBUTION_DURATION,
      DEFAULT_DELAY,
      nftPositionId,
    );

    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(originatorStakingWithLP.address)
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)

    const currentTime = await timeLatest()
    const newDistributionEnd = currentTime.add(NEW_DISTRIBUTION_DURATION)
    const newDefaultDate = currentTime.add(NEW_DISTRIBUTION_DURATION).add(DEFAULT_DELAY)
    expect(await originatorStakingWithLP.DISTRIBUTION_END()).to.be.equal(newDistributionEnd)
    expect(await originatorStakingWithLP.defaultDate()).to.be.equal(newDefaultDate)
  })

  it('Starker1 restarts earning rewards', async () => {
    const staker1RewardsBalanceBefore = await originatorStakingWithLP.getTotalRewardsBalance(staker1.address)

    await ethers.provider.send('evm_increaseTime', [10])
    await ethers.provider.send('evm_mine')

    const assets = await originatorStakingWithLP.assets(originatorStakingWithLP.address)
    const emissionPerSecond = assets[0]
    const staker1RewardsBalanceAfter = await originatorStakingWithLP.getTotalRewardsBalance(staker1.address)
    const expectedRewards = BigNumber.from(emissionPerSecond.mul(10))
    console.log('expected rewards for staker1--->', expectedRewards.toString())

    expect(staker1RewardsBalanceAfter).to.be.gt(staker1RewardsBalanceBefore)
  })

  it('Staker2 can stake to reach goal and check stakers balances and rewards', async () => {
    const amount = ethers.utils.parseEther('100')
    await ethix.connect(staker2).approve(originatorStakingWithLP.address, amount)

    const staker2RewardsBalanceBefore = await originatorStakingWithLP.getTotalRewardsBalance(staker2.address)
    const ethixStaker2BalanceBefore = await ethix.balanceOf(staker2.address)
    const originatorStakingWithLPStaker1BalanceBefore = await originatorStakingWithLP.balanceOf(staker1.address)
    const originatorStakingWithLPStaker2BalanceBefore = await originatorStakingWithLP.balanceOf(staker2.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)
    const stakingGoal = await originatorStakingWithLP.stakingGoal()
    const amountLeftToReachGoal = stakingGoal.sub(originatorStakingWithLPBalanceBefore)
    expect(amountLeftToReachGoal).to.be.equal(0)
    await expect(originatorStakingWithLP.connect(staker2).stake(staker2.address, amount)).to.be.revertedWith(
      'GOAL_HAS_REACHED'
    )

    await ethers.provider.send('evm_increaseTime', [60])
    await ethers.provider.send('evm_mine')

    const ethixStaker2BalanceAfter = await ethix.balanceOf(staker2.address)
    const originatorStakingWithLPStaker1BalanceAfter = await originatorStakingWithLP.balanceOf(staker1.address)
    const originatorStakingWithLPStaker2BalanceAfter = await originatorStakingWithLP.balanceOf(staker2.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)
    const staker2RewardsBalanceAfter = await originatorStakingWithLP.getTotalRewardsBalance(staker2.address)

    expect(staker2RewardsBalanceBefore).to.be.equal(0)
    expect(staker2RewardsBalanceAfter).to.be.equal(0)
    expect(staker2RewardsBalanceAfter).to.be.equal(staker2RewardsBalanceBefore)

    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.true
    expect(ethixStaker2BalanceAfter).to.be.equal(ethixStaker2BalanceBefore.sub(amountLeftToReachGoal))
    expect(originatorStakingWithLPStaker2BalanceAfter).to.be.equal(originatorStakingWithLPStaker2BalanceBefore.add(amountLeftToReachGoal))
    expect(originatorStakingWithLPStaker1BalanceBefore).to.be.gt(0)
    expect(originatorStakingWithLPStaker1BalanceAfter).to.be.equal(originatorStakingWithLPStaker1BalanceBefore)
    expect(originatorStakingWithLPBalanceAfter).to.be.equal(originatorStakingWithLPBalanceBefore.add(amountLeftToReachGoal))
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})

describe('OriginatorStakingWithLP. Renew terms and rewards', () => {
  let emissionManager, staker1, originator, auditor, ethix, rewardsVault, governance

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)
  const newAuditorAmount = BigNumber.from(NEW_STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const newOriginatorAmount = BigNumber.from(NEW_STAKING_GOAL_ETHIX).mul(NEW_ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())
    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, staker1, staker2, originator, auditor, ethix, rewardsVault, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
      newAmount0Desired = newAuditorAmount.add(newOriginatorAmount).div(2).div(DAI_ETHIX_RATIO)
      newAmount1Desired = newAuditorAmount.add(newOriginatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      newAmount0Desired = newAuditorAmount.add(newOriginatorAmount).div(2)
      newAmount1Desired = newAuditorAmount.add(newOriginatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // transfer ETHIX signers
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
        token0.address,
        token1.address,
        POOL_FEES,
        encodePrice
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    now = await timeLatest()
    mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: amount0Desired,
        amount1Desired: amount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    await originatorStakingWithLP.grantRole(ethers.utils.id('EMISSION_MANAGER'), deployer.address)
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStakingWithLP.address)

    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)

    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId,
    )

    const totalStaked = await originatorStakingWithLP.totalSupply()
    const config = {
      emissionPerSecond: EMMISIONS_PER_SECOND,
      totalStaked: totalStaked.toString(),
      underlyingAsset: originatorStakingWithLP.address,
    }
    await originatorStakingWithLP.configureAssets([config])

    const amount = ethers.utils.parseEther('100')
    await ethix.connect(staker1).approve(originatorStakingWithLP.address, amount)
    await originatorStakingWithLP.connect(staker1).stake(staker1.address, amount)
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')

    await originatorStakingWithLP.connect(governance).declareStakingEnd()

    now = await timeLatest()
    increaseParams = {
        tokenId: nftPositionId,
        amount0Desired: newAmount0Desired,
        amount1Desired: newAmount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        deadline: now.add(5)
    }
    await nftPositionManager.increaseLiquidity(increaseParams)

    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(originatorStakingWithLP.address)
    await originatorStakingWithLP.connect(governance).renewTerms(
      AUDITOR_SETUP_PERCENTAGE,
      NEW_ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(NEW_STAKING_GOAL_ETHIX).sub(newAuditorAmount.add(newOriginatorAmount)),
      NEW_DISTRIBUTION_DURATION,
      DEFAULT_DELAY,
      nftPositionId,
    )

    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(originatorStakingWithLP.address)
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)

    const currentTime = await timeLatest()
    const newDistributionEnd = currentTime.add(NEW_DISTRIBUTION_DURATION)
    const newDefaultDate = currentTime.add(NEW_DISTRIBUTION_DURATION).add(DEFAULT_DELAY)
    expect(await originatorStakingWithLP.DISTRIBUTION_END()).to.be.equal(newDistributionEnd)
    expect(await originatorStakingWithLP.defaultDate()).to.be.equal(newDefaultDate)
  })

  it('Starker1 continues earning rewards from previous staking period', async () => {
    const staker1RewardsBalanceBefore = await originatorStakingWithLP.getTotalRewardsBalance(staker1.address)

    await ethers.provider.send('evm_increaseTime', [1000])
    await ethers.provider.send('evm_mine')

    const assets = await originatorStakingWithLP.assets(originatorStakingWithLP.address)
    const emissionPerSecond = assets[0]
    const staker1RewardsBalanceAfter = await originatorStakingWithLP.getTotalRewardsBalance(staker1.address)
    const expectedRewards = BigNumber.from(emissionPerSecond.mul(1000).add(staker1RewardsBalanceBefore))
    console.log('expected rewards for staker1--->', expectedRewards.toString())

    expect(staker1RewardsBalanceBefore).to.be.gt(0)
    expect(staker1RewardsBalanceAfter).to.be.equal(expectedRewards)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
