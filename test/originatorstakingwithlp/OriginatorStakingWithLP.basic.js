/*
    Originator Staking with LP basic test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber, ContractFactory } = require('ethers')

const {
  MAX_UINT_AMOUNT,
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  DAI_ETHIX_RATIO,
  POOL_FEES,
  MIN_TICK,
  MAX_TICK
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { compareRewardsAtAction } = require('../helpers/reward')
const { encodePriceSqrt } = require('../helpers/encodepricesqrt')
const { getUserIndex, getRewards, evmRevert, evmSnapshot, timeLatest, linkLibraries } = require('../helpers/utils')

const UNINITIALIZED = 0
const STAKING = 1
const STAKING_END = 2
const DEFAULT = 3

const AUDITOR_ROLE = ethers.utils.id('AUDITOR_ROLE')
const ORIGINATOR_ROLE = ethers.utils.id('ORIGINATOR_ROLE')
const EMISSION_MANAGER_ROLE = ethers.utils.id('EMISSION_MANAGER')
const GOVERNANCE_ROLE = ethers.utils.id('GOVERNANCE_ROLE')

const UNISWAP_V3_FACTORY = require('@uniswap/v3-core/artifacts/contracts/UniswapV3Factory.sol/UniswapV3Factory.json')
const NFT_DESCRIPTOR_LIBRARY = require('@uniswap/v3-periphery/artifacts/contracts/libraries/NFTDescriptor.sol/NFTDescriptor.json')
const NFT_DESCRIPTOR = require('@uniswap/v3-periphery/artifacts/contracts/NonfungibleTokenPositionDescriptor.sol/NonfungibleTokenPositionDescriptor.json')
const NFT_POSITION_MANAGER = require('@uniswap/v3-periphery/artifacts/contracts/NonfungiblePositionManager.sol/NonfungiblePositionManager.json')

describe('OriginatorStakingWithLP. Basics', () => {
  let deployer, emissionManager, staker, originator, auditor, ethix, stableCoin, addr2, addr3, rewardsVault, governance, nftPositionManager, originatorStakingWithLP

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())
    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, staker, originator, auditor, addr2, addr3, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // transfer ETHIX signers
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
        token0.address,
        token1.address,
        POOL_FEES,
        encodePrice
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    const now = await timeLatest()
    const mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: amount0Desired,
        amount1Desired: amount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    await originatorStakingWithLP.grantRole(ethers.utils.id('EMISSION_MANAGER'), deployer.address)
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStakingWithLP.address)
  })

  it('Initial configuration after initialize() is correct', async () => {
    expect(await originatorStakingWithLP.name()).to.equal('OriginatorTest')
    expect(await originatorStakingWithLP.symbol()).to.equal('OT')
    expect(await originatorStakingWithLP.decimals()).to.equal(18)
    expect(await originatorStakingWithLP.TOKEN_TO_STAKE()).to.equal(ethix.address)
    expect(await originatorStakingWithLP.REWARDS_VAULT()).to.equal(rewardsVault.address)
    expect(await originatorStakingWithLP.hasRole(EMISSION_MANAGER_ROLE, emissionManager.address)).to.be.true
    expect(await originatorStakingWithLP.hasRole(EMISSION_MANAGER_ROLE, deployer.address)).to.be.true
  })

  it('Not set staking goal yet', async () => {
    await expect(originatorStakingWithLP.hasReachedGoal())
      .to.be.revertedWith('INVALID_ZERO_AMOUNT')
  })

  it('Reverts trying to stake on diferent state of STAKING', async () => {
    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await expect(originatorStakingWithLP.connect(staker).stake(staker.address, ethers.utils.parseEther('50'))).to.be.revertedWith(
      'ONLY_ON_STAKING_STATE'
    )
  })

  it('Could not set up terms when min tick of the position is higher than wide range', async () => {
    const now = await timeLatest()
    const mintParams = {
      token0: token0.address,
      token1: token1.address,
      fee: POOL_FEES,
      tickLower: 0,
      tickUpper: MAX_TICK,
      amount0Desired: amount0Desired,
      amount1Desired: amount1Desired,
      amount0Min: 0,
      amount1Min: 0,
      recipient: deployer.address,
      deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    const nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 1)
    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await expect(originatorStakingWithLP.connect(emissionManager).setUpTerms(
        auditor.address,
        originator.address,
        governance.address,
        AUDITOR_SETUP_PERCENTAGE,
        ORIGINATOR_SETUP_PERCENTAGE,
        BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
        DEFAULT_DELAY,
        nftPositionId))
    .to.be.revertedWith('MIN_TICK_MUCH_BE_WHOLE_RANGE_OF_POOL')
  })
  it('Could not set up terms when max tick of the position is lower than wide range', async () => {
    const now = await timeLatest()
    const mintParams = {
      token0: token0.address,
      token1: token1.address,
      fee: POOL_FEES,
      tickLower: MIN_TICK,
      tickUpper: 0,
      amount0Desired: amount0Desired,
      amount1Desired: amount1Desired,
      amount0Min: 0,
      amount1Min: 0,
      recipient: deployer.address,
      deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    const nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 2)
    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await expect(originatorStakingWithLP.connect(emissionManager).setUpTerms(
        auditor.address,
        originator.address,
        governance.address,
        AUDITOR_SETUP_PERCENTAGE,
        ORIGINATOR_SETUP_PERCENTAGE,
        BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
        DEFAULT_DELAY,
        nftPositionId))
      .to.be.revertedWith('MAX_TICK_MUCH_BE_WHOLE_RANGE_OF_POOL')
  })
  it('Could not set up terms when position amount is insufficient', async () => {
    const now = await timeLatest()
    const mintParams = {
      token0: token0.address,
      token1: token1.address,
      fee: POOL_FEES,
      tickLower: MIN_TICK,
      tickUpper: MAX_TICK,
      amount0Desired: amount0Desired.div(10),
      amount1Desired: amount1Desired.div(10),
      amount0Min: 0,
      amount1Min: 0,
      recipient: deployer.address,
      deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    const nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 3)
    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await expect(originatorStakingWithLP.connect(emissionManager).setUpTerms(
        auditor.address,
        originator.address,
        governance.address,
        AUDITOR_SETUP_PERCENTAGE,
        ORIGINATOR_SETUP_PERCENTAGE,
        BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
        DEFAULT_DELAY,
        nftPositionId))
      .to.be.revertedWith('POSITION_AMOUNT_IS_INSUFFICIENT')
  })
  it('can setup terms', async () => {
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)

    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId,
    )

    expect(await originatorStakingWithLP.hasRole(AUDITOR_ROLE, auditor.address)).to.be.true
    expect(await originatorStakingWithLP.hasRole(ORIGINATOR_ROLE, originator.address)).to.be.true
    expect(await originatorStakingWithLP.hasRole(GOVERNANCE_ROLE, governance.address)).to.be.true
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(originatorStakingWithLP.address)
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)
  })

  it('Validates change distribution period', async () => {
    const currentTime = await timeLatest()
    const firstDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    expect(firstDistributionEnd.toNumber()).to.be.within(currentTime.add(DISTRIBUTION_DURATION - 15).toNumber(), currentTime.add(DISTRIBUTION_DURATION + 15).toNumber()) // [1 day - 10 seconds, 1 day + 10 seconds]

    const distributionPeriodSeconds = 31536000
    await originatorStakingWithLP.connect(emissionManager).changeDistributionEndDate(firstDistributionEnd.add(distributionPeriodSeconds))
    const nextDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    expect(nextDistributionEnd.toNumber()).to.be.within(firstDistributionEnd.add(31536000).toNumber(), firstDistributionEnd.add(31536002).toNumber()) // [1 year - 2 seconds, 1 year + 2 seconds]
  })

  it('Not reached goal', async () => {
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.false
  })

  it('only emission manager can set up terms', async () => {
    await expect(originatorStakingWithLP.connect(auditor).setUpTerms(
        auditor.address,
        originator.address,
        governance.address,
        AUDITOR_SETUP_PERCENTAGE,
        ORIGINATOR_SETUP_PERCENTAGE,
        BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
        DEFAULT_DELAY,
        nftPositionId))
      .to.be.revertedWith('ONLY_EMISSION_MANAGER')
  })

  it('can not set up terms with same addresses', async () => {
    await expect(originatorStakingWithLP.connect(emissionManager).setUpTerms(
        auditor.address,
        auditor.address,
        governance.address,
        AUDITOR_SETUP_PERCENTAGE,
        ORIGINATOR_SETUP_PERCENTAGE,
        BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
        DEFAULT_DELAY,
        nftPositionId))
      .to.be.revertedWith('PROPOSERS_CANNOT_BE_THE_SAME')
  })

  it('can not set up terms with 0 referenceGoal', async () => {
    await expect(originatorStakingWithLP.connect(emissionManager).setUpTerms(auditor.address, originator.address, governance.address,
      AUDITOR_SETUP_PERCENTAGE, ORIGINATOR_SETUP_PERCENTAGE, 0, DEFAULT_DELAY, nftPositionId))
      .to.be.revertedWith('INVALID_ZERO_AMOUNT')
  })

  it('Reverts trying to stake 0 amount', async () => {
    await expect(originatorStakingWithLP.connect(emissionManager).stake(staker.address, 0)).to.be.revertedWith(
      'INVALID_ZERO_AMOUNT'
    )
  })

  it('User 1 stakes 30 ETHIX: receives 30 stkETHIX, StakedEthix balance of ETHIX is 30 and his rewards to claim are 0', async () => {
    const amount = ethers.utils.parseEther('30')

    const saveBalanceBefore = await originatorStakingWithLP.balanceOf(staker.address)
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)

    // Prepare actions for the test case
    const actions = () => [
      ethix.connect(staker).approve(originatorStakingWithLP.address, amount),
      originatorStakingWithLP.connect(staker).stake(staker.address, amount),
    ]

    // Check rewards
    await compareRewardsAtAction(originatorStakingWithLP, staker.address, actions)

    // Stake token tests
    expect((await originatorStakingWithLP.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount.toString()).toString()
    )
    expect((await ethix.balanceOf(originatorStakingWithLP.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount.toString()).toString()
    )
    expect((await originatorStakingWithLP.balanceOf(staker.address)).toString()).to.be.equal(amount)
    expect((await ethix.balanceOf(originatorStakingWithLP.address)).toString()).to.be.equal(amount)
  })

  it('User 1 stakes 5 ETHIX more: his total stkETHIX balance increases, stkETHIX balance of Ethix increases and his reward until now get accumulated', async () => {
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.false
    const amount = ethers.utils.parseEther('5')

    const saveBalanceBefore = await originatorStakingWithLP.balanceOf(staker.address)
    const actions = () => [
      ethix.connect(staker).approve(originatorStakingWithLP.address, amount),
      originatorStakingWithLP.connect(staker).stake(staker.address, amount),
    ]

    // Checks rewards
    await compareRewardsAtAction(originatorStakingWithLP, staker.address, actions, true)

    // Extra test checks
    expect((await originatorStakingWithLP.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount)
    )
    expect((await ethix.balanceOf(originatorStakingWithLP.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amount)
    )
  })

  it('User 2 stakes 1 ETHIX, with the rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('1')

    // Disable rewards via config
    const assetsConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // Checks rewards
    const actions = () => [
      ethix.connect(addr2).approve(originatorStakingWithLP.address, amount),
      originatorStakingWithLP.connect(addr2).stake(addr2.address, amount),
    ]

    await compareRewardsAtAction(originatorStakingWithLP, addr2.address, actions, false, assetsConfig)

    // Check expected stake balance for second staker
    expect(await originatorStakingWithLP.balanceOf(addr2.address)).to.equal(amount)

    // Expect rewards balance to still be zero
    const rewardsBalance = await originatorStakingWithLP.getTotalRewardsBalance(addr2.address)
    expect(rewardsBalance).to.be.equal('0')
  })

  it('User 3 stakes 2 ETHIX more, with the rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('2')

    // Keep rewards disabled via config
    const assetsConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    // Checks rewards
    const actions = () => [
      ethix.connect(addr3).approve(originatorStakingWithLP.address, amount),
      originatorStakingWithLP.connect(addr3).stake(addr3.address, amount),
    ]

    await compareRewardsAtAction(originatorStakingWithLP, addr3.address, actions, false, assetsConfig)

    // Expect rewards balance to still be zero
    const rewardsBalance = await originatorStakingWithLP.getTotalRewardsBalance(addr3.address)
    expect(rewardsBalance).to.be.equal('0')
  })

  it('User 1 stakes 10 ETHIX more, but only stake 2 to cap the goal: his total stkETHIX balance increases, stkETHIX balance of Ethix increases and his reward until now get accumulated', async () => {
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.false
    const amount = ethers.utils.parseEther('10')
    const user2StakedAmount = ethers.utils.parseEther('2')
    const user3StakedAmount = ethers.utils.parseEther('1')

    const saveBalanceBefore = await originatorStakingWithLP.balanceOf(staker.address)
    const actions = () => [
      ethix.connect(staker).approve(originatorStakingWithLP.address, amount),
      originatorStakingWithLP.connect(staker).stake(staker.address, amount),
    ]
    const stakingGoal = await originatorStakingWithLP.stakingGoal()
    const balanceStaked = await ethix.balanceOf(originatorStakingWithLP.address)
    const amountStaked = stakingGoal.sub(balanceStaked)

    // Checks rewards
    await compareRewardsAtAction(originatorStakingWithLP, staker.address, actions, true)

    // Extra test checks
    expect((await originatorStakingWithLP.balanceOf(staker.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amountStaked)
    )
    expect((await ethix.balanceOf(originatorStakingWithLP.address)).toString()).to.be.equal(
      saveBalanceBefore.add(amountStaked).add(user2StakedAmount).add(user3StakedAmount)
    )
  })

  it('Reached goal', async () => {
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.true
  })

  it('Should not stake when goal reached', async() => {
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.true
    const amount = ethers.utils.parseEther('10')
    ethix.connect(staker).approve(originatorStakingWithLP.address, amount)
    await expect(originatorStakingWithLP.connect(staker).stake(staker.address, amount)).to.be.revertedWith('GOAL_HAS_REACHED')
  })

  it('Should not liquidate when state != DEFAULT', async() => {
    expect(await originatorStakingWithLP.state()).to.be.not.equal(DEFAULT)
    await expect(originatorStakingWithLP.connect(governance).liquidateProposerStake(nftPositionId)).to.be.revertedWith('ONLY_ON_DEFAULT')
  })

  it('Should not withdraw when state != STAKING_END', async() => {
    expect(await originatorStakingWithLP.state()).to.be.not.equal(STAKING_END)
    await expect(originatorStakingWithLP.connect(auditor).withdrawProposerStake(nftPositionId)).to.be.revertedWith('ONLY_ON_STAKING_END_STATE')
  })

  it('Only governance declare staking end', async () => {
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.true
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)
    await expect(originatorStakingWithLP.connect(emissionManager).declareStakingEnd())
      .to.be.revertedWith('ONLY_GOVERNANCE')
  })

  it('Should declare staking end', async () => {
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.true
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)
    await originatorStakingWithLP.connect(governance).declareStakingEnd()
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING_END)
  })

  it('Should not stake when state is stake end', async() => {
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING_END)
    const amount = ethers.utils.parseEther('10')
    ethix.connect(staker).approve(originatorStakingWithLP.address, amount)
    await expect(originatorStakingWithLP.connect(staker).stake(staker.address, amount)).to.be.revertedWith('ONLY_ON_STAKING_STATE')
  })

  it('Should redeem when state is stake end', async() => {
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING_END)
    const amount = ethers.utils.parseEther('10')
    const beforeRedeemAmount = await ethix.balanceOf(staker.address)
    await originatorStakingWithLP.connect(staker).redeem(staker.address, amount)
    const afterRedeemAmount = await ethix.balanceOf(staker.address)
    expect(afterRedeemAmount).to.be.equal(beforeRedeemAmount.add(amount))
  })

  it('Should withdraw when state is stake end', async() => {
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING_END)
    expect(await originatorStakingWithLP.liquidityPositionOwner()).to.be.equal(deployer.address)
    await originatorStakingWithLP.connect(deployer).withdrawProposerStake(nftPositionId)
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
  })

  it('User 1 claim half rewards ', async () => {
    const distributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    // Increase time for bigger rewards
    ethers.provider.send('evm_increaseTime', [distributionEnd.toNumber()])
    ethers.provider.send('evm_mine')

    const halfRewards = (await originatorStakingWithLP.stakerRewardsToClaim(staker.address)).div(2)
    console.log('-->halfRewards', halfRewards.toString())
    const saveUserBalance = await ethix.balanceOf(staker.address)

    await originatorStakingWithLP.connect(staker).claimRewards(staker.address, halfRewards)

    const userBalanceAfterActions = await ethix.balanceOf(staker.address)
    expect(userBalanceAfterActions.eq(saveUserBalance.add(halfRewards))).to.be.ok
  })

  it('User 1 tries to claim higher reward than current rewards balance', async () => {
    const saveUserBalance = await ethix.balanceOf(staker.address)

    // Try to claim more amount than accumulated
    await expect(
      originatorStakingWithLP.connect(staker).claimRewards(staker.address, ethers.utils.parseEther('10000'))
    ).to.be.revertedWith('INVALID_AMOUNT')

    const userBalanceAfterActions = await ethix.balanceOf(staker.address)
    expect(userBalanceAfterActions.eq(saveUserBalance)).to.be.ok
  })

  it('User 1 claim all rewards', async () => {
    const userAddress = staker.address
    const underlyingAsset = originatorStakingWithLP.address

    const userBalance = await originatorStakingWithLP.balanceOf(userAddress)
    const userEthixBalance = await ethix.balanceOf(userAddress)

    const userRewards = await originatorStakingWithLP.stakerRewardsToClaim(userAddress)

    // Get index before actions
    const userIndexBefore = await getUserIndex(originatorStakingWithLP, userAddress, underlyingAsset)

    // Claim rewards
    await originatorStakingWithLP.connect(staker).claimRewards(staker.address, MAX_UINT_AMOUNT)

    // Get index after actions
    const userIndexAfter = await getUserIndex(originatorStakingWithLP, userAddress, underlyingAsset)

    const expectedAccruedRewards = getRewards(userBalance, userIndexAfter, userIndexBefore)
    const userEthixBalanceAfterAction = (await ethix.balanceOf(userAddress)).toString()

    expect(userEthixBalanceAfterAction).to.be.equal(
      userEthixBalance.add(userRewards).add(expectedAccruedRewards).toString()
    )
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
