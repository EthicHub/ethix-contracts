/*
    Originator Staking with LP STAKING_END and DEFAULT states test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber, ContractFactory } = require('ethers')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  DEFAULT_DEBT_AMOUNT,
  POOL_FEES,
  DAI_ETHIX_RATIO,
  MIN_TICK,
  MAX_TICK
} = require('../helpers/constants')
const { snapshotId, setSnapshotId } = require('../helpers/make-suite')
const { evmRevert, evmSnapshot, linkLibraries, timeLatest } = require('../helpers/utils')
const { encodePriceSqrt } = require('../helpers/encodepricesqrt')

const UNINITIALIZED = 0
const STAKING = 1
const STAKING_END = 2
const DEFAULT = 3

const AUDITOR_ROLE = ethers.utils.id('AUDITOR_ROLE')
const ORIGINATOR_ROLE = ethers.utils.id('ORIGINATOR_ROLE')
const GOVERNANCE_ROLE = ethers.utils.id('GOVERNANCE_ROLE')

const UNISWAP_V3_FACTORY = require('@uniswap/v3-core/artifacts/contracts/UniswapV3Factory.sol/UniswapV3Factory.json')
const NFT_DESCRIPTOR_LIBRARY = require('@uniswap/v3-periphery/artifacts/contracts/libraries/NFTDescriptor.sol/NFTDescriptor.json')
const NFT_DESCRIPTOR = require('@uniswap/v3-periphery/artifacts/contracts/NonfungibleTokenPositionDescriptor.sol/NonfungibleTokenPositionDescriptor.json')
const NFT_POSITION_MANAGER = require('@uniswap/v3-periphery/artifacts/contracts/NonfungiblePositionManager.sol/NonfungiblePositionManager.json')

describe('OriginatorStakingWithLP. DEFAULT state', () => {
  let deployer, emissionManager, staker, originator, auditor, ethix, rewardsVault, governance

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())

    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, staker, originator, auditor, ethix, rewardsVault, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // transfer ETHIX signers
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
        token0.address,
        token1.address,
        POOL_FEES,
        encodePrice
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    const now = await timeLatest()
    const mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: amount0Desired,
        amount1Desired: amount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)
    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId
    )
  })

  it('can not declare as DEFAULT because DEFAULT_DATE is not valid', async () => {
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)

    await expect(originatorStakingWithLP.connect(governance).declareDefault(DEFAULT_DEBT_AMOUNT))
    .to.be.revertedWith('DEFAULT_DATE_NOT_REACHED')
  })

  it('staker stakes 20 ETHIX', async () => {
    const amount = ethers.utils.parseEther('20')
    const stakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)

    await ethix.connect(staker).approve(originatorStakingWithLP.address, amount)
    await originatorStakingWithLP.connect(staker).stake(staker.address, amount)

    const stakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(stakerBalanceAfter.toString()).to.be.equal(stakerBalanceBefore.sub(amount.toString()))
    expect(originatorStakingWithLPBalanceAfter.toString()).to.be.equal(originatorStakingWithLPBalanceBefore.add(amount.toString()))
  })

  it('only governance role can declare as DEFAULT', async () => {
    await ethers.provider.send('evm_increaseTime', [259200]) // 3 days
    await ethers.provider.send('evm_mine')

    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)

    await expect(originatorStakingWithLP.connect(emissionManager).declareDefault(DEFAULT_DEBT_AMOUNT))
    .to.be.revertedWith('ONLY_GOVERNANCE')
  })

  it('can not liquidate because state is not default', async () => {
    expect(await originatorStakingWithLP.state()).to.not.be.equal(DEFAULT)
    await expect(originatorStakingWithLP.connect(governance).liquidateProposerStake(
      nftPositionId)).to.be.revertedWith('ONLY_ON_DEFAULT')
  })

  it('can declare contract as DEFAULT, DISTRIBUTION_END and DEFAULT_DATE already finished', async () => {
    const distributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    const defaultDate = await originatorStakingWithLP.defaultDate()
    const calculatedDistributionEnd = defaultDate.sub(DEFAULT_DELAY)
    expect(distributionEnd.toNumber()).to.be.equal(calculatedDistributionEnd.toNumber())
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)

    await originatorStakingWithLP.connect(governance).declareDefault(DEFAULT_DEBT_AMOUNT)

    const newDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    expect(await originatorStakingWithLP.defaultedAmount()).to.be.equal(DEFAULT_DEBT_AMOUNT)
    expect(newDistributionEnd).to.be.equal(distributionEnd)
    expect(await originatorStakingWithLP.state()).to.be.equal(DEFAULT)
  })

  it('staker can redeem in DEFAULT state because defaulted amount is less than proposer deposit amount', async () => {
    const ethixStakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPStakerBalanceBefore = await originatorStakingWithLP.balanceOf(staker.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)

    await originatorStakingWithLP.connect(staker).redeem(staker.address, ethixStakerBalanceBefore)

    const ethixStakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPStakerBalanceAfter = await originatorStakingWithLP.balanceOf(staker.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(ethixStakerBalanceAfter.sub(originatorStakingWithLPStakerBalanceBefore)).to.be.equal(ethixStakerBalanceBefore)
    expect(originatorStakingWithLPStakerBalanceAfter).to.be.equal(0)
    expect(originatorStakingWithLPBalanceAfter).to.be.equal(originatorStakingWithLPBalanceBefore.sub(originatorStakingWithLPStakerBalanceBefore))
  })

  it('governance can liquidate proposer stake', async () => {
    expect(await originatorStakingWithLP.liquidityPositionOwner()).to.be.equal(deployer.address)
    await originatorStakingWithLP.connect(governance).liquidateProposerStake(nftPositionId)
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(governance.address)
  })

  it('could not liquidate if liquidity position not exists', async () => {
    await expect(originatorStakingWithLP.connect(governance).liquidateProposerStake(
      nftPositionId)).to.be.revertedWith('INVALID_LP_TO_LIQUIDATE')
  })

  it('only governance can liquidate', async () => {
    await expect(originatorStakingWithLP.connect(emissionManager).liquidateProposerStake(
      nftPositionId)).to.be.revertedWith('ONLY_GOVERNANCE')
  })

  it('can not declare STAKING_END in DEFAULT state', async () => {
    await expect(originatorStakingWithLP.connect(governance).declareStakingEnd()).to.be.revertedWith(
      'ONLY_ON_STAKING_STATE'
    )
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
describe('OriginatorStakingWithLP. DEFAULT state with stakingGoal as debt amount', () => {
  let deployer, emissionManager, staker, originator, auditor, ethix, rewardsVault, governance

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())

    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, staker, originator, auditor, ethix, rewardsVault, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // transfer ETHIX signers
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
        token0.address,
        token1.address,
        POOL_FEES,
        encodePrice
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    const now = await timeLatest()
    const mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: amount0Desired,
        amount1Desired: amount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)
    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId
    )
  })

  it('staker stakes 20 ETHIX', async () => {
    const amount = ethers.utils.parseEther('20')
    const stakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)

    await ethix.connect(staker).approve(originatorStakingWithLP.address, amount)
    await originatorStakingWithLP.connect(staker).stake(staker.address, amount)

    const stakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(stakerBalanceAfter.toString()).to.be.equal(stakerBalanceBefore.sub(amount.toString()))
    expect(originatorStakingWithLPBalanceAfter.toString()).to.be.equal(originatorStakingWithLPBalanceBefore.add(amount.toString()))
  })

  it('can declare contract as DEFAULT, DISTRIBUTION_END and DEFAULT_DATE already finished', async () => {
    const distributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    const defaultDate = await originatorStakingWithLP.defaultDate()
    const calculatedDistributionEnd = defaultDate.sub(DEFAULT_DELAY)
    expect(distributionEnd.toNumber()).to.be.equal(calculatedDistributionEnd.toNumber())
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)

    await ethers.provider.send('evm_increaseTime', [259200]) // 3 days
    await ethers.provider.send('evm_mine')

    const proposersBalance = auditorAmount.add(originatorAmount)
    const defaultedAmountBefore = await originatorStakingWithLP.defaultedAmount()
    await originatorStakingWithLP.connect(governance).declareDefault(proposersBalance)

    const defaultedAmountAfter = await originatorStakingWithLP.defaultedAmount()
    expect(defaultedAmountBefore).to.be.equal(0)
    expect(defaultedAmountAfter).to.be.equal(proposersBalance)
    expect(await originatorStakingWithLP.state()).to.be.equal(DEFAULT)
  })

  it('staker can not redeem in DEFAULT state because defaulted amount is more than proposer deposit amount', async () => {
    const ethixStakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPStakerBalanceBefore = await originatorStakingWithLP.balanceOf(staker.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)
    await expect(originatorStakingWithLP.connect(staker).redeem(staker.address, ethixStakerBalanceBefore)).to.be.revertedWith(
      'WRONG_STATE'
    )
    const ethixStakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPStakerBalanceAfter = await originatorStakingWithLP.balanceOf(staker.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(ethixStakerBalanceAfter).to.be.equal(ethixStakerBalanceBefore)
    expect(originatorStakingWithLPStakerBalanceAfter).to.be.equal(originatorStakingWithLPStakerBalanceBefore)
    expect(originatorStakingWithLPBalanceAfter).to.be.equal(originatorStakingWithLPBalanceBefore)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})

describe('OriginatorStakingWithLP. STAKING_END state', () => {
  let deployer, emissionManager, originator, auditor, ethix, rewardsVault, governance

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())

    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, originator, auditor, ethix, rewardsVault, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
        token0.address,
        token1.address,
        POOL_FEES,
        encodePriceSqrt(DAI_ETHIX_RATIO, 1)
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    const now = await timeLatest()
    const mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: amount0Desired,
        amount1Desired: amount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)
    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId
    )
    await ethers.provider.send('evm_increaseTime', [parseInt(DISTRIBUTION_DURATION)])
    await ethers.provider.send('evm_mine')
    await originatorStakingWithLP.connect(governance).declareStakingEnd()
  })

  it('only can be declared as DEFAULT in STAKING state', async () => {
    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING_END)
    await expect(originatorStakingWithLP.connect(governance).declareDefault(DEFAULT_DEBT_AMOUNT))
    .to.be.revertedWith('ONLY_ON_STAKING_STATE')
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
