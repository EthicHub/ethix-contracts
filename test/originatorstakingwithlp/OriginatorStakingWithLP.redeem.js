/*
    Originator Staking with LP redeem test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber, ContractFactory } = require('ethers')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  APY_OBJETIVE,
  POOL_FEES,
  DAI_ETHIX_RATIO,
  MIN_TICK,
  MAX_TICK
} = require('../helpers/constants')

const { setSnapshotId, snapshotId } = require('../helpers/make-suite')

const { evmRevert, evmSnapshot, linkLibraries, timeLatest } = require('../helpers/utils')
const { encodePriceSqrt } = require('../helpers/encodepricesqrt')

const UNINITIALIZED = 0
const STAKING = 1
const STAKING_END = 2

const UNISWAP_V3_FACTORY = require('@uniswap/v3-core/artifacts/contracts/UniswapV3Factory.sol/UniswapV3Factory.json')
const NFT_DESCRIPTOR_LIBRARY = require('@uniswap/v3-periphery/artifacts/contracts/libraries/NFTDescriptor.sol/NFTDescriptor.json')
const NFT_DESCRIPTOR = require('@uniswap/v3-periphery/artifacts/contracts/NonfungibleTokenPositionDescriptor.sol/NonfungibleTokenPositionDescriptor.json')
const NFT_POSITION_MANAGER = require('@uniswap/v3-periphery/artifacts/contracts/NonfungiblePositionManager.sol/NonfungiblePositionManager.json')

describe('OriginatorStakingWithLP. Redeem', () => {
  let deployer, emissionManager, staker, staker2, originator, auditor, ethix, stableCoin, rewardsVault, governance, nftPositionManager, originatorStakingWithLP

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())
    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, staker, staker2, ethix, addr1, auditor, originator, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // transfer ETHIX signers
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))


    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
        token0.address,
        token1.address,
        POOL_FEES,
        encodePrice
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    const now = await timeLatest()
    const mintParams = {
        token0: token0.address,
        token1: token1.address,
        fee: POOL_FEES,
        tickLower: MIN_TICK,
        tickUpper: MAX_TICK,
        amount0Desired: amount0Desired,
        amount1Desired: amount1Desired,
        amount0Min: 0,
        amount1Min: 0,
        recipient: deployer.address,
        deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    await originatorStakingWithLP.grantRole(ethers.utils.id('EMISSION_MANAGER'), deployer.address)
    await rewardsVault.grantRole(ethers.utils.id('TRANSFER_ROLE'), originatorStakingWithLP.address)

    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)

    expect(await originatorStakingWithLP.state()).to.be.equal(UNINITIALIZED)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId,
    )

    const emissionPerSecondExpected = BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)).mul(APY_OBJETIVE).div(10000).div(31536000)
    const config = {
      emissionPerSecond: emissionPerSecondExpected,
      totalStaked: BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      underlyingAsset: originatorStakingWithLP.address
    }

    await originatorStakingWithLP.connect(emissionManager).configureAssets([config])
  })

  it('Reverts trying to redeem 0 amount', async () => {
    const amount = '0'

    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING)
    await expect(originatorStakingWithLP.connect(staker).redeem(staker.address, amount)).to.be.revertedWith(
      'INVALID_ZERO_AMOUNT'
    )
  })

  it('User 1 stakes 20 ETHIX', async () => {
    const amount = ethers.utils.parseEther('20')
    const stakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)

    await ethix.connect(staker).approve(originatorStakingWithLP.address, amount)
    await originatorStakingWithLP.connect(staker).stake(staker.address, amount)

    const stakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(stakerBalanceAfter.toString()).to.be.equal(stakerBalanceBefore.sub(amount.toString()))
    expect(originatorStakingWithLPBalanceAfter.toString()).to.be.equal(originatorStakingWithLPBalanceBefore.add(amount.toString()))
  })

  it('User 1 tries to redeem in STAKING state', async () => {
    const amount = ethers.utils.parseEther('20')

    await expect(originatorStakingWithLP.connect(staker).redeem(staker.address, amount)).to.be.revertedWith(
      'WRONG_STATE'
    )
  })

  it('User 2 tries to stake 50 ETHIX, but stakes only 20 due to stakingGoal', async () => {
    const amount = ethers.utils.parseEther('50')
    const staker2BalanceBefore = await ethix.balanceOf(staker2.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)
    const availableStakeAmount = BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)).sub(originatorStakingWithLPBalanceBefore)

    await ethix.connect(staker2).approve(originatorStakingWithLP.address, amount)
    await originatorStakingWithLP.connect(staker2).stake(staker2.address, amount)

    const staker2BalanceAfter = await ethix.balanceOf(staker2.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(staker2BalanceAfter.toString()).to.be.equal((staker2BalanceBefore.sub(availableStakeAmount)).toString())
    expect(originatorStakingWithLPBalanceAfter.toString()).to.be.equal((originatorStakingWithLPBalanceBefore.add(availableStakeAmount)).toString())
  })

  it('Compare APY and APY objective', async () => {
    const stakingGoal = await originatorStakingWithLP.stakingGoal()
    expect(await originatorStakingWithLP.hasReachedGoal()).to.be.true
    contractBalance = await ethix.balanceOf(originatorStakingWithLP.address)
    expect(contractBalance).to.be.equal(stakingGoal)

    const currentDistribution = await originatorStakingWithLP.assets(originatorStakingWithLP.address)
    const currentEmission = currentDistribution.emissionPerSecond;
    const apy = currentEmission.mul(31536000).mul(10000).div(contractBalance)

    expect(apy.toNumber()).to.be.within(BigNumber.from(APY_OBJETIVE).sub('1').toNumber(), BigNumber.from(APY_OBJETIVE).add('1').toNumber()) // [range 0.01 percentage]
  })

  it('only emission manager can change DISTRIBUTION_END date', async () => {
    const distributionPeriodSeconds = 3600 // 1 hour
    await expect(originatorStakingWithLP.connect(staker2).changeDistributionEndDate(distributionPeriodSeconds)).to.be.revertedWith(
      'ONLY_EMISSION_MANAGER'
    )
  })

  it('can change DISTRIBUTION_END date to 1 day before the initial DISTRIBUTION_END value', async () => {
    const initialDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    const distributionPeriodSeconds = 86400 // 1 day

    await originatorStakingWithLP.connect(emissionManager).changeDistributionEndDate(initialDistributionEnd.sub(distributionPeriodSeconds))
    const newDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    expect(newDistributionEnd).to.be.equal(initialDistributionEnd.sub(distributionPeriodSeconds))
  })

  it('can change DISTRIBUTION_END date to 1 day after the initial DISTRIBUTION_END value', async () => {
    const initialDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    const distributionPeriodSeconds = 86400 // 1 day

    await originatorStakingWithLP.connect(emissionManager).changeDistributionEndDate(initialDistributionEnd.add(distributionPeriodSeconds))
    const newDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    expect(newDistributionEnd).to.be.equal(initialDistributionEnd.add(distributionPeriodSeconds))
  })

  it('can declare STAKING_END before DISTRIBUTION_END date', async () => {
    const initialDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()
    const distributionPeriodSeconds = 3600 // 1 hour

    await ethers.provider.send('evm_increaseTime', [distributionPeriodSeconds])
    await ethers.provider.send('evm_mine')

    await originatorStakingWithLP.connect(governance).declareStakingEnd()

    const newDistributionEnd = await originatorStakingWithLP.DISTRIBUTION_END()

    expect(await originatorStakingWithLP.state()).to.be.equal(STAKING_END)
    expect(newDistributionEnd).not.to.be.equal(initialDistributionEnd)
    expect(initialDistributionEnd).to.be.above(newDistributionEnd)
  })

  it('User 1 tries to redeem a bigger amount that he has staked, receiving the balance', async () => {
    const amount = ethers.utils.parseEther('1000')

    const ethixStakerBalanceBefore = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPStakerBalanceBefore = await originatorStakingWithLP.balanceOf(staker.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)

    await originatorStakingWithLP.connect(staker).redeem(staker.address, amount)

    const ethixStakerBalanceAfter = await ethix.balanceOf(staker.address)
    const originatorStakingWithLPStakerBalanceAfter = await originatorStakingWithLP.balanceOf(staker.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(ethixStakerBalanceAfter.sub(originatorStakingWithLPStakerBalanceBefore)).to.be.equal(ethixStakerBalanceBefore)
    expect(originatorStakingWithLPStakerBalanceAfter).to.be.equal(0)
    expect(originatorStakingWithLPBalanceAfter).to.be.equal(originatorStakingWithLPBalanceBefore.sub(originatorStakingWithLPStakerBalanceBefore))
  })

  it('User 1 tries to redeem again when his/her balance on the contract is already 0', async () => {
    const amount = ethers.utils.parseEther('20')
    await expect(originatorStakingWithLP.connect(staker).redeem(staker.address, amount))
    .to.be.revertedWith('SENDER_BALANCE_ZERO')
  })

  it('User 2 tries to redeem half of what staked previously (10 Ethix)', async () => {
    const ethixStaker2BalanceBefore = await ethix.balanceOf(staker2.address)
    const originatorStakingWithLPStaker2BalanceBefore = await originatorStakingWithLP.balanceOf(staker2.address)
    const originatorStakingWithLPBalanceBefore = await ethix.balanceOf(originatorStakingWithLP.address)

    await originatorStakingWithLP.connect(staker2).redeem(staker2.address, originatorStakingWithLPStaker2BalanceBefore.div(2))

    const ethixStaker2BalanceAfter = await ethix.balanceOf(staker2.address)
    const originatorStakingWithLPStaker2BalanceAfter = await originatorStakingWithLP.balanceOf(staker2.address)
    const originatorStakingWithLPBalanceAfter = await ethix.balanceOf(originatorStakingWithLP.address)

    expect(ethixStaker2BalanceAfter).to.be.equal(ethixStaker2BalanceBefore.add(originatorStakingWithLPStaker2BalanceBefore.div(2)))
    expect(originatorStakingWithLPStaker2BalanceAfter).to.be.equal(originatorStakingWithLPStaker2BalanceBefore.sub(originatorStakingWithLPStaker2BalanceBefore.div(2)))
    expect(originatorStakingWithLPBalanceAfter).to.be.equal(originatorStakingWithLPBalanceBefore.sub(originatorStakingWithLPStaker2BalanceBefore.div(2)))
  })


  after(async () => {
    await evmRevert(snapshotId)
  })
})
