/*
    Originator Staking with LP transfer test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { ethers } = require('hardhat')
const { BigNumber, ContractFactory } = require('ethers')

const {
  DISTRIBUTION_DURATION,
  ORIGINATOR_SETUP_PERCENTAGE,
  AUDITOR_SETUP_PERCENTAGE,
  STAKING_GOAL_ETHIX,
  DEFAULT_DELAY,
  POOL_FEES,
  DAI_ETHIX_RATIO,
  MIN_TICK,
  MAX_TICK
} = require('../helpers/constants')

const { compareRewardsAtAction, compareRewardsAtTransfer } = require('../helpers/reward')
const { evmRevert, evmSnapshot, linkLibraries, timeLatest } = require('../helpers/utils')
const { setSnapshotId, snapshotId } = require('../helpers/make-suite')
const { encodePriceSqrt } = require('../helpers/encodepricesqrt')

const UNISWAP_V3_FACTORY = require('@uniswap/v3-core/artifacts/contracts/UniswapV3Factory.sol/UniswapV3Factory.json')
const NFT_DESCRIPTOR_LIBRARY = require('@uniswap/v3-periphery/artifacts/contracts/libraries/NFTDescriptor.sol/NFTDescriptor.json')
const NFT_DESCRIPTOR = require('@uniswap/v3-periphery/artifacts/contracts/NonfungibleTokenPositionDescriptor.sol/NonfungibleTokenPositionDescriptor.json')
const NFT_POSITION_MANAGER = require('@uniswap/v3-periphery/artifacts/contracts/NonfungiblePositionManager.sol/NonfungiblePositionManager.json')

describe('OriginatorStakingWithLP. Transfers', () => {
  let deployer, emissionManager, staker, staker2, staker3, originator, auditor, ethix, rewardsVault, governance

  const auditorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(AUDITOR_SETUP_PERCENTAGE).div(10000)
  const originatorAmount = BigNumber.from(STAKING_GOAL_ETHIX).mul(ORIGINATOR_SETUP_PERCENTAGE).div(10000)

  before(async () => {
    setSnapshotId(await evmSnapshot())

    let signers = await ethers.getSigners()

    ;[deployer, emissionManager, staker, staker2, staker3, originator, auditor, ethix, rewardsVault, governance] = signers

    // Deploy Ethix, stableCoin and WETH
    const EthixToken = await ethers.getContractFactory('EthixToken')
    ethix = await upgrades.deployProxy(EthixToken)
    const TestToken = await ethers.getContractFactory('TestToken')
    stableCoin = await TestToken.deploy()
    const weth = await TestToken.deploy()

    // set tokens if mayor or minor
    if (stableCoin.address < ethix.address) {
      token0 = stableCoin
      token1 = ethix
      encodePrice = encodePriceSqrt(DAI_ETHIX_RATIO, 1)
      amount0Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
      amount1Desired = auditorAmount.add(originatorAmount).div(2)
    } else {
      token0 = ethix
      token1 = stableCoin
      encodePrice = encodePriceSqrt(1, DAI_ETHIX_RATIO)
      amount0Desired = auditorAmount.add(originatorAmount).div(2)
      amount1Desired = auditorAmount.add(originatorAmount).div(2).div(DAI_ETHIX_RATIO)
    }

    // transfer ETHIX signers
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    // Deploy Rewards reserve and transfer (10000) ethixs to reserve
    const RewardsVault = await ethers.getContractFactory('ERC20Reserve')
    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    // Deploy Uniswap Factory
    const UniswapV3Factory = new ContractFactory(UNISWAP_V3_FACTORY.abi, UNISWAP_V3_FACTORY.bytecode, deployer)
    const uniV3Factory = await UniswapV3Factory.deploy()

    // NFT Position Manager
    const NFTDescriptorLibrary = new ContractFactory(NFT_DESCRIPTOR_LIBRARY.abi, NFT_DESCRIPTOR_LIBRARY.bytecode, deployer)
    const nftDescriptorLibrary = await NFTDescriptorLibrary.deploy()
    const linkedBytecode = linkLibraries({ bytecode: NFT_DESCRIPTOR.bytecode, linkReferences: NFT_DESCRIPTOR.linkReferences }, { NFTDescriptor: nftDescriptorLibrary.address });
    const NFTDescriptor = new ContractFactory(NFT_DESCRIPTOR.abi, linkedBytecode, deployer)
    const nftDescriptor = await NFTDescriptor.deploy(weth.address, ethers.utils.formatBytes32String('EthicHub'))
    const NFTPositionManager = new ContractFactory(NFT_POSITION_MANAGER.abi, NFT_POSITION_MANAGER.bytecode, deployer)
    nftPositionManager = await NFTPositionManager.deploy(uniV3Factory.address, weth.address, nftDescriptor.address)

    // Approve and transfer tokens to deployer and nftPositionManager
    await ethix.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await stableCoin.approve(nftPositionManager.address, ethers.constants.MaxUint256)
    await ethix.transfer(deployer.address, ethers.utils.parseEther('10000'))
    await stableCoin.transfer(deployer.address, ethers.utils.parseEther('10000'))

    // Create Uniswap v3 pool
    await nftPositionManager.createAndInitializePoolIfNecessary(
      token0.address,
      token1.address,
      POOL_FEES,
      encodePrice
    )
    uniV3PoolAddress = await uniV3Factory.getPool(token0.address, token1.address, 3000)
    const now = await timeLatest()
    const mintParams = {
      token0: token0.address,
      token1: token1.address,
      fee: POOL_FEES,
      tickLower: MIN_TICK,
      tickUpper: MAX_TICK,
      amount0Desired: amount0Desired,
      amount1Desired: amount1Desired,
      amount0Min: 0,
      amount1Min: 0,
      recipient: deployer.address,
      deadline: now.add(5)
    }
    await nftPositionManager.mint(mintParams)
    nftPositionId = await nftPositionManager.tokenOfOwnerByIndex(deployer.address, 0)

    const OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    originatorStakingWithLP = await upgrades.deployProxy(OriginatorStakingWithLP, [
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      uniV3PoolAddress,
      nftPositionManager.address
    ])
    expect(await nftPositionManager.ownerOf(nftPositionId)).to.equal(deployer.address)
    // Approve transfer nftPostion to OriginatorStaking contract
    await nftPositionManager.approve(originatorStakingWithLP.address, nftPositionId)
    await originatorStakingWithLP.connect(emissionManager).setUpTerms(
      auditor.address,
      originator.address,
      governance.address,
      AUDITOR_SETUP_PERCENTAGE,
      ORIGINATOR_SETUP_PERCENTAGE,
      BigNumber.from(STAKING_GOAL_ETHIX).sub(auditorAmount.add(originatorAmount)),
      DEFAULT_DELAY,
      nftPositionId
    )
    await originatorStakingWithLP.grantRole(ethers.utils.id('EMISSION_MANAGER'), deployer.address)
  })

  it('User 1 stakes 30 ETHIX', async () => {
    const amount = ethers.utils.parseEther('30')
    const actions = () => [
      ethix.connect(staker).approve(originatorStakingWithLP.address, amount),
      originatorStakingWithLP.connect(staker).stake(staker.address, amount),
    ]
    await compareRewardsAtAction(originatorStakingWithLP, staker.address, actions)
  })

  it('User 1 transfers 30 originatorStakingWithLP to User 2', async () => {
    const amount = ethers.utils.parseEther('30')
    await compareRewardsAtTransfer(originatorStakingWithLP, staker, staker2, amount, true, false)
  })

  it('User 2 transfers 30 originatorStakingWithLP to himself', async () => {
    const amount = ethers.utils.parseEther('30')
    await compareRewardsAtTransfer(originatorStakingWithLP, staker2, staker2, amount, true, true)
  })

  it('User 2 transfers 30 originatorStakingWithLP to user 2, with rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('30')

    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }
    await compareRewardsAtTransfer(originatorStakingWithLP, staker2, staker2, amount, false, false, assetConfig)
  })

  it('User 3 stakes and transfers 10 originatorStakingWithLP to user 2, with rewards not enabled', async () => {
    const amount = ethers.utils.parseEther('10')
    // Configuration to disable emission
    const assetConfig = {
      emissionPerSecond: '0',
      totalStaked: '0',
    }

    const actions = () => [
      ethix.connect(staker3).approve(originatorStakingWithLP.address, amount),
      originatorStakingWithLP.connect(staker3).stake(staker3.address, amount),
    ]

    await compareRewardsAtAction(originatorStakingWithLP, staker3.address, actions, false, assetConfig)
    await compareRewardsAtTransfer(originatorStakingWithLP, staker3, staker2, amount, false, false, assetConfig)
  })

  after(async () => {
    await evmRevert(snapshotId)
  })
})
