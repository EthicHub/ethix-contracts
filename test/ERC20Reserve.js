const { expect } = require('chai')
const { BigNumber } = require('ethers')
const { ethers } = require('hardhat')

describe('ERC20Reserve', function () {
  let owner, addr1, token

  beforeEach(async () => {
    const Reserve = await ethers.getContractFactory('ERC20Reserve')
    const Token = await ethers.getContractFactory('TestToken')

    ;[owner, addr1] = await ethers.getSigners()

    token = await Token.deploy()
    anotherToken = await Token.deploy()
    reserve = await Reserve.deploy(token.address)

    await token.transfer(reserve.address, 100)
  })

  it('Check balance', async () => {
    expect(await reserve.balance()).to.equal(100)
  })

  it('Transfer', async () => {
    await reserve.transfer(addr1.address, 100)
    expect(await token.balanceOf(addr1.address)).to.equal(100)
  })

  it('Transfer from address without transfer role', async () => {
    expect(reserve.connect(addr1).transfer(addr1.address, 100)).to.be.revertedWith(
      'ERC20Reserve: Caller is not a transferrer'
    )
  })

  it('Recover funds fails if token is main token', async () => {
    expect(reserve.rescueFunds(token.address, addr1.address, 100)).to.be.revertedWith(
      'ERC20Reserve: Cannot claim token held by the contract'
    )
  })

  it('Recover funds fails if not rescuer', async () => {
    await anotherToken.transfer(reserve.address, 100)
    expect(
      reserve.connect(addr1).rescueFunds(anotherToken.address, addr1.address, 100)
    ).to.be.revertedWith('ERC20Reserve: Caller is not a rescuer')
  })


  it('Recover funds', async () => {
    await anotherToken.transfer(reserve.address, 100)
    await reserve.rescueFunds(anotherToken.address, addr1.address, 100)
    expect(await anotherToken.balanceOf(addr1.address)).to.equal(100)
  })
})
