/*
    Originator Staking factory test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { upgrades } = require('hardhat')
const { getImplementationAddress, getAdminAddress } = require('@openzeppelin/upgrades-core')

const {
  DISTRIBUTION_DURATION
} = require('../helpers/constants')

describe('OriginatorStakingFactory', () => {
  let deployer, emissionManager

  before(async () => {
    OriginatorFactory = await ethers.getContractFactory('OriginatorStakingFactory')
    OriginatorStaking = await ethers.getContractFactory('OriginatorStaking')
    EthixToken = await ethers.getContractFactory('EthixToken')
    RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[deployer, emissionManager] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    firstOriginatorStaking = await deployOriginator('First', 'FIRST', OriginatorStaking)
    firstImplOriginatorStakingAddress = await getImplementationAddress(firstOriginatorStaking.provider, firstOriginatorStaking.address)

    implementation = await OriginatorStaking.deploy()
    
    originatorFactory = await upgrades.deployProxy(OriginatorFactory, [implementation.address])
    originatorFactoryProxyAdmin = await getAdminAddress(originatorFactory.provider, originatorFactory.address)
  })

  it('should not create a new originator, only owner', async () => {
    await expect(originatorFactory.connect(emissionManager).createOriginator(
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    )).to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('should create a new originator', async () => {
    await originatorFactory.createOriginator(
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    )
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implementation.address)

    const originatorAddress = await originatorFactory.getOriginator('OT')
    expect(await originatorFactory.allOriginators(0)).to.be.equal(originatorAddress)

    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(1)

    const originator = OriginatorStaking.attach(originatorAddress)
    expect(await originator.symbol()).to.be.equal('OT')
    expect(await originator.name()).to.be.equal('OriginatorTest')
    expect(await originator.STAKED_TOKEN()).to.be.equal(ethix.address)
    expect(await originator.REWARDS_VAULT()).to.be.equal(rewardsVault.address)
    // Is ERC1967 transparent upgradeable proxy contract
    const implOriginatorStakingAddress = await getImplementationAddress(originator.provider, originator.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implOriginatorStakingAddress)
  })

  it('should return factory item implementation', async function () {
    expect(await originatorFactory.originatorImplementation()).equal(implementation.address)
  })

  it('should upgrade factory', async function () {
    originatorFactoryUpgraded = await upgrades.upgradeProxy(originatorFactory.address, OriginatorFactory)
    expect(await originatorFactory.address).equal(originatorFactoryUpgraded.address)
  })

  it('should not add new originator to factory if originator proxyAdmin is diferent to factory originatorProxyAdmin', async function () {
    const originatorStaking = await deployOriginator('Test', 'TEST', OriginatorStaking)
    const implementationOriginatorStakingAddress = await getImplementationAddress(originatorStaking.provider, originatorStaking.address)
    expect(implementationOriginatorStakingAddress).to.be.not.equal(implementation.address)
    const originatorProxyAdminAddress = await getAdminAddress(originatorStaking.provider, originatorStaking.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.not.equal(originatorProxyAdminAddress)
    await expect(originatorFactory.addOriginator(originatorStaking.address))
      .to.be.revertedWith('PROXYADMIN_IS_DIFFERENT')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(1)
  })

  it('change implementation of originator so now should add to factory', async function () {
    const originatorStaking = await deployOriginator('Test', 'TEST', OriginatorStaking)
    const implementationOriginatorStakingAddress = await getImplementationAddress(originatorStaking.provider, originatorStaking.address)
    expect(implementationOriginatorStakingAddress).to.be.not.equal(implementation.address)

    // change ProxyAdmin to Factory
    originatorProxyAdminAddress = await getAdminAddress(originatorStaking.provider, originatorStaking.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.not.equal(originatorProxyAdminAddress)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', originatorProxyAdminAddress, deployer)
    await proxyAdmin.changeProxyAdmin(originatorStaking.address, await originatorFactory.originatorProxyAdmin())
    originatorProxyAdminAddress = await getAdminAddress(originatorStaking.provider, originatorStaking.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.equal(originatorProxyAdminAddress)

    await originatorFactory.addOriginator(originatorStaking.address)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    expect(await originatorFactory.getOriginator('TEST')).to.be.equal(originatorStaking.address)
    const originator = OriginatorStaking.attach(originatorStaking.address)
    // Is ERC1967 transparent upgradeable proxy contract
    const implOriginatorStakingAddress = await getImplementationAddress(originator.provider, originator.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implOriginatorStakingAddress)
  })

  it('should not add originator if already exists', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStaking = await deployOriginator('Test', 'TEST', OriginatorStaking)
    const originatorAdminAddress = await getAdminAddress(originatorStaking.provider, originatorStaking.address)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', originatorAdminAddress, deployer)
    await proxyAdmin.upgrade(originatorStaking.address, implementation.address)
    await expect(originatorFactory.addOriginator(originatorStaking.address))
    .to.be.revertedWith('ORIGINATOR_EXISTS')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should not add originator, only owner', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStaking = await deployOriginator('Other', 'OTHER', OriginatorStaking)
    const originatorAdminAddress = await getAdminAddress(originatorStaking.provider, originatorStaking.address)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', originatorAdminAddress, deployer)
    await proxyAdmin.upgrade(originatorStaking.address, implementation.address)
    await expect(originatorFactory.connect(emissionManager).addOriginator(originatorStaking.address))
    .to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('should remove originator if exists', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    await originatorFactory.createOriginator(
      'Other',
      'OTHER',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    )
    expect(await originatorFactory.allOriginators(0)).to.be.equal(await originatorFactory.getOriginator('OT'))
    expect(await originatorFactory.allOriginators(1)).to.be.equal(await originatorFactory.getOriginator('TEST'))
    expect(await originatorFactory.allOriginators(2)).to.be.equal(await originatorFactory.getOriginator('OTHER'))
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(3)

    const originatorStakingAddress = await originatorFactory.getOriginator('TEST')
    await originatorFactory.deleteOriginator(originatorStakingAddress)
    expect(await originatorFactory.allOriginators(0)).to.be.equal(await originatorFactory.getOriginator('OT'))
    expect(await originatorFactory.allOriginators(1)).to.be.equal(await originatorFactory.getOriginator('OTHER'))
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    expect(await originatorFactory.getOriginator('TEST')).to.be.equal(ethers.constants.AddressZero)
  })

  it('should not remove originator from factory if not exists', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStaking = await deployOriginator('Test', 'TEST', OriginatorStaking)
    await expect(originatorFactory.deleteOriginator(originatorStaking.address))
    .to.be.revertedWith('ORIGINATOR_NOT_EXISTS')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should not remove originator from factory if not owner', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStaking = await deployOriginator('Test', 'TEST', OriginatorStaking)
    await expect(originatorFactory.connect(emissionManager).deleteOriginator(originatorStaking.address))
    .to.be.revertedWith('Ownable: caller is not the owner')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should deploy without factory and not add to the factory if originator proxyAdmin is diferent to factory originatorProxyAdmin', async function () {
    originatorAddress = await originatorFactory.getOriginator('FIRST')
    expect(originatorAddress).to.be.equal(ethers.constants.AddressZero)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)

    const implementationOriginatorStakingAddress = await getImplementationAddress(firstOriginatorStaking.provider, firstOriginatorStaking.address)
    expect(implementationOriginatorStakingAddress).to.be.not.equal(implementation.address)
    await expect(originatorFactory.addOriginator(firstOriginatorStaking.address)).to.be.revertedWith('PROXYADMIN_IS_DIFFERENT')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should deploy without factory and add to the factory', async function () {
    originatorAddress = await originatorFactory.getOriginator('FIRST')
    expect(originatorAddress).to.be.equal(ethers.constants.AddressZero)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)


    // change ProxyAdmin to Factory
    firstOriginatorAdminAddress = await getAdminAddress(firstOriginatorStaking.provider, firstOriginatorStaking.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.not.equal(firstOriginatorAdminAddress)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', firstOriginatorAdminAddress, deployer)
    await proxyAdmin.changeProxyAdmin(firstOriginatorStaking.address, await originatorFactory.originatorProxyAdmin())
    firstOriginatorAdminAddress = await getAdminAddress(firstOriginatorStaking.provider, firstOriginatorStaking.address)
    expect(await originatorFactory.originatorProxyAdmin()).equal(firstOriginatorAdminAddress)

    await originatorFactory.addOriginator(firstOriginatorStaking.address)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(3)
    expect(await originatorFactory.getOriginator('FIRST')).to.be.equal(firstOriginatorStaking.address)
    originatorAddress = await originatorFactory.getOriginator('FIRST')
    originator = OriginatorStaking.attach(originatorAddress)
    implOriginatorStakingAddress = await getImplementationAddress(originator.provider, originator.address)
    expect(implOriginatorStakingAddress).to.be.equal(await originatorFactory.originatorImplementation())
  })

  it('should not change implementation', async () => {
    const newOriginatorStaking = await deployOriginator('New', 'NEW', OriginatorStaking)
    const newImplementationOriginatorStakingAddress = await getImplementationAddress(newOriginatorStaking.provider, newOriginatorStaking.address)
    await expect(originatorFactory.connect(emissionManager).changeOriginatorImplementation(newImplementationOriginatorStakingAddress))
    .to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('should change implementation', async () => {
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implementation.address)
    const beforeImplFirstOriginatorStakingAddress = await getImplementationAddress(firstOriginatorStaking.provider, firstOriginatorStaking.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(beforeImplFirstOriginatorStakingAddress)


    const OriginatorStakingTest = await ethers.getContractFactory('OriginatorStakingTest')
    const newOriginatorStaking = await deployOriginator('New', 'NEW', OriginatorStakingTest)
    const newImplementationOriginatorStakingAddress = await getImplementationAddress(newOriginatorStaking.provider, newOriginatorStaking.address)
    expect(newImplementationOriginatorStakingAddress).to.be.not.equal(await originatorFactory.originatorImplementation())
    await originatorFactory.changeOriginatorImplementation(newImplementationOriginatorStakingAddress)
    expect(await originatorFactory.originatorImplementation()).equal(newImplementationOriginatorStakingAddress)

    expect(await originatorFactory.originatorImplementation()).to.be.not.equal(implementation.address)
    const afterImplFirstOriginatorStakingAddress = await getImplementationAddress(firstOriginatorStaking.provider, firstOriginatorStaking.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(afterImplFirstOriginatorStakingAddress)

    // check new function getVersion
    originatorAddress = await originatorFactory.getOriginator('OT')
    originator = OriginatorStakingTest.attach(originatorAddress)
    expect(await originator.getVersion()).to.be.equal(1)
  })

  async function deployOriginator(name, symbol, contractFactory) {
    const originatorStaking = await upgrades.deployProxy(contractFactory, [
      name,
      symbol,
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION
    ])

    return originatorStaking
  }
})