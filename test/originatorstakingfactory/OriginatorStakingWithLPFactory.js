/*
    Originator Staking factory test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { upgrades } = require('hardhat')
const { getImplementationAddress, getAdminAddress } = require('@openzeppelin/upgrades-core')

const {
  DISTRIBUTION_DURATION
} = require('../helpers/constants')

describe('OriginatorStakingWithLPFactory', () => {
  let deployer, emissionManager

  before(async () => {
    OriginatorFactory = await ethers.getContractFactory('OriginatorStakingWithLPFactory')
    OriginatorStakingWithLP = await ethers.getContractFactory('OriginatorStakingWithLP')
    EthixToken = await ethers.getContractFactory('EthixToken')
    RewardsVault = await ethers.getContractFactory('ERC20Reserve')

    let signers = await ethers.getSigners()

    ;[deployer, emissionManager] = signers

    ethix = await upgrades.deployProxy(EthixToken)
    await signers.map(
      async (signer) => await ethix.transfer(signer.address, ethers.utils.parseEther('100'))
    )

    rewardsVault = await RewardsVault.deploy(ethix.address)
    await ethix.transfer(rewardsVault.address, ethers.utils.parseEther('10000'))

    firstOriginatorStakingWithLP = await deployOriginatorWithLP('First', 'FIRST', OriginatorStakingWithLP)
    firstImplOriginatorStakingWithLPAddress = await getImplementationAddress(firstOriginatorStakingWithLP.provider, firstOriginatorStakingWithLP.address)

    implementation = await OriginatorStakingWithLP.deploy()
    
    originatorFactory = await upgrades.deployProxy(OriginatorFactory, [implementation.address])
    originatorFactoryProxyAdmin = await getAdminAddress(originatorFactory.provider, originatorFactory.address)
  })

  it('should not create a new originator, only owner', async () => {
    await expect(originatorFactory.connect(emissionManager).createOriginatorWithLP(
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      ethers.Wallet.createRandom().address,
      ethers.Wallet.createRandom().address
    )).to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('should create a new originator', async () => {
    await originatorFactory.createOriginatorWithLP(
      'OriginatorTest',
      'OT',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      ethers.Wallet.createRandom().address,
      ethers.Wallet.createRandom().address
    )
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implementation.address)

    const originatorAddress = await originatorFactory.getOriginator('OT')
    expect(await originatorFactory.allOriginators(0)).to.be.equal(originatorAddress)

    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(1)

    const originator = OriginatorStakingWithLP.attach(originatorAddress)
    expect(await originator.symbol()).to.be.equal('OT')
    expect(await originator.name()).to.be.equal('OriginatorTest')
    expect(await originator.TOKEN_TO_STAKE()).to.be.equal(ethix.address)
    expect(await originator.REWARDS_VAULT()).to.be.equal(rewardsVault.address)
    // Is ERC1967 transparent upgradeable proxy contract
    const implOriginatorStakingWithLPAddress = await getImplementationAddress(originator.provider, originator.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implOriginatorStakingWithLPAddress)
  })

  it('should return factory item implementation', async function () {
    expect(await originatorFactory.originatorImplementation()).equal(implementation.address)
  })

  it('should upgrade factory', async function () {
    originatorFactoryUpgraded = await upgrades.upgradeProxy(originatorFactory.address, OriginatorFactory)
    expect(await originatorFactory.address).equal(originatorFactoryUpgraded.address)
  })

  it('should not add new originator to factory if originator proxyAdmin is diferent to factory originatorProxyAdmin', async function () {
    const originatorStakingWithLP = await deployOriginatorWithLP('Test', 'TEST', OriginatorStakingWithLP)
    const implementationOriginatorStakingWithLPAddress = await getImplementationAddress(originatorStakingWithLP.provider, originatorStakingWithLP.address)
    expect(implementationOriginatorStakingWithLPAddress).to.be.not.equal(implementation.address)
    const originatorProxyAdminAddress = await getAdminAddress(originatorStakingWithLP.provider, originatorStakingWithLP.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.not.equal(originatorProxyAdminAddress)
    await expect(originatorFactory.addOriginator(originatorStakingWithLP.address))
      .to.be.revertedWith('PROXYADMIN_IS_DIFFERENT')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(1)
  })

  it('change implementation of originator so now should add to factory', async function () {
    const originatorStakingWithLP = await deployOriginatorWithLP('Test', 'TEST', OriginatorStakingWithLP)
    const implementationOriginatorStakingWithLPAddress = await getImplementationAddress(originatorStakingWithLP.provider, originatorStakingWithLP.address)
    expect(implementationOriginatorStakingWithLPAddress).to.be.not.equal(implementation.address)

    // change ProxyAdmin to Factory
    originatorProxyAdminAddress = await getAdminAddress(originatorStakingWithLP.provider, originatorStakingWithLP.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.not.equal(originatorProxyAdminAddress)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', originatorProxyAdminAddress, deployer)
    await proxyAdmin.changeProxyAdmin(originatorStakingWithLP.address, await originatorFactory.originatorProxyAdmin())
    originatorProxyAdminAddress = await getAdminAddress(originatorStakingWithLP.provider, originatorStakingWithLP.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.equal(originatorProxyAdminAddress)

    await originatorFactory.addOriginator(originatorStakingWithLP.address)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    expect(await originatorFactory.getOriginator('TEST')).to.be.equal(originatorStakingWithLP.address)
    const originator = OriginatorStakingWithLP.attach(originatorStakingWithLP.address)
    // Is ERC1967 transparent upgradeable proxy contract
    const implOriginatorStakingWithLPAddress = await getImplementationAddress(originator.provider, originator.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implOriginatorStakingWithLPAddress)
  })

  it('should not add originator if already exists', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStakingWithLP = await deployOriginatorWithLP('Test', 'TEST', OriginatorStakingWithLP)
    const originatorAdminAddress = await getAdminAddress(originatorStakingWithLP.provider, originatorStakingWithLP.address)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', originatorAdminAddress, deployer)
    await proxyAdmin.upgrade(originatorStakingWithLP.address, implementation.address)
    await expect(originatorFactory.addOriginator(originatorStakingWithLP.address))
    .to.be.revertedWith('ORIGINATOR_EXISTS')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should not add originator, only owner', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStakingWithLP = await deployOriginatorWithLP('Other', 'OTHER', OriginatorStakingWithLP)
    const originatorAdminAddress = await getAdminAddress(originatorStakingWithLP.provider, originatorStakingWithLP.address)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', originatorAdminAddress, deployer)
    await proxyAdmin.upgrade(originatorStakingWithLP.address, implementation.address)
    await expect(originatorFactory.connect(emissionManager).addOriginator(originatorStakingWithLP.address))
    .to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('should remove originator if exists', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    await originatorFactory.createOriginatorWithLP(
      'Other',
      'OTHER',
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      ethers.Wallet.createRandom().address,
      ethers.Wallet.createRandom().address
    )
    expect(await originatorFactory.allOriginators(0)).to.be.equal(await originatorFactory.getOriginator('OT'))
    expect(await originatorFactory.allOriginators(1)).to.be.equal(await originatorFactory.getOriginator('TEST'))
    expect(await originatorFactory.allOriginators(2)).to.be.equal(await originatorFactory.getOriginator('OTHER'))
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(3)

    const originatorStakingWithLPAddress = await originatorFactory.getOriginator('TEST')
    await originatorFactory.deleteOriginator(originatorStakingWithLPAddress)
    expect(await originatorFactory.allOriginators(0)).to.be.equal(await originatorFactory.getOriginator('OT'))
    expect(await originatorFactory.allOriginators(1)).to.be.equal(await originatorFactory.getOriginator('OTHER'))
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    expect(await originatorFactory.getOriginator('TEST')).to.be.equal(ethers.constants.AddressZero)
  })

  it('should not remove originator from factory if not exists', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStakingWithLP = await deployOriginatorWithLP('Test', 'TEST', OriginatorStakingWithLP)
    await expect(originatorFactory.deleteOriginator(originatorStakingWithLP.address))
    .to.be.revertedWith('ORIGINATOR_NOT_EXISTS')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should not remove originator from factory if not owner', async function () {
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
    const originatorStakingWithLP = await deployOriginatorWithLP('Test', 'TEST', OriginatorStakingWithLP)
    await expect(originatorFactory.connect(emissionManager).deleteOriginator(originatorStakingWithLP.address))
    .to.be.revertedWith('Ownable: caller is not the owner')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should deploy without factory and not add to the factory if originator proxyAdmin is diferent to factory originatorProxyAdmin', async function () {
    originatorAddress = await originatorFactory.getOriginator('FIRST')
    expect(originatorAddress).to.be.equal(ethers.constants.AddressZero)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)

    const implementationOriginatorStakingWithLPAddress = await getImplementationAddress(firstOriginatorStakingWithLP.provider, firstOriginatorStakingWithLP.address)
    expect(implementationOriginatorStakingWithLPAddress).to.be.not.equal(implementation.address)
    await expect(originatorFactory.addOriginator(firstOriginatorStakingWithLP.address)).to.be.revertedWith('PROXYADMIN_IS_DIFFERENT')
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)
  })

  it('should deploy without factory and add to the factory', async function () {
    originatorAddress = await originatorFactory.getOriginator('FIRST')
    expect(originatorAddress).to.be.equal(ethers.constants.AddressZero)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(2)


    // change ProxyAdmin to Factory
    firstOriginatorAdminAddress = await getAdminAddress(firstOriginatorStakingWithLP.provider, firstOriginatorStakingWithLP.address)
    expect(await originatorFactory.originatorProxyAdmin()).to.be.not.equal(firstOriginatorAdminAddress)
    const proxyAdmin = await ethers.getContractAt('ProxyAdmin', firstOriginatorAdminAddress, deployer)
    await proxyAdmin.changeProxyAdmin(firstOriginatorStakingWithLP.address, await originatorFactory.originatorProxyAdmin())
    firstOriginatorAdminAddress = await getAdminAddress(firstOriginatorStakingWithLP.provider, firstOriginatorStakingWithLP.address)
    expect(await originatorFactory.originatorProxyAdmin()).equal(firstOriginatorAdminAddress)

    await originatorFactory.addOriginator(firstOriginatorStakingWithLP.address)
    expect(await originatorFactory.allOriginatorsLength()).to.be.equal(3)
    expect(await originatorFactory.getOriginator('FIRST')).to.be.equal(firstOriginatorStakingWithLP.address)
    originatorAddress = await originatorFactory.getOriginator('FIRST')
    originator = OriginatorStakingWithLP.attach(originatorAddress)
    implOriginatorStakingWithLPAddress = await getImplementationAddress(originator.provider, originator.address)
    expect(implOriginatorStakingWithLPAddress).to.be.equal(await originatorFactory.originatorImplementation())
  })

  it('should not change implementation', async () => {
    const newOriginatorStakingWithLP = await deployOriginatorWithLP('New', 'NEW', OriginatorStakingWithLP)
    const newImplementationOriginatorStakingWithLPAddress = await getImplementationAddress(newOriginatorStakingWithLP.provider, newOriginatorStakingWithLP.address)
    await expect(originatorFactory.connect(emissionManager).changeOriginatorImplementation(newImplementationOriginatorStakingWithLPAddress))
    .to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('should change implementation', async () => {
    expect(await originatorFactory.originatorImplementation()).to.be.equal(implementation.address)
    const beforeImplFirstOriginatorStakingWithLPAddress = await getImplementationAddress(firstOriginatorStakingWithLP.provider, firstOriginatorStakingWithLP.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(beforeImplFirstOriginatorStakingWithLPAddress)


    const OriginatorStakingWithLPTest = await ethers.getContractFactory('OriginatorStakingWithLPTest')
    const newOriginatorStakingWithLP = await deployOriginatorWithLP('New', 'NEW', OriginatorStakingWithLPTest)
    const newImplementationOriginatorStakingWithLPAddress = await getImplementationAddress(newOriginatorStakingWithLP.provider, newOriginatorStakingWithLP.address)
    expect(newImplementationOriginatorStakingWithLPAddress).to.be.not.equal(await originatorFactory.originatorImplementation())
    await originatorFactory.changeOriginatorImplementation(newImplementationOriginatorStakingWithLPAddress)
    expect(await originatorFactory.originatorImplementation()).equal(newImplementationOriginatorStakingWithLPAddress)

    expect(await originatorFactory.originatorImplementation()).to.be.not.equal(implementation.address)
    const afterImplFirstOriginatorStakingWithLPAddress = await getImplementationAddress(firstOriginatorStakingWithLP.provider, firstOriginatorStakingWithLP.address)
    expect(await originatorFactory.originatorImplementation()).to.be.equal(afterImplFirstOriginatorStakingWithLPAddress)

    // check new function getVersion
    originatorAddress = await originatorFactory.getOriginator('OT')
    originator = OriginatorStakingWithLPTest.attach(originatorAddress)
    expect(await originator.getVersion()).to.be.equal(1)
  })

  async function deployOriginatorWithLP(name, symbol, contractFactory) {
    const originatorStakingWithLP = await upgrades.deployProxy(contractFactory, [
      name,
      symbol,
      ethix.address,
      rewardsVault.address,
      emissionManager.address,
      DISTRIBUTION_DURATION,
      ethers.Wallet.createRandom().address,
      ethers.Wallet.createRandom().address
    ])

    return originatorStakingWithLP
  }
})