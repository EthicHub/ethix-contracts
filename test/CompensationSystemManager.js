const { expect, util } = require('chai')
const { parseEther } = require('ethers/lib/utils')
const { ethers } = require('hardhat')
const hre = require('hardhat')
const poolABI = require('../utils/balancerABI/bPool.json')

describe('CompensationSystemManager', function () {
  let owner, addr1, ethix, dai, reserve, compensationSystemManager, bpool, BPoolMock

  beforeEach(async () => {
    ;[owner, addr1, compensationReserve] = await ethers.getSigners()

    const Reserve = await ethers.getContractFactory('ERC20Reserve')
    const CompensationSystemManager = await ethers.getContractFactory('CompensationSystemManager')
    BPoolMock = await hre.waffle.deployMockContract(owner, poolABI)
    const Token = await ethers.getContractFactory('TestToken')

    ethix = await Token.deploy()
    dai = await Token.deploy()

    compensationReserve = await Reserve.deploy(ethix.address)

    compensationSystemManager = await CompensationSystemManager.deploy()
    await compensationSystemManager.initialize(
      ethix.address,
      dai.address,
      compensationReserve.address,
      BPoolMock.address
    )

    await compensationReserve.grantRole(
      ethers.utils.solidityKeccak256(['bytes'], [ethers.utils.toUtf8Bytes('TRANSFER_ROLE')]),
      compensationSystemManager.address
    )
  })

  it('storeInReserve requires amount > 0', async () => {
    expect(compensationSystemManager.storeInReserve(0, 0)).to.be.revertedWith(
      'CompensationSystemManager: Amount cannot be 0'
    )
  })

  it('storeInReserve', async () => {
    console.log(BPoolMock.address)
    await ethix.transfer(compensationSystemManager.address, parseEther('12')) // Done to simulate BPool swap output
    await dai.approve(compensationSystemManager.address, parseEther('10'))

    await BPoolMock.mock.swapExactAmountIn
      .withArgs(dai.address, parseEther('10'), ethix.address, 1, parseEther('20.2'))
      .returns(parseEther('12'), parseEther('34'))

    const balBefore = await ethix.balanceOf(compensationReserve.address)
    await expect(compensationSystemManager.storeInReserve(parseEther('10'), parseEther('20.2')))
      .to.emit(compensationSystemManager, 'StoreInReserve')
      .withArgs(parseEther('12'), parseEther('34'))
    const balAfter = await ethix.balanceOf(compensationReserve.address)

    expect(balAfter).to.eq(balBefore.add(parseEther('12')))
  })

  it('getCompensation requires amount > 0', async () => {
    expect(compensationSystemManager.storeInReserve(0, 0)).to.be.revertedWith(
      'CompensationSystemManager: Amount cannot be 0'
    )
  })

  it('getCompensation requires amount < maxCompensation', async () => {
    expect(
      compensationSystemManager.getCompensation(ethers.utils.parseEther('3001'), 0)
    ).to.be.revertedWith('CompensationSystemManager: Amount cannot be superior to maxCompensation')
  })

  it('getCompensation', async () => {
    const ethixIn = parseEther('10')
    const daiAmount = parseEther('20')

    await BPoolMock.mock.getSwapFee.returns(parseEther('1'))
    await BPoolMock.mock.getBalance.withArgs(ethix.address).returns(parseEther('123'))
    await BPoolMock.mock.getBalance.withArgs(dai.address).returns(parseEther('234'))
    await BPoolMock.mock.getDenormalizedWeight.withArgs(ethix.address).returns(parseEther('345'))
    await BPoolMock.mock.getDenormalizedWeight.withArgs(dai.address).returns(parseEther('456'))
    await BPoolMock.mock.calcInGivenOut
      .withArgs(
        parseEther('123'),
        parseEther('345'),
        parseEther('234'),
        parseEther('456'),
        daiAmount,
        parseEther('1')
      )
      .returns(ethixIn)
    const allowance = await ethix.allowance(
      compensationSystemManager.address,
      compensationReserve.address
    )

    const maxEthixIn = ethixIn.add(ethixIn.mul(parseEther('10')).div(parseEther('100')))
    await ethix.transfer(compensationReserve.address, maxEthixIn)
    await dai.transfer(compensationSystemManager.address, daiAmount)
    const tokenAmountIn = parseEther('10.5')
    const spotPriceAfter = parseEther('99')
    const maxPrice = parseEther('999')
    await BPoolMock.mock.swapExactAmountOut
      .withArgs(ethix.address, maxEthixIn, dai.address, daiAmount, maxPrice)
      .returns(tokenAmountIn, spotPriceAfter)
    const balanceBefore = await dai.balanceOf(owner.address)

    await compensationSystemManager.getCompensation(daiAmount, maxPrice)
    expect(await dai.balanceOf(owner.address)).to.equal(balanceBefore.add(daiAmount))
    expect(await ethix.allowance(compensationSystemManager.address, BPoolMock.address)).to.gte(
      maxEthixIn
    )
  })

  it('setMaxCompensation', async () => {
    await compensationSystemManager.setMaxCompensation(parseEther('2'))
    expect(await compensationSystemManager.maxCompensation()).to.equal(parseEther('2'))
  })

  it('setMaxCompensation fails if not owner', async () => {
    expect(compensationSystemManager.connect(addr1).setMaxCompensation(1)).to.be.revertedWith(
      'Ownable: caller is not the owner'
    )
  })

  it('setMaxEthixTolerance fails if not owner', async () => {
    expect(
      compensationSystemManager.connect(addr1).setMaxEthixTolerance(parseEther('1'))
    ).to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('setMaxEthixTolerance fails if less than 0.1 owner', async () => {
    expect(compensationSystemManager.setMaxEthixTolerance(parseEther('0.01'))).to.be.revertedWith(
      'CompensationSystemManager: wrong value for _maxEthixTolerance'
    )
  })

  it('setMaxEthixTolerance', async () => {
    await compensationSystemManager.setMaxEthixTolerance(parseEther('2'))
    expect(await compensationSystemManager.maxEthixTolerance()).to.equal(parseEther('2'))
  })
})
