const { expect } = require('chai')
const { waffle, ethers } = require('hardhat')

const provider = waffle.provider

describe('NativeReserve', function () {
  let owner, addr1, receiverWallet, token

  beforeEach(async () => {
    const Reserve = await ethers.getContractFactory('NativeReserve')
    const Token = await ethers.getContractFactory('TestToken')

    ;[owner, addr1] = await ethers.getSigners()

    token = await Token.deploy()
    reserve = await Reserve.deploy()

    receiverWallet = ethers.Wallet.createRandom()

    let signer = await provider.getSigner(owner.address)
    await signer.sendTransaction({
      to: reserve.address,
      value: 100,
    })
  })

  it('Check balance', async () => {
    expect(await reserve.balance()).to.equal(100)
  })

  it('Transfer', async () => {
    await reserve.transfer(receiverWallet.address, 100)
    expect(await provider.getBalance(receiverWallet.address)).to.equal(100)
  })

  it('Transfer from address without transfer role', async () => {
    await expect(reserve.connect(addr1).transfer(receiverWallet.address, 100)).to.be.revertedWith(
      'NativeReserve: Caller is not a transferrer'
    )
  })

  it('Recover funds fails if not owner', async () => {
    expect(await reserve.hasRole(ethers.utils.id('RESCUE_ROLE'), addr1.address)).to.be.false
    await token.transfer(reserve.address, 100)
    await expect(reserve.connect(addr1).rescueFunds(token.address, addr1.address, 100)).to.be.revertedWith(
      'NativeReserve: Caller is not a rescuer'
    )
  })

  it('Recover funds', async () => {
    await token.transfer(reserve.address, 100)
    await reserve.rescueFunds(token.address, addr1.address, 100)
    expect(await token.balanceOf(addr1.address)).to.equal(100)
  })
})
