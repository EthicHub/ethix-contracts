const { ethers } = require('hardhat')
const { expect } = require('chai')
const { createPermitDigest } = require('./helpers/erc2612')
const EIP712Domain = [
  {
    name: 'name',
    type: 'string',
  },
  {
    name: 'version',
    type: 'string',
  },
  {
    name: 'chainId',
    type: 'uint256',
  },
  {
    name: 'verifyingContract',
    type: 'address',
  },
]
const Permit = [
  {
    name: 'holder',
    type: 'address',
  },
  {
    name: 'spender',
    type: 'address',
  },
  {
    name: 'nonce',
    type: 'uint256',
  },
  {
    name: 'expiry',
    type: 'uint256',
  },
  {
    name: 'allowed',
    type: 'bool',
  },
]
const VERBOSE = false

describe('EthixToken', function () {
  let holderWallet
  let wallet1
  let wallet2
  let domain

  beforeEach(async () => {
    EthixToken = await ethers.getContractFactory('EthixToken')
    ;[wallet1, wallet2] = await ethers.getSigners()

    let network = await ethers.getDefaultProvider().getNetwork()
    let mnemonic = 'explain tackle mirror kit van hammer degree position ginger unfair soup bonus'
    holderWallet = ethers.Wallet.fromMnemonic(mnemonic)
    ethix = await EthixToken.deploy()

    await ethix.initialize()
    await ethix.transfer(holderWallet.address, 200)

    domain = {
      name: 'Ethix',
      version: '1',
      chainId: await ethix.getChainId(),
      verifyingContract: ethix.address,
    }
  })

  it('should be properly initialized', async () => {
    expect(await ethix.name()).to.equal('Ethix')
    expect(await ethix.symbol()).to.equal('ETHIX')
    expect(await ethix.decimals()).to.equal(18)
    expect(await ethix.totalSupply()).to.equal(ethers.utils.parseEther('100000000'))
  })

  it('should allow permit for approvals, then transferFrom', async () => {
    let holder = await holderWallet.getAddress()
    let spender = wallet1.address
    let nonce = await ethix.nonces(holder)
    expect(nonce).to.equal(0)
    const expiry = Math.floor(Date.now() / 1000) + 900000
    let allowed = true
    var message = {
      holder: holder,
      spender: spender,
      nonce: nonce.toNumber(),
      expiry: expiry,
      allowed: allowed,
    }

    const data = {
      types: {
        EIP712Domain,
        Permit,
      },
      domain,
      primaryType: 'Permit',
      message,
    }
    //console.log(data)
    // Hardhat network first account
    if (VERBOSE) {
      console.log('Ethix-----')
      console.log('PERMIT_TYPEHASH')
      console.log(await ethix.PERMIT_TYPEHASH())
      console.log('DOMAIN_SEPARATOR')
      console.log(await ethix.DOMAIN_SEPARATOR())
      console.log('--------------------')
    }

    const digest = await createPermitDigest(ethix, holder, spender, nonce, expiry, allowed)
    const holderPk = holderWallet.privateKey
    let signingKey = new ethers.utils.SigningKey(holderPk)
    let signature = signingKey.signDigest(digest)

    await ethix.permit(
      holder,
      spender,
      nonce,
      expiry,
      allowed,
      signature.v,
      signature.r,
      signature.s
    )

    let allowance = await ethix.allowance(holder, spender)
    expect(allowance).to.equal(ethers.constants.MaxUint256)
    const balHolder = await ethix.balanceOf(holder)
    const balWallet1 = await ethix.balanceOf(wallet1.address)
    await ethix.transferFrom(holder, wallet1.address, 200)
    const balHolderAfter = await ethix.balanceOf(holder)
    const balWallet1After = await ethix.balanceOf(wallet1.address)
    expect(balHolderAfter).to.equal(balHolder.sub(200))
    expect(balWallet1After).to.equal(balWallet1.add(200))
  })
})
