const { expect } = require('chai')
const { ethers } = require('hardhat')
const { timeLatest, evmRevert, evmSnapshot } = require('./helpers/utils')
const { setSnapshotId, snapshotId } = require('./helpers/make-suite')
const parseEther = ethers.utils.parseEther

describe('ManagedVestingReserve', function () {
  let Reserve
  let Token

  let owner
  let addr1
  let token
  let claimingAddress

  before(async () => {
    setSnapshotId(await evmSnapshot())
  })

  beforeEach(async () => {
    ;[owner, addr1, holder1, holder2] = await ethers.getSigners()

    Token = await ethers.getContractFactory('TestToken')
    token = await Token.deploy()

    claimingAddress = '0x70997970C51812dc3A010C7d01b50e0d17dc79C8'
  })

  it('claim should fail except owner', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)
    const vested = await reserve.vestedOf(holder2.address)

    await expect(reserve.connect(holder1).claim(vested)).to.be.revertedWith(
      'ManagedVestingReserve: Unsupported method'
    )

    await expect(reserve.connect(holder2).claim(vested)).to.be.revertedWith(
      'ManagedVestingReserve: Unsupported method'
    )
  })

  it('claim for', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    const vested = await reserve.vestedOf(holder2.address)
    await reserve.connect(owner).claimFor(holder2.address, vested)

    const balance = await token.balanceOf(holder2.address)
    const difference = vested - balance

    expect(difference < 1000000)
  })

  it('claim for with another address', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    const vested = await reserve.vestedOf(holder2.address)
    await expect(reserve.connect(holder2).claimFor(holder2.address, vested)).to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('could not initialize twice', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await expect(reserve.initialize()).to.be.revertedWith(`ManagedVestingReserve: Already initialized`)
  })

  async function deployReserve(startTime, endTime, editAddressUntil) {
    Reserve = await ethers.getContractFactory('ManagedVestingReserveTest')
    const reserve = await Reserve.deploy(token.address, startTime, endTime, editAddressUntil)
    await token.approve(reserve.address, parseEther('1'))
    await reserve.initialize()
    return reserve
  }

  after(async () => {
    await evmRevert(snapshotId)
  })
})
