const { expect } = require('chai')
const { ethers } = require('hardhat')
const { timeLatest, evmRevert, evmSnapshot } = require('./helpers/utils')
const { setSnapshotId, snapshotId } = require('./helpers/make-suite')
const parseEther = ethers.utils.parseEther

describe('VestingReserve', function () {
  let Reserve
  let Token

  let owner
  let addr1
  let token
  let claimingAddress

  before(async () => {
    setSnapshotId(await evmSnapshot())
  })

  beforeEach(async () => {
    Reserve = await ethers.getContractFactory('VestingReserveTest')
    Token = await ethers.getContractFactory('TestToken')
    ;[owner, addr1, holder1, holder2] = await ethers.getSigners()

    token = await Token.deploy()

    claimingAddress = '0x70997970C51812dc3A010C7d01b50e0d17dc79C8'
  })

  it('vestedOf after time passes returns total locked', async () => {
    const now = await timeLatest()
    const end = now.add(10)
    const reserve = await deployReserve(now, end, 0)

    ethers.provider.send('evm_increaseTime', [end.add(1).toNumber()])
    ethers.provider.send('evm_mine')

    expect(await reserve.vestedOf(claimingAddress)).to.equal('500000000000000000')
  })

  it('vestedOf at half vested time', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(10000), 0)

    ethers.provider.send('evm_increaseTime', [5000])
    ethers.provider.send('evm_mine')

    const vested = await reserve.vestedOf(claimingAddress)

    const difference = vested - 250000000000000000 // Approximation of the returned value
    expect(difference < 1000000)
  })

  it('lockedOf after time passes returns 0', async () => {
    const now = await timeLatest()
    const end = now.add(10)
    const reserve = await deployReserve(now, end, 0)

    ethers.provider.send('evm_increaseTime', [end.add(1).toNumber()])
    ethers.provider.send('evm_mine')

    expect(await reserve.lockedOf(claimingAddress)).to.equal(0)
  })

  it('lockedOf at half vested time', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(10000), 0)

    ethers.provider.send('evm_increaseTime', [5000])
    ethers.provider.send('evm_mine')

    const locked = await reserve.lockedOf(claimingAddress)
    const difference = locked - 250000000000000000

    expect(difference < 1000000)
  })

  it('startime has to be before endtime', async () => {
    const now = await timeLatest()
    const reserve = await 
    expect(
      deployReserve(now.add(100000),now, now.add(100000))
    ).to.be.revertedWith('VestingReserve: end time must be later than start time')
  })

  it('_editAddressUntil has to be before endtime', async () => {
    const now = await timeLatest()
    const reserve = await 
    expect(
      deployReserve(now,now.add(10), now.add(100000))
    ).to.be.revertedWith('VestingReserve: _editAddressUntil time should be before than end time')
  })


  it('changeTokenOwnership fails if caller is not owner', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(100000), now.add(100000))
    expect(
      reserve.connect(addr1).changeTokenOwnership(claimingAddress, addr1.address)
    ).to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('changeTokenOwnership fails if block.timestamp is older than editAddressUntil', async () => {
    const now = await timeLatest()

    const reserve = await deployReserve(now, now.add(100000), now.add(1))
    ethers.provider.send('evm_increaseTime', [5000])
    ethers.provider.send('evm_mine')
    expect(reserve.changeTokenOwnership(claimingAddress, addr1.address)).to.be.revertedWith(
      'VestingReserve: Expired date to change token ownership'
    )
  })

  it('changeTokenOwnership', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(100000), now.add(100000))
    expect(await reserve.locked(holder2.address)).to.equal('0')
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)
    expect(await reserve.locked(holder2.address)).to.equal('500000000000000000')
  })

  it('claim', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    const vested = await reserve.vestedOf(holder2.address)
    await reserve.connect(holder2).claim(vested)

    const balance = await token.balanceOf(holder2.address)
    const difference = vested - balance

    expect(difference < 1000000)
  })

  it('claim with more than available', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    const vested = await reserve.vestedOf(holder2.address)
    await reserve.connect(holder2).claim(vested + parseEther('10000'))

    const balance = await token.balanceOf(holder2.address)
    const difference = vested - balance

    expect(difference < 1000000)
  })

  it('claim half', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    const vested = await reserve.vestedOf(holder2.address)
    console.log(vested.toString())
    await reserve.connect(holder2).claim(vested.div(2))

    const balance = await token.balanceOf(holder2.address)
    const difference = vested / 2 - balance

    expect(difference < 1000000)
  })

  it('when half of vested is available, claim less and later the rest', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(10000), now.add(10000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    ethers.provider.send('evm_increaseTime', [5000])
    ethers.provider.send('evm_mine')

    const vested = await reserve.vestedOf(holder2.address)
    let difference = vested - parseEther('0.25')

    expect(difference < 1000000)

    const balanceFirst = await token.balanceOf(holder2.address)
    const lessThanVested = vested.sub(parseEther('0.1'))

    await reserve.connect(holder2).claim(lessThanVested)

    const balanceAfterFirstClaim = await token.balanceOf(holder2.address)
    expect(balanceAfterFirstClaim).to.eq(balanceFirst.add(lessThanVested))

    ethers.provider.send('evm_increaseTime', [10001])
    ethers.provider.send('evm_mine')

    const totalVested = await reserve.vestedOf(holder2.address)
    expect(totalVested).to.eq(parseEther('0.5'))

    await reserve.connect(holder2).claim(lessThanVested)
    const finalBalance = await token.balanceOf(holder2.address)
    difference = finalBalance - parseEther('0.5')

    expect(difference < 1000000)
  })

  it('claim for', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    const vested = await reserve.vestedOf(holder2.address)
    await reserve.connect(owner).claimFor(holder2.address, vested)

    const balance = await token.balanceOf(holder2.address)
    const difference = vested - balance

    expect(difference < 1000000)
  })

  it('claim for with another address', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.changeTokenOwnership(claimingAddress, holder2.address)

    const vested = await reserve.vestedOf(holder2.address)
    await expect(reserve.connect(holder2).claimFor(holder2.address, parseEther('1'))).to.be.reverted
  })

  async function deployReserve(startTime, endTime, editAddressUntil) {
    const reserve = await Reserve.deploy(token.address, startTime, endTime, editAddressUntil)
    await token.approve(reserve.address, parseEther('1'))
    await reserve.initialize()
    return reserve
  }

  after(async () => {
    await evmRevert(snapshotId)
  })
})
