/*
    Crow equity vesting reserve test.

    Copyright (C) 2023 EthicHub

    This file is part of EthicHub ethix-contracts.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')
const { ethers } = require('hardhat')
const { timeLatest, evmRevert, evmSnapshot } = require('./helpers/utils')
const { setSnapshotId, snapshotId } = require('./helpers/make-suite')
const parseEther = ethers.utils.parseEther

describe('CrowdEquityVestingReserve', function () {
  let Reserve
  let Token

  let owner
  let token
  let holder1

  before(async () => {
    setSnapshotId(await evmSnapshot())
  })

  beforeEach(async () => {
    ;[owner, holder1] = await ethers.getSigners()

    Token = await ethers.getContractFactory('TestToken')
    token = await Token.deploy()
  })

  it('claim should fail for another user', async () => {
    // Deploy CrowLending Reserve
    const now = await timeLatest()
    const startTime = now
    const endTime = now.add(1000000)
    const editAddressUntil = now.add(1000000)
    const reserve = await deployReserve(startTime, endTime, editAddressUntil)

    // Send 1 token
    await expect(
      () =>  token.transfer(reserve.address, parseEther('1'))
    ).to.changeTokenBalance(token, reserve, parseEther('1'))

    // Add new investor
    expect(await reserve.allInvestorsLength()).to.equal(0)
    await reserve.addInvestor(owner.address, parseEther('1'))
    expect(await reserve.allInvestorsLength()).to.equal(1)
    expect(await reserve.getAllInvestors()).to.be.eql([`${owner.address}`])

    const vested = await reserve.vestedOf(owner.address)
    expect(vested).to.be.not.equal(0)

    await expect(reserve.connect(holder1).claim(vested)).to.be.revertedWith(
      'VestingReserve: No tokens to transfer'
    )
  })

  it('claim', async () => {
    // Deploy CrowLending Reserve
    const now = await timeLatest()
    const startTime = now
    const endTime = now.add(1000000)
    const editAddressUntil = now.add(1000000)
    const reserve = await deployReserve(startTime, endTime, editAddressUntil)

    // Send 1 token
    await expect(
      () =>  token.transfer(reserve.address, parseEther('1'))
    ).to.changeTokenBalance(token, reserve, parseEther('1'))

    // Add new investor
    expect(await reserve.allInvestorsLength()).to.equal(0)
    await reserve.addInvestor(owner.address, parseEther('1'))
    expect(await reserve.allInvestorsLength()).to.equal(1)
    expect(await reserve.getAllInvestors()).to.be.eql([`${owner.address}`])

    const vested = await reserve.vestedOf(owner.address)
    expect(vested).to.be.not.equal(0)

    await expect(
      () => reserve.claim(vested)
    ).to.changeTokenBalance(token, owner, vested)
  })

  it('claim change address investor', async () => {
    // Deploy CrowLending Reserve
    const now = await timeLatest()
    const startTime = now
    const endTime = now.add(1000000)
    const editAddressUntil = now.add(1000000)
    const reserve = await deployReserve(startTime, endTime, editAddressUntil)

    // Send 1 token
    await expect(
      () =>  token.transfer(reserve.address, parseEther('1'))
    ).to.changeTokenBalance(token, reserve, parseEther('1'))

    // Add new investor
    expect(await reserve.allInvestorsLength()).to.equal(0)
    await reserve.addInvestor(owner.address, parseEther('1'))
    expect(await reserve.allInvestorsLength()).to.equal(1)
    expect(await reserve.getAllInvestors()).to.be.eql([`${owner.address}`])

    const vested = await reserve.vestedOf(owner.address)
    expect(vested).to.be.not.equal(0)

    // change investor address
    await reserve.changeTokenOwnership(owner.address, holder1.address)

    await expect(reserve.claim(vested)).to.be.revertedWith(
      'VestingReserve: No tokens to transfer'
    )

    await expect(
      () => reserve.connect(holder1).claim(vested)
    ).to.changeTokenBalance(token, holder1, vested)
  })


  it('claimFor', async () => {
    // Deploy CrowLending Reserve
    const now = await timeLatest()
    const startTime = now
    const endTime = now.add(1000000)
    const editAddressUntil = now.add(1000000)
    const reserve = await deployReserve(startTime, endTime, editAddressUntil)

    // Send 1 token
    await expect(
      () =>  token.transfer(reserve.address, parseEther('1'))
    ).to.changeTokenBalance(token, reserve, parseEther('1'))

    // Add new investor
    expect(await reserve.allInvestorsLength()).to.equal(0)
    await reserve.addInvestor(owner.address, parseEther('1'))
    expect(await reserve.allInvestorsLength()).to.equal(1)
    expect(await reserve.getAllInvestors()).to.be.eql([`${owner.address}`])

    const vested = await reserve.vestedOf(owner.address)
    expect(vested).to.be.not.equal(0)

    // change investor address
    await reserve.changeTokenOwnership(owner.address, holder1.address)

    await expect(
      () => reserve.claimFor(holder1.address, vested)
    ).to.changeTokenBalance(token, holder1, vested)
  })

  it('claimFor should fail without owner', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await reserve.addInvestor(owner.address, parseEther('1'))

    const vested = await reserve.vestedOf(owner.address)
    expect(vested).to.be.not.equal(0)
    await expect(reserve.connect(holder1).claimFor(owner.address, vested)).to.be.revertedWith('Ownable: caller is not the owner')
  })

  it('Could not initialize twice', async () => {
    const now = await timeLatest()
    const reserve = await deployReserve(now, now.add(1000000), now.add(100000))
    await expect(reserve.initialize()).to.be.revertedWith(`CrowdEquityVestingReserve: Already initialized`)
  })

  async function deployReserve(startTime, endTime, editAddressUntil) {
    Reserve = await ethers.getContractFactory('CrowdEquityVestingReserve')
    const reserve = await Reserve.deploy(token.address, startTime, endTime, editAddressUntil)
    await token.approve(reserve.address, parseEther('1'))
    await reserve.initialize()
    return reserve
  }

  after(async () => {
    await evmRevert(snapshotId)
  })
})
