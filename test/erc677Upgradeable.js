const hardhat = require('hardhat')
const { expect } = require('chai')
const ethers = hardhat.ethers

describe('ERC677 Upgradeable', function () {
  let wallet1, wallet2

  beforeEach(async () => {
    ERC677Test = await ethers.getContractFactory('ERC677Test')
    ;[wallet1, wallet2] = await ethers.getSigners()

    token = await ERC677Test.deploy()
    await token.__ERC677Test_init()

    Receiver = await ethers.getContractFactory('ERC677ReceiverTest')
    receiver = await Receiver.deploy()
  })

  it('should transferAndCall', async () => {
    let data = ethers.utils.formatBytes32String('1')
    await token.transferAndCall(receiver.address, 100, data)
    expect(await receiver.amount()).to.equal(100)
    // TODO test data
    // expect(ethers.utils.parseBytes32String(await receiver.data())).to.equal(ethers.utils.parseBytes32String('1'))
    expect(await receiver.from()).to.equal(wallet1.address)
  })
})
